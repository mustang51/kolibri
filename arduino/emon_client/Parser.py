class Parser:
    source = None
    item   = {}
    eof    = False
    MAX_CHANNELS = 3

    def __init__(self, source):
        self.source = source

    def parse_base(self, line):
        values = line.split()

        try:
            dcoffset    = int(values[1])
            samples     = int(values[2])
            timems      = int(values[3])
            temperature = int(values[4])

            self.item['dcoffset']    = dcoffset
            self.item['samples']     = samples
            self.item['timems']      = timems
            self.item['temperature'] = temperature

        except ValueError:
            pass

    def parse_voltage(self, line):
        values = line.split()

        if "offline" == values[1]:
            return

        try:
            voltage   = float(values[1])
            frequency = float(values[2])

            self.item['voltage']   = voltage
            self.item['frequency'] = frequency
        except ValueError:
            pass

    def parse_current(self, line):
        values = line.split()

        try:
            channel = int(values[1])

            if channel < 1 or channel > self.MAX_CHANNELS:
                return

            if "offline" == values[2]:
                return

            current_key = 'current_' + str(channel)

            current = {}
            current['power_real']     = float(values[2])
            current['power_apparent'] = float(values[3])
            current['current']        = float(values[4])
            current['current_max']    = float(values[5])
            current['power_factor']   = float(values[6])
            current['gain']           = values[7]

            self.item[current_key] = current
        except ValueError:
            pass

    def parse_line(self, line):
        retval = None

        if line.startswith("BASE "):
            retval = self.item
            self.item = {}
            self.parse_base(line)

        elif line.startswith("VOLT "):
            self.parse_voltage(line)

        elif line.startswith("CURR "):
            self.parse_current(line)
        else:
            pass

        return retval

    def parse(self):
        while True:
            line = self.source.readline()

            if "" == line:
                if False == self.eof:
                    self.eof = True
                    return item
                else:
                    raise EOFError
            else:
                self.eof = False

                line = line.strip()

                item = self.parse_line(line)

                if None != item:
                    return item
