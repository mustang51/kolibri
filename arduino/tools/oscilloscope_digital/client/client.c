#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

static const char *portname = "/dev/ttyUSB0";

int set_interface_attribs (int fd, int speed, int parity)
{
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	if (tcgetattr (fd, &tty) != 0) {
		perror("error from tcgetattr");
		return -1;
	}

	cfsetospeed (&tty, speed);
	cfsetispeed (&tty, speed);

	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
	// disable IGNBRK for mismatched speed tests; otherwise receive break
	// as \000 chars
	tty.c_iflag &= ~IGNBRK;         // ignore break signal
	tty.c_lflag = 0;                // no signaling chars, no echo,
	// no canonical processing
	tty.c_oflag = 0;                // no remapping, no delays
	tty.c_cc[VMIN]  = 0;            // read doesn't block
	tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

	tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
	// enable reading
	tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
	tty.c_cflag |= parity;
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CRTSCTS;

	if (tcsetattr (fd, TCSANOW, &tty) != 0) {
		perror("error from tcsetattr");
		return -1;
	}
	return 0;
}

void set_blocking (int fd, int should_block)
{
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	if (tcgetattr (fd, &tty) != 0) {
		perror ("error from tggetattr");
		return;
	}

	tty.c_cc[VMIN]  = should_block ? 1 : 0;
	tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

	if (tcsetattr (fd, TCSANOW, &tty) != 0) {
		perror("error setting term attributes");
	}
}

int main(void)
{
	int fd;
	unsigned char byte;
	unsigned int value1, value2, value3, value4;
	unsigned int time, base_time = 0, last_time = 0;

	fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);

	if (fd < 0) {
		perror("error in open()");
		return;
	}

	set_interface_attribs (fd, B230400, 0);  // set speed to 230400 bps, 8n1 (no parity)
	set_blocking (fd, 1); // set no blocking

	while(1) {
		if (1 != read(fd, &byte, 1)) {
			continue;
		}

		time = base_time + ((byte & 0xF) * 8);

		if (time < last_time) {
			base_time += (16 * 8);
			time      += (16 * 8);
		}

		last_time = time;

		value1 = (byte & 0x80) ? 1 : 0;
		value2 = (byte & 0x40) ? 1 : 0;
		value3 = (byte & 0x20) ? 1 : 0;
		value4 = (byte & 0x10) ? 1 : 0;

		printf("%d, %d, %d, %d, %d\n",
		       time, value1, value2, value3, value4);
	}

	return 0;
}
