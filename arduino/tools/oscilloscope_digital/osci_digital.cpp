// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>

#define PROBE1 2
#define PROBE2 3
#define PROBE3 4
#define PROBE4 5

// Setup the serial port and pin 2
void setup()
{
	Serial.begin(230400);

	pinMode(PROBE1, INPUT);
	pinMode(PROBE2, INPUT);
	pinMode(PROBE3, INPUT);
	pinMode(PROBE4, INPUT);
}

void loop()
{
	unsigned char value;

	// read time
	value = (micros() / 8) & 0x0F; // 4 bit timer, in 8 us steps

	// read first channel
	value |= digitalRead(PROBE1) << 7;
	value |= digitalRead(PROBE2) << 6;
	value |= digitalRead(PROBE3) << 5;
	value |= digitalRead(PROBE4) << 4;

	Serial.write(value);
}
