// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>

#define NR_SAMPLES 450 // 1800 bytes at 4 byte per sample
static int probe = 0;  // pin A0 to probe

static int led = 13;

struct sample {
	unsigned int value;
	unsigned int time;
};

struct sample samples[NR_SAMPLES];

static void get_samples(void)
{
	unsigned int i;
	unsigned long start;

	digitalWrite(led, HIGH);

	start = micros() / 10; // using 10 uS as timing resolution;
	
	// read samples as fast as possible
	for (i = 0; i < NR_SAMPLES; i++) {
		samples[i].value = analogRead(probe);

		samples[i].time = (micros() / 10) - start;
	}
	
	digitalWrite(led, LOW);
}

static void xmit_samples(void)
{
	unsigned int i;

	// dump all samples via serial
	for (i = 0; i < NR_SAMPLES; i++) {
		Serial.print(i, DEC);
		Serial.print(": ");
		Serial.print(((unsigned long) samples[i].time) * 10UL, DEC);
		Serial.print(": ");
		Serial.print(samples[i].value, DEC);
		Serial.println("");
		Serial.flush();
	}

	Serial.flush();
}

void setup()
{
	pinMode(led, OUTPUT);

	Serial.begin(9600);
	
}

void loop()
{
	static int shot = 0;
	
	if ( ! shot ) {
		get_samples();
	
		xmit_samples();

		shot = 1;
	}
}
