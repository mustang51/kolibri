// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>

// Define various ADC prescaler
#define PS_16  (1 << ADPS2)
#define PS_32  ((1 << ADPS2) | (1 << ADPS0))
#define PS_64  ((1 << ADPS2) | (1 << ADPS1))
#define PS_128 ((1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0))

// Setup the serial port and pin 2
void setup()
{
	Serial.begin(230400);
	pinMode(2, INPUT);

	// set up the ADC
	ADCSRA &= ~PS_128;  // remove bits set by Arduino library

	ADCSRA |= PS_16;    // set our own prescaler to 16
}

void loop()
{
	unsigned char time1, time2;
	unsigned char value1, value2;

	// read first channel

	time1 = (micros() / 8) % 256; // 8 bit timer, in 8 us steps

	value1 = analogRead(0) / 16; // 6 bit resolution

	value1 |= 0x00; // mark as first channel

	// read second channel

	time2 = (micros() / 8) % 256; // 8 bit timer, in 8 us steps

	value2 = analogRead(1) / 16; // 6 bit resolution

	value2 |= 0x40; // mark as second channel

	Serial.write(value1);
	Serial.write(time1);

	Serial.write(value2);
	Serial.write(time2);
}

