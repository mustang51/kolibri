// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>
#define PULSE_TIME_MS	333
#define LED_PULSE	13
#define LED_PULSE_MS	50

char straight[8] = {3, 2, 4, 5, 6, 7, 8, 9};
char cross[8] = {4, 7, 2, 5, 6, 3, 8, 9};

void pulse_out(char pin, unsigned int ms);

void setup() {
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(12, INPUT); // Straight (normal) cable or cross cable
  pinMode(13, OUTPUT); // Control LED
}

void loop() {
	for (int n = 0; n < 8; n++) {
		pulse_out(straight[n], PULSE_TIME_MS);
	}
}

void pulse_out(char pin, unsigned int ms)
{
	if (ms < LED_PULSE_MS) {
		ms = LED_PULSE_MS;
	}

	digitalWrite(LED_PULSE, HIGH);
	digitalWrite(pin, HIGH);

	delay(LED_PULSE_MS); // just a small pulse on the internal LED

	digitalWrite(LED_PULSE, LOW);

	delay(ms - LED_PULSE_MS); // wait the remaining time

	digitalWrite(pin, LOW);
}
