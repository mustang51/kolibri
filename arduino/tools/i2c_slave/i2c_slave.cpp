// -*- mode: c++; c-file-style: "linux" -*-

/** @file */

#include <Arduino.h>

#include <Wire.h>

static unsigned char counter = 0;

int led = 13;

void i2c_request(void)
{
	Wire.write(counter);
	counter++;

	digitalWrite(led, HIGH);
	delay(70);
	digitalWrite(led, LOW);
}

void setup()
{
	Wire.begin(10);
	Wire.onRequest(i2c_request);
	pinMode(led, OUTPUT);
}

void loop()
{
	
}
