#define NR_CHANNELS 4
#define NR_PINS 8
#define PRESCALER 16

class FastADC
{
 public:
  FastADC();

  void setPrescaler(unsigned char prescaler);

  void start();

  int readPin(unsigned char pin);

  bool isFinished();

  unsigned long getDuration();

  long readVcc();

  long readTemp();

  void setChannel(unsigned char channel, unsigned char pin);

  static volatile unsigned int values[NR_CHANNELS];

 private:

  void setupADC(unsigned char prescaler);
};
