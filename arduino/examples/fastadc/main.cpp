#include <Arduino.h>

#include "FastADC.h"
#include "Statistic.h"

#define MSECS_IN_A_SEC (1000)
#define MAX_TIME_DIFF (1UL << 31UL)
#define LOOP_LENGTH_MSECS(m) (m * MSECS_IN_A_SEC)

static FastADC fastadc;

void setup(void)
{
  Serial.begin(115200);

  fastadc.setChannel(0, 0);
  fastadc.setChannel(1, 1);
  fastadc.setChannel(2, 2);
  fastadc.setChannel(3, 3);
}

void adc_loop(int loop_secs, char* scaler)
{
  unsigned long end = millis() + LOOP_LENGTH_MSECS(loop_secs);
  unsigned long now;
  unsigned long count = 0;
  unsigned long microseconds_adc = 0;
  unsigned int last_samples[NR_CHANNELS];
  Statistic stats[NR_CHANNELS];
  unsigned char ptr;

  fastadc.start();
  for (now = millis();
       (end - now) < MAX_TIME_DIFF;
       now = millis()) {

    while(!fastadc.isFinished());

    microseconds_adc += fastadc.getDuration();

    // feeling brave here, dont block interrupts, start conversion BEFORE reading
    fastadc.start();

    for (ptr = 0; ptr < NR_CHANNELS; ptr++) {
      last_samples[ptr] = fastadc.values[ptr];
    }
    // out of the woods now, and we have data to work with

    for (ptr = 0; ptr < NR_CHANNELS; ptr++) {
      stats[ptr].add(last_samples[ptr]);
    }
  }

  count = stats[0].count();

  Serial.print("scaler: ");
  Serial.print(scaler);
  Serial.print(", measures: ");
  Serial.print(count);
  Serial.print(", usecs/measure: ");
  Serial.println((double)microseconds_adc / (double)((count / NR_CHANNELS)));

  for (ptr = 0; ptr < NR_CHANNELS; ptr++) {
    count += stats[ptr].count();
    Serial.print("   ");
    Serial.print(ptr);
    Serial.print(":  average: ");
    Serial.print(stats[ptr].average());
    // Serial.print(", stdev: ");
    // Serial.print(stats[ptr].pop_stdev());
    // Serial.print(", variance: ");
    // Serial.print(stats[ptr].variance());
    Serial.print(", min: ");
    Serial.print(stats[ptr].minimum());
    Serial.print(", max: ");
    Serial.println(stats[ptr].maximum());
  }
}

void loop(void)
{
  Serial.print("Temp: ");
  Serial.print(fastadc.readTemp());

  Serial.print(", vcc: ");
  Serial.print(fastadc.readVcc());

  Serial.print(",  0: ");
  Serial.println(fastadc.readPin(1));

  fastadc.setPrescaler(4);
  adc_loop(2, "4");

  fastadc.setPrescaler(8);
  adc_loop(2, "8");

  fastadc.setPrescaler(16);
  adc_loop(2, "16");

  fastadc.setPrescaler(32);
  adc_loop(2, "32");

  fastadc.setPrescaler(64);
  adc_loop(2, "64");

  fastadc.setPrescaler(128);
  adc_loop(2, "128");
}
