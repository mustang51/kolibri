#include <Arduino.h>

#include "FastADC.h"

#define LOOP_LENGTH_MSECS(x) (x * MSECS_IN_A_SEC)
#define LOOP_LENGTH_USECS(x) (x * USECS_IN_A_SEC)
#define MSECS_IN_A_SEC (1000UL)
#define USECS_IN_A_SEC (1000UL * MSECS_IN_A_SEC)

#define MAX_TIME_DIFF  (1UL << 31UL)

// clear ADLAR in ADMUX (0x7C) to right-adjust the result
// ADCL will contain lower 8 bits, ADCH upper 2 (in last two bits)
// Set REFS1..0 in ADMUX (0x7C) to change reference voltage to the
// proper source (01)
#define ADMUX_FROM_SOURCE(s) (B01000000 | s)
#define SOURCE_FROM_ADMUX(a) (B00001111 & a)

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

volatile unsigned int FastADC::values[NR_CHANNELS];

static volatile bool ready;
static volatile unsigned char ptr;
static volatile unsigned long start_us;
static volatile unsigned long stop_us;
static volatile unsigned long admux[NR_CHANNELS];

FastADC::FastADC()
{
  setupADC(PRESCALER);
}

void FastADC::setupADC(unsigned char prescaler)
{
  unsigned char channel;

  // Set ADEN in ADCSRA (0x7A) to enable the ADC.
  // Note, this instruction takes 12 ADC clocks to execute
  sbi(ADCSRA, ADEN);

  // clear ADATE in ADCSRA (0x7A) to disable auto-triggering.
  cbi(ADCSRA, ADATE);

  // Clear ADTS2..0 in ADCSRB (0x7B) to set trigger mode to free running.
  // This means that as soon as an ADC has finished, the next will be
  // immediately started.
  cbi(ADCSRB, ADTS0);
  cbi(ADCSRB, ADTS1);
  cbi(ADCSRB, ADTS2);

  // Set ADIE in ADCSRA (0x7A) to enable the ADC interrupt.
  // Without this, the internal interrupt will not trigger.
  sbi(ADCSRA, ADIE);

  // set prescaler
  setPrescaler(prescaler);

  // set the channel to measure
  for (channel = 0; channel < NR_CHANNELS; channel++) {
    admux[channel] = ADMUX_FROM_SOURCE(channel);
  }
}

void FastADC::setChannel(unsigned char channel, unsigned char pin)
{
  if ((channel >= NR_CHANNELS) || (pin >= NR_PINS)) {
    return;
  }

  admux[channel] = ADMUX_FROM_SOURCE(pin);
}

void FastADC::setPrescaler(unsigned char value)
{
  cbi(ADCSRA,ADPS2);
  cbi(ADCSRA,ADPS1);
  cbi(ADCSRA,ADPS0);

  switch (value) {
  case 2:
    ADCSRA |= 1;
    break;
  case 4:
    ADCSRA |= 2;
    break;
  case 8:
    ADCSRA |= 3;
    break;
  case 16:
    ADCSRA |= 4;
    break;
  case 32:
    ADCSRA |= 5;
    break;
  case 64:
    ADCSRA |= 6;
    break;
  case 128:
    ADCSRA |= 7;
    break;
  }
}

void FastADC::start(void)
{
  ptr	= 0;
  ready = false;
  start_us = micros();

  ADMUX = admux[ptr];

  // Set ADSC in ADCSRA (0x7A) to start the ADC conversion
  sbi(ADCSRA, ADSC);
}


int FastADC::readPin(unsigned char pin)
{
  int result;

  // clear ADIE in ADCSRA (0x7A) to disable the ADC interrupt.
  cbi(ADCSRA, ADIE);
  ADMUX = ADMUX_FROM_SOURCE(pin);

  // Set ADSC in ADCSRA (0x7A) to start the ADC conversion
  sbi(ADCSRA, ADSC);

  while (bit_is_set(ADCSRA, ADSC));
  result  = ADCL;
  result |= (ADCH << 8);

  // reenable interrupts
  sbi(ADCSRA, ADIE);

  return result;
}


long FastADC::readVcc()
{
  long result;

  // clear ADIE in ADCSRA (0x7A) to disable the ADC interrupt.
  cbi(ADCSRA, ADIE);

  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);

  delay(5);					// Wait for Vref to settle
  ADCSRA |= _BV(ADSC);				// Convert
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = 1126400L / result;			//1100mV*1024 ADC steps http://openenergymonitor.org/emon/node/1186

  // reenable interrupts
  sbi(ADCSRA, ADIE);

  return result;
}

long FastADC::readTemp()
{
  long result;

  // clear ADIE in ADCSRA (0x7A) to disable the ADC interrupt.
  cbi(ADCSRA, ADIE);

  // Set the internal reference and mux.
  ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(MUX3);

  delay(5); // Wait for Vref to settle

  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA, ADSC));
  result = ADCL;
  result |= ADCH<<8;

  //1100mV*1024 ADC steps http://openenergymonitor.org/emon/node/1186
  result = (result - 324) / 1.22;

  // reenable interrupts
  sbi(ADCSRA, ADIE);

  return result;
}

unsigned long FastADC::getDuration()
{
  return stop_us - start_us;
}

bool FastADC::isFinished()
{
  return ready;
}

// Interrupt service routine for the ADC completion
ISR(ADC_vect)
{
  FastADC::values[ptr] = ADCL | (ADCH << 8);
  ptr++;

  if (ptr >= NR_CHANNELS) { // last measure, rest
    stop_us = micros();
    ready   = true;
    return;
  }

  // select next channel in ADMUX
  ADMUX = admux[ptr];

  // Set ADSC in ADCSRA (0x7A) to start the ADC conversion
  sbi(ADCSRA, ADSC);
}
