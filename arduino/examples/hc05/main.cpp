#include <Arduino.h>
#include <SoftwareSerial.h>

SoftwareSerial bt(10, 11); // RX, TX
char buf[64];
unsigned char buf_idx = 0;

void setup(void)
{
	Serial.begin(38400);
	Serial.println("HC05 AT");

	bt.begin(38400);
}

void loop(void)
{
	int c;

	if (Serial.available()) {
		buf[buf_idx] = Serial.read();

		if (buf[buf_idx] == '\r') {
			buf[buf_idx + 1] = '\n';

			for (c = 0; c < buf_idx + 2; c++) {
				Serial.print("S: ");
				Serial.println(buf[c]);
				bt.print(buf[c]);
			}

			buf_idx = 0;

		} else {
			buf_idx++;
		}
	}

	if (bt.available()) {
		Serial.print("R: ");
		Serial.println(bt.read());
	}
}
