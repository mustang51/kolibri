#include <Arduino.h>

void setup()
{
	Serial.begin(115200);
}

void loop()
{
	static unsigned long last = micros();
	static bool magnet = false;
	static unsigned long hz = 0;

	if(digitalRead(A0) == LOW) {
		if (!magnet) {
			magnet = true;
			unsigned long now = micros();
			unsigned long delta = now - last;
			hz = 60000000 / delta;

			last = now;
		}
	} else {
		if (magnet) {
			magnet = false;
			Serial.println(hz);
		}
	}
}
