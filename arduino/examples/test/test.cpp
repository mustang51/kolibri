#include <Arduino.h>

void setup(void)
{
  Serial.begin(9600);
}

void loop(void)
{
  static int counter = 0;

  Serial.print("Count: ");
  Serial.print(counter);
  Serial.println("");
  Serial.flush();
  counter++;
  delay(100);

}
