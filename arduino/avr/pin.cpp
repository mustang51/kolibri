// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>

#include "pin.h"
#include "tasks.h"
#include "eeprom.h"
#include "task_queue.h"
#include "state.h"

unsigned int Pin::values_[PIN_MAX];
LastValues Pin::last_values_[PIN_MAX];

void LastValues::clear()
{
	cnt_ = 0;
}

void LastValues::push_analog(unsigned int value)
{
	unsigned char store;

	// new analog value, is it high, low or undefined?
	if (value >= Pin::ANALOG_THRESHOLD_HIGH) {
		store = 1;
	} else if (value <= Pin::ANALOG_THRESHOLD_LOW) {
		store = 0;
	} else {
		// undefined value, clear all accumulated values
		cnt_ = 0;
		return;
	}

	// push values aside
	values_ = (values_ << 1) | store;

	if (cnt_ < 8) {
		cnt_++;
	}
}

unsigned int LastValues::last()
{
	return values_ & 0x1;
}

void LastValues::push_digital(unsigned int value)
{
	unsigned char store;

	store = (0 == value)? 0 : 1;

	// push values aside
	values_ = (values_ << 1) | store;

	if (cnt_ < 8) {
		cnt_++;
	}
}

bool LastValues::all_high()
{
	unsigned char how_many = State::get(STATE_INPUT_FILTER_CNT).value();

	// not enough valid values stored yet
	if ((how_many > cnt_) || (0 == cnt_)) {
		return false;
	}

	for (char i = 0; i < (how_many + 1); i++) {
		unsigned char mask;

		mask = 1 << i;

		if (0 == (values_ & mask)) {
			return false; // a zero was found
		}
	}

	return true; // all tested values were nonzero
}

bool LastValues::all_low()
{
	unsigned char how_many = State::get(STATE_INPUT_FILTER_CNT).value();

	// not enough valid values stored yet
	if ((how_many > cnt_) || (0 == cnt_)) {
		return false;
	}

	for (char i = 0; i < (how_many + 1); i++) {
		unsigned char mask;

		mask = 1 << i;

		if (0 != (values_ & mask)) {
			return false; // a nonzero was found
		}
	}

	return true; // all tested values were zero
}

void Pin::setup()
{
	// for all digital pins
	for(int p = PIN_D4; p <= PIN_D12; p++){
		DigitalPin dp(p);
		union PinMode pm = dp.get_mode(); // read from eeprom
		dp.command_mode(pm, SRC_LOCAL);    // apply it
	}

	// for all analog pins
	for(int p = PIN_A0; p <= PIN_A7; p++){
		AnalogPin ap(p);
		union PinMode pm = ap.get_mode(); // read from eeprom
		ap.command_mode(pm, SRC_LOCAL);    // apply it
	}
}

unsigned char Pin::phy_pin()
{
	switch (pin_id_) {
	case PIN_D4:
		return 4;
	case PIN_D7:
		return 7;
	case PIN_D11:
		return 11;
	case PIN_D12:
		return 12;
	case PIN_A0:
		return A0;
	case PIN_A1:
		return A1;
	case PIN_A2:
		return A2;
	case PIN_A3:
		return A3;
	case PIN_A6:
		return A6;
	case PIN_A7:
		return A7;
	default:
		return 255;
	}
}

bool Pin::command(unsigned int value, enum command_source src)
{
	unsigned char output;
	union PinMode pm = get_mode();

	// handle overrides, local and remote
	if (pm.as_mode.local_override &&
	    (SRC_LOCAL == src)) {
		return false;
	}

	if (pm.as_mode.remote_override &&
	    (SRC_REMOTE == src)) {
		return false;
	}

	if (!pm.as_mode.is_output) {
		// commanding an input? (might be disabled, for testing)
		push_value(value, false);

		return true;
	} else if (pm.as_mode.is_output && pm.as_mode.is_enabled) {
		// write output value to pin
		output = (0 == value) ? LOW : HIGH;

		digitalWrite(phy_pin(), output);

		return true;
	}

	return false; // disabled output?
}

unsigned int Pin::eeprom_address()
{
	return (EEPROM_DIGITAL_MODE_D4 + pin_id_ - PIN_D4);
}

union PinMode Pin::get_mode()
{
	union PinMode pm;

	eeprom_load(eeprom_address(), &(pm.as_uint));

	return pm;
}

bool Pin::command_mode(union PinMode pm, enum command_source src)
{
	unsigned char reg;
	reg = pin_to_reg(pin_id_);

	task_cancel(get_task(), pin_id_); // new mode, new rules

	if (!pm.as_mode.is_enabled){
		// disable value
		if (pm.as_mode.is_output) {
			command(0, src); // disable it
		}

		State::get(reg).disable(); // show 0xFFFF as value

		return true;
	}

	// so... pin enabled

	if (pm.as_mode.is_output){
		if (!is_valid_output_pin()) {
			return false;
		}

		pinMode(phy_pin(), OUTPUT);

		command(0, src); // disable it
		set_value(0);
	} else {
		pinMode(phy_pin(), INPUT);
		task_add(get_task(), pin_id_, get_refresh_time());
	}

	// clear last values
	last().clear();

	return true;
}

bool Pin::is_analog()
{
	switch (pin_id_) {
	case PIN_D4:
	case PIN_D7:
	case PIN_D11:
	case PIN_D12:
		return false;
	default:
		return true;
	}
}


bool Pin::is_valid_output_pin()
{
	// in the nano, a6 and a7 cannot be outputs
	if ((PIN_A6 == pin_id_) || (PIN_A7 == pin_id_)) {
		return false;
	}

	return true;
}

unsigned int Pin::invert_value(unsigned int value)
{
	if (is_analog()) {
		return value = ANALOG_MAX - value;
	} else {
		return ! value;
	}
}

unsigned long Pin::get_refresh_time()
{
	return State::get(STATE_INPUT_POLL_TIME).value();
}

task_callback Pin::get_task()
{
	if (is_analog()) {
		return AnalogPin::task;
	} else {
		return DigitalPin::task;
	}
}

bool Pin::set_mode(union PinMode pm)
{
	return (true == eeprom_store(eeprom_address(), pm.as_uint));
}

unsigned char Pin::reg_to_pin(unsigned char reg)
{
	// mode or normal register?
	if ((reg >= STATE_DIGITAL_MODE_D4) &&
	    (reg <= STATE_ANALOG_MODE_A7)) {
		return (PIN_D4 + reg - STATE_DIGITAL_MODE_D4);
	} else if ((reg >= STATE_DIGITAL_D4) &&
	    (reg <= STATE_ANALOG_A7)) {
		return (PIN_D4 + reg - STATE_DIGITAL_D4);
	}

	return 255; // error
}

unsigned char Pin::pin_to_reg(unsigned char pin)
{
	return (STATE_DIGITAL_D4 + pin - PIN_D4);
}

void Pin::push_value(unsigned int value, bool do_filter)
{
	union PinMode pm = get_mode();
	bool change = false;

	// is the pin inverted?
	if (pm.as_mode.is_inverted) {
		value = invert_value(value);
	}

	// store the value
	if (is_analog()) {
		last().push_analog(value);
	} else {
		last().push_digital(value);
	}

	// coming from UNK
	if (STATE_UNK == get_value()) {
		set_value(0);
	}

	// filter it
	if (do_filter) {
		if ((0 == get_value()) &&
		    last().all_high()) {
			// we were low and the last readings are high
			change = true;

		} else if (( 1 == get_value()) &&
			   last().all_low()) {
			// we were high and the last readings are low
			change = true;
		}
	} else {
		if (get_value() != last().last()) {
			change = true;
		}
	}

	// update accordingly
	switch(pm.as_mode.type) {
	case REGISTER_MODE_TYPE_METER:
		set_value(value);
		break;

	case REGISTER_MODE_TYPE_BUTTON:
		if (change) {

			set_value(! get_value());

			if (1 == get_value()) {
				input_event(pm.as_mode.target, INPUT_TOGGLE);
			}
		}

		break;

	case REGISTER_MODE_TYPE_SWITCH:
		if (change) {
			set_value(! get_value());

			input_event(pm.as_mode.target, get_value());
		}

		break;
	}
}


DigitalPin::DigitalPin(unsigned char reg) : Pin(reg) { };

unsigned long DigitalPin::task(unsigned char pin_id,
			       unsigned long expiration)
{
	DigitalPin p(pin_id);

	// read the pin
	unsigned int input = (HIGH == digitalRead(p.phy_pin())) ? 1 : 0;

	// push the value
	p.push_value(input, true);

	// rearm task
	return p.get_refresh_time();
}

AnalogPin::AnalogPin(unsigned char reg) : Pin(reg) { };

unsigned long AnalogPin::task(unsigned char pin_id,
			      unsigned long expiration)
{
	AnalogPin p(pin_id);
	State& s = State::get();

	// avoid race condition with vcc and temp tasks by
	// postponing this task some time
	if (true == s.analog_comparator_lock_) {
		return (p.get_refresh_time() / 2);
	}

	// read the pin
	unsigned int input = analogRead(p.phy_pin());

	// push the value
	p.push_value(input, true);

	// rearm task
	return p.get_refresh_time();
}


