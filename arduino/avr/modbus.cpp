// -*- mode: c++; c-file-style: "linux" -*-

#include "modbus.h"
#include "misc.h"
#include "state.h"
#include "task_queue.h"

#include "registers.h"
#include "buzzer.h"

#define BUFFER_SIZE 60 // same as arduino serial RX ring buffer

// frame[] is used to recieve and transmit packages.
static unsigned char frame[BUFFER_SIZE];
static unsigned int modbusFrameDelay; // modbus intra frame delay

static unsigned int rxCnt;
static unsigned long timeLastByte;

static unsigned char msgCnt = 0;

#define MAX485_DELAY_US (5) // 5 us

#define MODBUS_MAX_REGISTERS_PER_READ 25

#define WAIT_MAX_485 delayMicroseconds(MAX485_DELAY_US)

static unsigned int calculateCRC(byte bufferSize)
{
	unsigned int temp, temp2, flag;
	temp = 0xFFFF;
	for (unsigned char i = 0; i < bufferSize; i++) {
		temp = temp ^ frame[i];
		for (unsigned char j = 1; j <= 8; j++) {
			flag = temp & 0x0001;
			temp >>= 1;
			if (flag) {
				temp ^= 0xA001;
			}
		}
	}
	// Reverse byte order.
	temp2 = temp >> 8;
	temp = (temp << 8) | temp2;
	temp &= 0xFFFF;
	/* the returned value is already swopped - crcLo byte is
	   first & crcHi byte is last */
	return temp;
}

void writeRS485(bool mode)
{
	if (PIN_RS485_DIRECTION > 1) {
		WAIT_MAX_485;

		digitalWrite(PIN_RS485_DIRECTION, mode ? HIGH : LOW);

		WAIT_MAX_485;
	}
}

static void sendPacket(unsigned char bufferSize)
{
	Serial.flush();

	writeRS485(true);

	Serial.write(frame, bufferSize);

	/* contrary to Arduino documentation, this does what it is
	   supposed to do, wait for the tx buffer to be empty
	   check out HardwareSerial class on Arduino
	   base software. */

	Serial.flush();

	writeRS485(false);
}

static void exceptionResponse(unsigned char exception,
			      unsigned char function)
{
	frame[0] = State::get(STATE_DEVICE_ID).value();
	// set the MSB bit high, informs the master of an exception
	frame[1] = (function | 0x80);
	frame[2] = exception;
	// ID, function + 0x80, exception code == 3 bytes
	unsigned int crc = calculateCRC(3);
	frame[3] = crc >> 8;
	frame[4] = crc & 0xFF;
	/* exception response is always 5 bytes ID, function + 0x80,
	   exception code, 2 bytes crc */
	sendPacket(5);
}

void modbus_setup(unsigned long baud)
{
	Serial.begin(baud);

	// enable parity, even
	UCSR0C |= (1 << UPM01);

	// pin 0 & pin 1 are reserved for RX/TX. To disable set txenpin < 2
	if (PIN_RS485_DIRECTION > 1) {
		pinMode(PIN_RS485_DIRECTION, OUTPUT);

		writeRS485(false);
	}

	/* Modbus states that a baud rate higher than 19200 must use a
	fixed 750 us for inter character time out and 1.75 ms for a
	frame delay.  For baud rates below 19200 the timeing is more
	critical and has to be calculated.  E.g. 9600 baud in a 10 bit
	packet is 960 characters per second In milliseconds this will
	be 960characters per 1000ms. So for 1 character
	1000ms/960characters is 1.04167ms per character and finaly
	modbus states an intercharacter must be 1.5T or 1.5 times
	longer than a normal character and thus 1.5T = 1.04167ms * 1.5
	= 1.5625ms. A frame delay is 3.5T. */

	if (baud > 19200) {
		modbusFrameDelay = 1750;
	} else {
		modbusFrameDelay = 35000000UL / baud; // 1T * 3.5 = T3.5
	}

	// to milliseconds, round to the next value
	modbusFrameDelay = (modbusFrameDelay + 999) / 1000;

	rxCnt = 0; // buffer writing cursor

	Serial.flush();
}

bool modbus_poll(void)
{
	/* The maximum number of bytes is limited to the serial buffer
	   size of BUFFER_SIZE bytes. If more bytes are received than
	   the BUFFER_SIZE the serial buffer will be read until all
	   the data is cleared from the receive buffer. */

	// read any bytes present on the UART
	while (Serial.available()) {
		if (rxCnt < BUFFER_SIZE) {
			// another byte to read
			frame[rxCnt] = Serial.read();
		} else {
			// discard the remaining bytes
			Serial.read();
		}

		rxCnt++;
		timeLastByte = millis();
	}

	// wait for the frame delay
	if ((rxCnt == 0) ||
	    ((millis() - timeLastByte) < modbusFrameDelay)) {
		return false;
	}

	// ok, we have incoming data, and the delay between
	// frames has expired, so they may be a modbus frame.
	// But did we overflow?

	if (rxCnt >= BUFFER_SIZE) {
		rxCnt = 0; // clear the buffer for new data
		return false;
	}

	// everything looks OK
	return true;
}

void modbus_parse(void)
{
	unsigned char function;
	State& s = State::get();

	unsigned char broadcastFlag = 0;
	unsigned char noOfBytes, responseFrameSize, id, index, address;
	unsigned int  temp, noOfRegisters, crc, startingAddress, maxData;
	bool string_request = false;

	// If an overflow occurred increment the errorCount
	// variable and return to the main sketch without
	// responding to the request i.e. force a timeout
	if (rxCnt >= BUFFER_SIZE) {
		goto error_and_exit;
	}

	// The minimum request packet is 8 bytes for function 3 & 16
	if (rxCnt < 7) {
		goto error_and_exit;
	}

	// start processing the received frame
	id = frame[0];

	if (id == 0) {
		broadcastFlag = 1;
	}

	// check the destination ID
	if ((id != s.get(STATE_DEVICE_ID).value()) && (! broadcastFlag)) {
		goto exit;
	}

	// combine the CRC Low & High bytes
	crc = ((frame[rxCnt - 2] << 8) | frame[rxCnt - 1]);

	// check CRC
	if (calculateCRC(rxCnt - 2) != crc) {
		goto error_and_exit;
	}

	function = frame[1];
	// combine the starting address bytes
	startingAddress = ((frame[2] << 8) | frame[3]);
	// combine the number of register bytes
	noOfRegisters = ((frame[4] << 8) | frame[5]);
	maxData = startingAddress + noOfRegisters;

	msgCnt++;

	switch (function) {
	case 3: /* read holding registers */

		if (broadcastFlag) {
			// broadcasting is not allowed for function 3
			goto error_and_exit;
		}

		if (startingAddress >= 1000) {
			/* to recover a string describing a register,
			   access 1000 + register */
			string_request = true;

			if (startingAddress >=
			    (1000 + s.get(STATE_REGISTER_COUNT).value())) {
				// exception 2 ILLEGAL DATA ADDRESS
				exceptionResponse(2, function);
				goto error_and_exit;
			}

			if (maxData > startingAddress + 1) {
				// only one string per time
				exceptionResponse(2, function);
				goto error_and_exit;
			}

			Register r(startingAddress - 1000);
			noOfBytes = r.name_len() + 1;

			// must be multiple of 2
			if (noOfBytes & 0x1) {
				noOfBytes++;
			}

		} else {  // regular register access
			if (startingAddress >=
			    s.get(STATE_REGISTER_COUNT).value()) {
				// exception 2 ILLEGAL DATA ADDRESS
				exceptionResponse(2, function);
				goto error_and_exit;
			}

			if (maxData >
			    s.get(STATE_REGISTER_COUNT).value()) {
				// exception 3 ILLEGAL DATA VALUE
				exceptionResponse(3, function);
				goto error_and_exit;
			}

			if (noOfRegisters > MODBUS_MAX_REGISTERS_PER_READ) {
				exceptionResponse(2, function);
				goto error_and_exit;
			}

			noOfBytes = noOfRegisters * 2;
		}

		/* ID, function, noOfBytes,
		   (dataLo + dataHi) * number of registers, crcLo, crcHi */
		responseFrameSize = 5 + noOfBytes;

		frame[0] = s.get(STATE_DEVICE_ID).value();
		frame[1] = function;
		frame[2] = noOfBytes;

		address = 3; // PDU starts at the 4th byte

		if (false == string_request) {
			for (index = startingAddress; index < maxData;
			     index++) {
				Register r(index);

				temp = r.value();
				// split the register into 2 bytes
				frame[address] = temp >> 8;
				address++;
				frame[address] = temp & 0xFF;
				address++;
			}
		} else {
			Register r(startingAddress - 1000);
			r.name((char*)&(frame[address]));
		}

		crc = calculateCRC(responseFrameSize - 2);
		// split crc into 2 bytes
		frame[responseFrameSize - 2] = crc >> 8;
		frame[responseFrameSize - 1] = crc & 0xFF;

		sendPacket(responseFrameSize);
		goto exit;

	case 16: // write multiple registers

		if (broadcastFlag) {
			// don't respond if it's a broadcast message
			goto error_and_exit;
		}

		/* check if the recieved number of bytes matches the
		   calculated bytes minus the request bytes id + function
		   + (2 * address bytes) + (2 * no of register bytes) +
		   byte count + (2 * CRC bytes) = 9 bytes */
		if (frame[6] != (rxCnt - 9)) {
			goto error_and_exit;
		}

		if (startingAddress >= s.get(STATE_REGISTER_COUNT).value()) {
			// exception 2 ILLEGAL DATA ADDRESS
			exceptionResponse(2, function);
			goto error_and_exit;
		}

		if (maxData > s.get(STATE_REGISTER_COUNT).value()) {
			// exception 3 ILLEGAL DATA VALUE
			exceptionResponse(3, function);
			goto error_and_exit;
		}

		address = 7; // start at the 8th byte in the frame

		for (index = startingAddress; index < maxData; index++) {
			temp = ((frame[address] << 8) |
				frame[address + 1]);
			// send the command order to the state machine
			Register r(index);
			r.command(temp, SRC_REMOTE);

			address += 2;
		}

		// only the first 6 bytes are used for CRC calculation
		crc = calculateCRC(6);
		frame[6] = crc >> 8; // split crc into 2 bytes
		frame[7] = crc & 0xFF;

		/* a function 16 response is an echo of the first 6
		bytes from the request + 2 crc bytes */

		sendPacket(8);
		goto exit;
	default:
		exceptionResponse(1, function); // exception 1 ILLEGAL FUNCTION
		goto error_and_exit;
	}

error_and_exit:
	STATE_INC(STATE_MODBUS_ERRORS);
exit:
	rxCnt = 0;

	return;
}

void modbus_update_msg_counter(void)
{
	State::get(STATE_MODBUS_MSG_SEC).update(msgCnt);

	msgCnt = 0;
}

