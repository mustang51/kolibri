// -*- mode: c++; c-file-style: "linux" -*-

/**
 * @file   state.cpp
 * @author Yari Adán Petralanda <mustang@incantations>
 * @date   Tue Apr  9 14:22:20 2013
 *
 * @brief  Maintains the state variables in memory
 *
 *
 */

#include <Arduino.h>
#include <Wire.h>

#include "state.h"

#include "timer2.h"
#include "tasks.h"
#include "task_queue.h"
#include "sht2x.h"
#include "eeprom.h"
#include "heartbeat.h"
#include "relay.h"
#include "buzzer.h"
#include "modbus.h"
#include "pin.h"

// #define INITIALIZER

/**
 *
 *
 * @param reg
 * @param value
 * @param via_modbus
 *
 * @return
 */

void input_event(unsigned char target, unsigned char value)
{
	unsigned char next;
	State& s = State::get();
	unsigned char reg;
	unsigned int position, desired;

	switch(target) {
	case REGISTER_MODE_TARGET_NULL:
		break;
	case REGISTER_MODE_TARGET_CURTAINS:
		break;
	case REGISTER_MODE_TARGET_CURTAINS_NORMAL:
		reg      = STATE_CURTAINS_DESIRED1;
		position = s.get(STATE_CURTAINS_POSITION1).value();
		desired  = s.get(STATE_CURTAINS_DESIRED1).value();

		if (value == INPUT_TOGGLE) {
			if ((STATE_UNK == desired) ||
			    (position == desired)) { // stopped, so start
				s.get(reg).command(100, SRC_LOCAL);
			} else {
				// user wants to stop the movement
				s.get(reg).disable();
			}
		}

		break;
	case REGISTER_MODE_TARGET_CURTAINS_REVERSE:
		reg = STATE_CURTAINS_DESIRED1;

		position = s.get(STATE_CURTAINS_POSITION1).value();
		desired  = s.get(STATE_CURTAINS_DESIRED1).value();

		if (value == INPUT_TOGGLE) {
			if ((STATE_UNK == desired) ||
			    (position == desired)) { // stopped, so start
				s.get(reg).command(1, SRC_LOCAL);
			} else {
				// user wants to stop the movement
				s.get(reg).disable();
			}
		}

		break;
	case REGISTER_MODE_TARGET_AC1:
	case REGISTER_MODE_TARGET_AC2:
	case REGISTER_MODE_TARGET_AC3:
		reg = (STATE_RELAY_AC1 + target - REGISTER_MODE_TARGET_AC1);

		if (value == INPUT_TOGGLE) {
			next = ! s.get(reg).value();
		} else {
			next = (value == 0 ? 0 : 1);
		}

		s.get(reg).command(next, SRC_LOCAL);

		break;
	case REGISTER_MODE_TARGET_ALL_ACS:
		for (reg = STATE_RELAY_AC1; reg <= STATE_RELAY_AC3; reg++) {
			if (value == INPUT_TOGGLE) {
				next = ! s.get(reg).value();
			} else {
				next = (value == 0 ? 0 : 1);
			}

			s.get(reg).command(next, SRC_LOCAL);
		}

		break;
	}
}

State::State() :
	analog_comparator_lock_(false),
	loop_end_(0),
	loop_last_(0),
	loop_cnt_(0)
{

}

/**
 *
 *
 *
 * @return
 */

unsigned long State::serial_speed(void)
{
	unsigned int speed = get(STATE_SERIAL_SPEED).value();

	switch (speed) {
	case SERIAL_SPEED_9600:
		return 9600;
	case SERIAL_SPEED_19200:
		return 19200;
	case SERIAL_SPEED_38400:
		return 38400;
	case SERIAL_SPEED_57600:
		return 57600;
	case SERIAL_SPEED_115200:
		return 115200;
	case SERIAL_SPEED_230400:
		return 230400;
	case SERIAL_SPEED_460800:
		return 460800;
	}

	return 9600;
}

// put the strings in program memory to avoid wasting RAM
PROGMEM const char init_string_name[]	   = "KOLIBRI";
PROGMEM const char init_string_devid[]	   = "Dev ID = ";
PROGMEM const char init_string_speed[]	   = "Speed  = ";
PROGMEM const char init_string_switching[] = "switching to modbus";

void State::show(void)
{
	char buffer[32];

	Serial.begin(default_serial_speed_);

	// disable rs485 direction pin and wait a bit (we don't want
	// to contaminate the rs485 bus with our debug values)
	digitalWrite(PIN_RS485_DIRECTION, HIGH);
	delay(1);

	Serial.println();

	strcpy_P(buffer, (char*)init_string_name);
	Serial.println(buffer);

	strcpy_P(buffer, (char*)init_string_devid);
	Serial.print(buffer);
	Serial.print(State::get(STATE_DEVICE_ID).value());
	Serial.println();

	strcpy_P(buffer, (char*)init_string_speed);
	Serial.print(buffer);

	Serial.print(serial_speed());
	Serial.println();

	strcpy_P(buffer, (char*)init_string_switching);
	Serial.println(buffer);
	Serial.flush();

	delay(1);

	Serial.end();
}

#define DEFAULT_DEVICE_ID 69
#define DEFAULT_SERIAL_SPEED SERIAL_SPEED_115200

void State::check(void)
{
	if (0 == State::get(STATE_DEVICE_ID).value()) {
		// new Arduino, most likely, assign a valid device id
		State::get(STATE_DEVICE_ID).update(DEFAULT_DEVICE_ID);
	}

#ifdef INITIALIZER
	State::get(STATE_DEVICE_ID).update(DEFAULT_DEVICE_ID);
	State::get(STATE_SERIAL_SPEED).update(DEFAULT_SERIAL_SPEED);

	State::get(STATE_PWM_DUTY).update(255);
	State::get(STATE_RELAY_OPEN_TIME).update(100);
	State::get(STATE_VCC_CALIB_0).update(0);
	State::get(STATE_VCC_CALIB_1).update(0);
	State::get(STATE_DIGITAL_MODE_D4).update(1);
	State::get(STATE_DIGITAL_MODE_D7).update(1);
	State::get(STATE_DIGITAL_MODE_D11).update(1);
	State::get(STATE_DIGITAL_MODE_D12).update(1);
	State::get(STATE_ANALOG_MODE_A0).update(1);
	State::get(STATE_ANALOG_MODE_A1).update(1);
	State::get(STATE_ANALOG_MODE_A2).update(1);
	State::get(STATE_ANALOG_MODE_A3).update(1);
	State::get(STATE_ANALOG_MODE_A6).update(1);
	State::get(STATE_ANALOG_MODE_A7).update(1);
	State::get(STATE_CURTAINS_TRAVEL1_UP).update(0);
	State::get(STATE_CURTAINS_TRAVEL1_DOWN).update(0);
	State::get(STATE_CURTAINS_TRAVEL2_UP).update(0);
	State::get(STATE_CURTAINS_TRAVEL2_DOWN).update(0);
	State::get(STATE_INPUT_POLL_TIME).update(30);
	State::get(STATE_INPUT_FILTER_CNT).update(2);
#endif
}

/**
 *
 *
 */

void State::setup(void)
{
	// setup I2C
	Wire.begin();

	relays_setup();

	buzzer_setup();

	timer2_setup();

	Pin::setup();

	tasks_setup();

	heartbeat_setup();

	// check values
	check();

	// show configuration parameters before switching to modbus
	show();

	// setup modbus
	modbus_setup(serial_speed());

	// ring the buzzer to indicate the boot-up
	buzzer_ring(0, 0, 1);

	// start the latency timing
	loop_last_ = millis();
	loop_end_  = loop_last_ + loop_ms_to_measure_;
}

void State::latency()
{
	unsigned long now;
	unsigned int delta_delay;

	// we are called before each State:loop() is run
	now = millis();

	// calculate the delay of the last loop
	delta_delay = (unsigned int) (now & 0xFFFF) - loop_last_;

	// check if it is the biggest we have seen
	if (delta_delay > get(STATE_LATENCY_MS_MAX).value()) {
		get(STATE_LATENCY_MS_MAX).update(delta_delay);
	}

	loop_cnt_++;

	if (now >= loop_end_) {
		get(STATE_LATENCY_100NS).update(((unsigned long) loop_ms_to_measure_ *
					      1000 * 10 / loop_cnt_));

		loop_cnt_ = 0;
		loop_end_ = now + loop_ms_to_measure_;
	}

	loop_last_ = now;
}

/**
 *
 *
 */

void State::loop(void)
{
	latency();

	// execute any expired tasks
	task_run();
}

Register State::get(unsigned char reg)
{
	Register r(reg);

	return r;
}
