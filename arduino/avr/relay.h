// -*- mode: c++; c-file-style: "linux" -*-

#ifndef __RELAY_H__
#define __RELAY_H__

#define DEFAULT_CURTAINS_TRAVEL 100 /* ms */

enum {
	RELAY_CURTAINS1 = 0,
	RELAY_CURTAINS2,
	RELAY_AC1,
	RELAY_AC2,
	RELAY_AC3,

	RELAY_MAX // last one
};

enum {
	RELAY_STATE_CLOSED = 0,
	RELAY_STATE_OPENED
};

void relays_setup(void);

void relay_refresh_duty_cycle(void);

void relay_refresh_travels(void);

#endif
