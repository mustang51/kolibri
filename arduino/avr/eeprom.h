// -*- mode: c++; c-file-style: "linux" -*-

#ifndef __EEPROM_H__
#define __EEPROM_H__

bool eeprom_load(unsigned int address, unsigned int *value);

bool eeprom_store(unsigned int address, unsigned int value);

#endif
