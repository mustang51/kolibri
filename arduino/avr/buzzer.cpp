// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>

#include "state.h"
#include "buzzer.h"
#include "task_queue.h"

#define MAX_BUZZER_CMDS 2

enum {
	BUZZER_STATE_RINGING = 0,
	BUZZER_STATE_SILENT
};

static unsigned char buzzer_state;
static unsigned char buzzer_cnt;

struct buzzer_cmd {
	unsigned char ring;
	unsigned char silent;
	unsigned char ring_cnt;
};

static struct buzzer_cmd buzzer_cmds[MAX_BUZZER_CMDS];

static unsigned int buzzer_time_to_ms(unsigned char time)
{
	return 100 + (200 * (unsigned int) time);
}

static unsigned long buzzer_task(unsigned char c, unsigned long now)
{
	unsigned char i;
	unsigned long due = 0;

	switch (buzzer_state) {
	case BUZZER_STATE_RINGING:
		// was ringing, now it has to be silent
		digitalWrite(BUZZER_PIN, LOW);

		// one ring less
		if (buzzer_cmds[0].ring_cnt < 15) {
			buzzer_cmds[0].ring_cnt--;
		}

		due	     = buzzer_time_to_ms(buzzer_cmds[0].silent);
		buzzer_state = BUZZER_STATE_SILENT;

		break;

	case BUZZER_STATE_SILENT:
		if (buzzer_cmds[0].ring_cnt > 0) {
			// was silent, now it has to be ringing again
			digitalWrite(BUZZER_PIN, HIGH);

			due = buzzer_time_to_ms(buzzer_cmds[0].ring);
			buzzer_state = BUZZER_STATE_RINGING;
		} else {
			buzzer_cnt--;

			// update cmd queue
			for (i = 0; i < buzzer_cnt; i++) {
				buzzer_cmds[i] = buzzer_cmds[i + 1];
			}

			// any pending commands?
			if (buzzer_cnt > 0) {
				digitalWrite(BUZZER_PIN, HIGH);

				due = buzzer_time_to_ms(buzzer_cmds[0].ring);
				buzzer_state = BUZZER_STATE_RINGING;
			} else {
				due = 0;
			}
		}

		break;
	}

	return due;
}

void buzzer_setup(void)
{
	pinMode(BUZZER_PIN, OUTPUT);

	buzzer_state = BUZZER_STATE_SILENT;
	buzzer_cnt   = 0;
}

void buzzer_ring(unsigned char ring, unsigned char silent,
		 unsigned char count)
{
	// check if we can queue this cmd
	if (buzzer_cnt >= MAX_BUZZER_CMDS) {
		return; // skip cmd
	}

	buzzer_cmds[buzzer_cnt].ring_cnt = count;
	buzzer_cmds[buzzer_cnt].ring	 = ring;
	buzzer_cmds[buzzer_cnt].silent	 = silent;

	buzzer_cnt++;

	// start the task if needed
	if (1 == buzzer_cnt) {
		task_add(buzzer_task, STATE_LAST, 0);
	}
}

void buzzer_silence(void)
{
	buzzer_cnt = 0;
	task_cancel(buzzer_task, STATE_LAST);

	if (BUZZER_STATE_RINGING == buzzer_state) {
		digitalWrite(BUZZER_PIN, LOW);
		buzzer_state = BUZZER_STATE_SILENT;
	}
}



