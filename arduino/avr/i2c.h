// -*- mode: c++; c-file-style: "linux" -*-

#ifndef __I2C_H__
#define __I2C_H__

#include <Arduino.h>

#include <Wire.h> //IIC

unsigned char i2c_read(unsigned char address, unsigned char *byte);

unsigned char i2c_read(unsigned char address, unsigned char *buff,
		       unsigned char nbytes);

unsigned char i2c_write(unsigned char address, unsigned char byte);

unsigned char i2c_write(unsigned char address, unsigned char *buff,
			unsigned char nbytes);

#endif
