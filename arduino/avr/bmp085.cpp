// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>

#include <Wire.h> //IIC
#include <math.h>

#include "state.h"
#include "task_queue.h"
#include "bmp085.h"
#include "i2c.h"

/***************************************************************************
  Adaptation of the Adafruit BMP085 pressure sensor driver, the following
  is the original license

  This is a library for the BMP085 pressure sensor

  Designed specifically to work with the Adafruit BMP085 Breakout

  These displays use I2C to communicate, 2 pins are required to interface.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit andopen-source hardware by purchasing products
  from Adafruit!

  Written by Kevin Townsend for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ***************************************************************************/

#define BMP085_ADDRESS                (0x77)

#define BMP085_TIMEOUT_DEFAULT_MS           250
#define BMP085_TIMEOUT_POLL_MS             5000
#define BMP085_TIMEOUT_NEXT_MEASURE_MS    60000

enum {
	BMP085_REGISTER_CAL_AC1         = 0xAA,	// Calibration data (16 bits)
	BMP085_REGISTER_CAL_AC2         = 0xAC,	// Calibration data (16 bits)
	BMP085_REGISTER_CAL_AC3         = 0xAE,	// Calibration data (16 bits)
	BMP085_REGISTER_CAL_AC4         = 0xB0,	// Calibration data (16 bits)
	BMP085_REGISTER_CAL_AC5         = 0xB2,	// Calibration data (16 bits)
	BMP085_REGISTER_CAL_AC6         = 0xB4,	// Calibration data (16 bits)
	BMP085_REGISTER_CAL_B1          = 0xB6,	// Calibration data (16 bits)
	BMP085_REGISTER_CAL_B2          = 0xB8,	// Calibration data (16 bits)
	BMP085_REGISTER_CAL_MB          = 0xBA,	// Calibration data (16 bits)
	BMP085_REGISTER_CAL_MC          = 0xBC,	// Calibration data (16 bits)
	BMP085_REGISTER_CAL_MD          = 0xBE,	// Calibration data (16 bits)
	BMP085_REGISTER_CHIPID          = 0xD0,
	BMP085_REGISTER_VERSION         = 0xD1,
	BMP085_REGISTER_SOFTRESET       = 0xE0,
	BMP085_REGISTER_CONTROL         = 0xF4,
	BMP085_REGISTER_TEMPDATA        = 0xF6,
	BMP085_REGISTER_PRESSUREDATA    = 0xF6,
	BMP085_REGISTER_READTEMPCMD     = 0x2E,
	BMP085_REGISTER_READPRESSURECMD = 0x34
};

typedef enum {
	BMP085_MODE_ULTRALOWPOWER          = 0,
	BMP085_MODE_STANDARD               = 1,
	BMP085_MODE_HIGHRES                = 2,
	BMP085_MODE_ULTRAHIGHRES           = 3
} bmp085_mode_t;

typedef struct {
	int16_t  ac1;
	int16_t  ac2;
	int16_t  ac3;
	uint16_t ac4;
	uint16_t ac5;
	uint16_t ac6;
	int16_t  b1;
	int16_t  b2;
	int16_t  mb;
	int16_t  mc;
	int16_t  md;
} bmp085_calib_data;

enum bmp085_state_e {
	BMP085_STATE_DOWN = 0,
	BMP085_STATE_UP,
	BMP085_STATE_IDLE,
	BMP085_STATE_RAW_TEMP_MEASURE,
	BMP085_STATE_RAW_PRESSURE_MEASURE,
	BMP085_STATE_CALCULATE_VALUES
};
static enum bmp085_state_e bmp085_state = BMP085_STATE_DOWN; // initial state

static bmp085_calib_data _bmp085_coeffs;   // Last read accelerometer data will be available here

static uint8_t           _bmp085Mode = 	BMP085_MODE_ULTRAHIGHRES;

/**************************************************************************/
/*!
    @brief  Writes an 8 bit value over I2C
*/
/**************************************************************************/
static unsigned char writeCommand(byte reg, byte value)
{
	unsigned char buff[2];
	buff[0] = reg;
	buff[1] = value;

	return i2c_write(BMP085_ADDRESS, buff, 2);
}

/**************************************************************************/
/*!
    @brief  Reads an 8 bit value over I2C
*/
/**************************************************************************/
static unsigned char read8(byte reg, uint8_t *value)
{
	// send the request
	if (0 != i2c_write(BMP085_ADDRESS, &reg, 1)) return 1;

	// read the reply
	return i2c_read(BMP085_ADDRESS, value, 1);
}

/**************************************************************************/
/*!
    @brief  Reads a 16 bit value over I2C
*/
/**************************************************************************/
static unsigned char read16(byte reg, uint16_t *value)
{
	unsigned char buff[2];

	// send the request
	if (0 != i2c_write(BMP085_ADDRESS, &reg, 1)) return 1;

	if (0 != i2c_read(BMP085_ADDRESS, buff, 2)) return 1;

	*value = (buff[0] << 8) | buff[1];

	return 0;

}

/**************************************************************************/
/*!
    @brief  Reads a signed 16 bit value over I2C
*/
/**************************************************************************/
static unsigned char readS16(byte reg, int16_t *value)
{
	uint16_t i;
	if (0 != read16(reg, &i)) return 1;

	*value = (int16_t)i;
	return 0;
}

static unsigned char readCoefficients(void)
{
	if (0 != readS16(BMP085_REGISTER_CAL_AC1, &_bmp085_coeffs.ac1)) {
		return 1;
	}

	if (0 != readS16(BMP085_REGISTER_CAL_AC2, &_bmp085_coeffs.ac2)) {
		return 1;
	}

	if (0 != readS16(BMP085_REGISTER_CAL_AC3, &_bmp085_coeffs.ac3)) {
		return 1;
	}

	if (0 != read16(BMP085_REGISTER_CAL_AC4, &_bmp085_coeffs.ac4)) {
		return 1;
	}

	if (0 != read16(BMP085_REGISTER_CAL_AC5, &_bmp085_coeffs.ac5)) {
		return 1;
	}

	if (0 != read16(BMP085_REGISTER_CAL_AC6, &_bmp085_coeffs.ac6)) {
		return 1;
	}

	if (0 != readS16(BMP085_REGISTER_CAL_B1, &_bmp085_coeffs.b1)) {
		return 1;
	}

	if (0 != readS16(BMP085_REGISTER_CAL_B2, &_bmp085_coeffs.b2)) {
		return 1;
	}

	if (0 != readS16(BMP085_REGISTER_CAL_MB, &_bmp085_coeffs.mb)) {
		return 1;
	}

	if (0 != readS16(BMP085_REGISTER_CAL_MC, &_bmp085_coeffs.mc)) {
		return 1;
	}

	if (0 != readS16(BMP085_REGISTER_CAL_MD, &_bmp085_coeffs.md)) {
		return 1;
	}

	return 0;
}

static void getPressure(int32_t rawTemperature, int32_t rawPressure,
			float *compTemperature, float *compPressure)
{
	int32_t ut = 0, up = 0, compp = 0;
	int32_t  x1, x2, b5, b6, x3, b3, p;
	uint32_t b4, b7;

	/* Get the raw pressure and temperature values */
	ut = rawTemperature;
	up = rawPressure;

	/* Temperature compensation */
	x1 = (ut - (int32_t)(_bmp085_coeffs.ac6)) * ((int32_t)(_bmp085_coeffs.ac5)) / pow(2,15);
	x2 = ((int32_t)(_bmp085_coeffs.mc * pow(2,11))) / (x1 + (int32_t)(_bmp085_coeffs.md));

	b5 = x1 + x2;

	*compTemperature = (b5 + 8) / pow(2, 4);
	*compTemperature /= 10;

	/* Pressure compensation */
	b6 = b5 - 4000;
	x1 = (_bmp085_coeffs.b2 * ((b6 * b6) >> 12)) >> 11;
	x2 = (_bmp085_coeffs.ac2 * b6) >> 11;
	x3 = x1 + x2;
	b3 = (((((int32_t) _bmp085_coeffs.ac1) * 4 + x3) << _bmp085Mode) + 2) >> 2;
	x1 = (_bmp085_coeffs.ac3 * b6) >> 13;
	x2 = (_bmp085_coeffs.b1 * ((b6 * b6) >> 12)) >> 16;
	x3 = ((x1 + x2) + 2) >> 2;
	b4 = (_bmp085_coeffs.ac4 * (uint32_t) (x3 + 32768)) >> 15;
	b7 = ((uint32_t) (up - b3) * (50000 >> _bmp085Mode));

	if (b7 < 0x80000000) {
		p = (b7 << 1) / b4;
	} else {
		p = (b7 / b4) << 1;
	}

	x1 = (p >> 8) * (p >> 8);
	x1 = (x1 * 3038) >> 16;
	x2 = (-7357 * p) >> 16;
	compp = p + ((x1 + x2 + 3791) >> 4);

	*compPressure = compp;
}

static unsigned long bmp085_task(unsigned char reg, unsigned long expiration)
{
	uint8_t id;
	uint16_t t;
	uint8_t  p8;
	uint16_t p16;
	int32_t  p32;
	static int32_t rawTemp, rawPressure;
	float temp, pressure;
	unsigned long due = BMP085_TIMEOUT_DEFAULT_MS;
	Register rp = State::get(STATE_BMP085_PRESSURE);
	Register rt = State::get(STATE_BMP085_TEMP);

	switch (bmp085_state) {
	case BMP085_STATE_DOWN:
		rp.disable();
		rt.disable();

		// poll chip-id
		if (!((0 == read8(BMP085_REGISTER_CHIPID, &id)) &&
		      (id == 0x55))) {
			due = BMP085_TIMEOUT_POLL_MS;
			goto error;
		}

		// go to the next step
		bmp085_state = BMP085_STATE_UP;
		break;

	case BMP085_STATE_UP:
		if (0 != readCoefficients()) {
			goto error;
		}

		// go to the next step
		bmp085_state = BMP085_STATE_IDLE;
		break;

	case BMP085_STATE_IDLE:
		// start raw temperature measure
		if (0 != writeCommand(BMP085_REGISTER_CONTROL,
				      BMP085_REGISTER_READTEMPCMD)) {
			goto error;
		}

		// go to the next step
		bmp085_state = BMP085_STATE_RAW_TEMP_MEASURE;
		break;

	case BMP085_STATE_RAW_TEMP_MEASURE:
		// read the measured temperature
		if (0 != read16(BMP085_REGISTER_TEMPDATA, &t)) {
			goto error;
		}

		rawTemp = t;

		// start pressure measure
		if (0 != writeCommand(BMP085_REGISTER_CONTROL,
				      BMP085_REGISTER_READPRESSURECMD +
				      (_bmp085Mode << 6))) {
			goto error;
		}

		// go to the next step
		bmp085_state = BMP085_STATE_RAW_PRESSURE_MEASURE;
		break;

	case BMP085_STATE_RAW_PRESSURE_MEASURE:
		if (0 != read16(BMP085_REGISTER_PRESSUREDATA, &p16)) {
			goto error;
		}

		p32 = (uint32_t)p16 << 8;

		if (0 != read8(BMP085_REGISTER_PRESSUREDATA + 2, &p8)) {
			goto error;
		}

		p32 += p8;
		p32 >>= (8 - _bmp085Mode);

		rawPressure = p32;

		// go to the next step
		bmp085_state = BMP085_STATE_CALCULATE_VALUES;

		break;

	case BMP085_STATE_CALCULATE_VALUES:
		getPressure(rawTemp, rawPressure, &temp, &pressure);

		rt.update((temp + 0.005) * 100);
		rp.update((pressure + 5) / 10.0F);

		// go to the next step
		bmp085_state = BMP085_STATE_IDLE;

		due = BMP085_TIMEOUT_NEXT_MEASURE_MS;

		break;
	}

	return due; // normal termination
error:
	bmp085_state = BMP085_STATE_DOWN;

	return due; // error termination
}

#ifdef BMP085_PRESSURE
static float pressureToAltitude(float seaLevel, float atmospheric, float temp)
{
  /* Hyposometric formula:                      */
  /*                                            */
  /*     ((P0/P)^(1/5.257) - 1) * (T + 273.15)  */
  /* h = -------------------------------------  */
  /*                   0.0065                   */
  /*                                            */
  /* where: h   = height (in meters)            */
  /*        P0  = sea-level pressure (in hPa)   */
  /*        P   = atmospheric pressure (in hPa) */
  /*        T   = temperature (in °C)           */

  return (((float)pow((seaLevel/atmospheric), 0.190223F) - 1.0F)
         * (temp + 273.15F)) / 0.0065F;
}
#endif

void bmp085_setup(void)
{
	task_add(bmp085_task, STATE_LAST, BMP085_TIMEOUT_DEFAULT_MS);
}
