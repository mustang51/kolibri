// -*- mode: c++; c-file-style: "linux" -*-

#ifndef __PIN_H__
#define __PIN_H__

#include "registers.h"
#include "task_queue.h"

enum {
	PIN_D4 = 0,
	PIN_D7,
	PIN_D11,
	PIN_D12,
	PIN_A0,
	PIN_A1,
	PIN_A2,
	PIN_A3,
	PIN_A6,
	PIN_A7,
	PIN_MAX
};

// mode selection registers

//
// 15         12           8   7      5     4       3        2       1     0
// +----------+------------+   +------+-----+-------+--------+-------+-----+
// |   RSV    |   TARGET   |   | TYPE | INV | OUTPU | REMOTE | LOCAL | ENA |
// |   (4)    |    (4)     |   | (3)  | (1) |  (1)  |  OFF(1)| OFF(1)| (1) |
// +----------+------------+   +------+-----+-------+--------+-------+-----+
//
// ENA        : register is enabled
// LOCAL_OVER : output is not commandable locally
// REMOTE_OVER: output is not commandable remotely
// OUTPUT     : is input or output?
// INV        : inverted
// TYPE       : behaviour of the input
// TARGET     : destination of the input
// RSV        : reserved

enum {
	REGISTER_MODE_TYPE_METER = 0,
	REGISTER_MODE_TYPE_BUTTON,
	REGISTER_MODE_TYPE_SWITCH,
	REGISTER_MODE_TYPE_ANALOG,
	REGISTER_MODE_TYPE_ANALOG_SWITCH
};

enum {
	REGISTER_MODE_TARGET_NULL = 0,
	REGISTER_MODE_TARGET_CURTAINS,
	REGISTER_MODE_TARGET_CURTAINS_NORMAL,
	REGISTER_MODE_TARGET_CURTAINS_REVERSE,
	REGISTER_MODE_TARGET_AC1,
	REGISTER_MODE_TARGET_AC2,
	REGISTER_MODE_TARGET_AC3,
	REGISTER_MODE_TARGET_ALL_ACS
};

union PinMode {
	struct {
		/* lower byte */
		unsigned char is_enabled      : 1;
		unsigned char local_override  : 1;
		unsigned char remote_override : 1;
		unsigned char is_output       : 1;
		unsigned char is_inverted     : 1;
		unsigned char type            : 3;

		/* upper byte */
		unsigned char target          : 4;
		unsigned char reserved        : 4;
	} as_mode;
	unsigned int as_uint;
};

class LastValues
{
public:
	LastValues() : cnt_(0), values_(0) {};
	~LastValues() {};

	void push_analog(unsigned int value);
	void push_digital(unsigned int value);

	bool all_high();
	bool all_low();

	void clear();

	unsigned int last();
private:
	unsigned char cnt_;
	unsigned char values_;
};

class Pin
{
public:
	static const unsigned int ANALOG_THRESHOLD_HIGH = 900;
	static const unsigned int ANALOG_THRESHOLD_LOW = 123;

	// read from eeprom the config for all pins and apply it
	static void setup();

	unsigned int get_value() { return values_[pin_id_]; };

	void set_value(unsigned int v) { values_[pin_id_] = v; };

	// reads from eeprom the configured mode for the pin
	union PinMode get_mode();

	// stores in eeprom the configured mode for the pin
	bool set_mode(union PinMode pm);
	bool set_mode(unsigned int value);

	// attempt to modify the mode of a pin
	bool command_mode(union PinMode value, enum command_source src);

	bool command(unsigned int value, enum command_source src);

	// given the modbus register, returns the pin
	static unsigned char reg_to_pin(unsigned char reg);
	static unsigned char reg_mode_to_pin(unsigned char reg);

	// given the pin, returns the modbus register
	static unsigned char pin_to_reg(unsigned char pin);

protected:
	unsigned char pin_id_;

	static unsigned int values_[PIN_MAX];

	static LastValues last_values_[PIN_MAX];

	// Pin is a base class, it should not be created from the outside
	Pin(unsigned char pin_id) { this->pin_id_ = pin_id; };
	~Pin() { };

	// returns the physical pin (for arduino commands)
	unsigned char phy_pin();

	unsigned int eeprom_address();

	LastValues & last() { return last_values_[pin_id_]; };

	bool is_valid_output_pin();

	unsigned long get_refresh_time();

	unsigned int invert_value(unsigned int value);

	bool is_analog();

	task_callback get_task();

	void push_value(unsigned int value, bool do_filter);
};

class AnalogPin : public Pin
{
public:
	AnalogPin(unsigned char id);

	static unsigned long task(unsigned char pin_id,
				  unsigned long expiration);
};

class DigitalPin : public Pin
{
public:
	DigitalPin(unsigned char id);

	static unsigned long task(unsigned char pin_id,
				  unsigned long expiration);

};

#endif
