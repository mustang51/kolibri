// -*- mode: c++; c-file-style: "linux" -*-

#include "registers.h"
#include "eeprom.h"

#include "state.h"
#include "commands.h"
#include "pin.h"

#include <avr/pgmspace.h>

#define PARAMS_STRING_MAX_LEN 26

PROGMEM const char register_names[][PARAMS_STRING_MAX_LEN] = {
        PARAM_LIST(STRINGY)
};

PROGMEM const char params_speed_name[][8] = {
	"9600",
	"19200",
	"38400",
	"57600",
	"115200",
	"230400",
	"460800"
};

unsigned int Register::value(void)
{
	unsigned int value;

	switch(reg) {
	case STATE_REGISTER_COUNT:
		value = STATE_LAST;
		break;
	case STATE_DEVICE_ID:
		eeprom_load(EEPROM_DEVICE_ID, &value);
		break;
	case STATE_SERIAL_SPEED:
		eeprom_load(EEPROM_SERIAL_SPEED, &value);
		break;
	case STATE_SW_VERSION:
		value = GIT_VERSION;
		break;
	case STATE_RESET:
		value = 0;
		break;
	case STATE_PWM_DUTY:
		eeprom_load(EEPROM_PWM_DUTY, &value);
		break;
	case STATE_RELAY_OPEN_TIME:
		eeprom_load(EEPROM_RELAY_OPEN_TIME, &value);
		break;
	case STATE_INPUT_POLL_TIME:
		eeprom_load(EEPROM_INPUT_POLL_TIME, &value);
		break;
	case STATE_INPUT_FILTER_CNT:
		eeprom_load(EEPROM_INPUT_FILTER_CNT, &value);
		break;
	case STATE_VCC_CALIB_0:
		eeprom_load(EEPROM_VCC_CALIB_0, &value);
		break;
	case STATE_VCC_CALIB_1:
		eeprom_load(EEPROM_VCC_CALIB_1, &value);
		break;
	case STATE_CURTAINS_TRAVEL1_UP:
		eeprom_load(EEPROM_CURTAINS_TRAVEL1_UP, &value);
		break;
	case STATE_CURTAINS_TRAVEL1_DOWN:
		eeprom_load(EEPROM_CURTAINS_TRAVEL1_DOWN, &value);
		break;
	case STATE_CURTAINS_TRAVEL2_UP:
		eeprom_load(EEPROM_CURTAINS_TRAVEL2_UP, &value);
		break;
	case STATE_CURTAINS_TRAVEL2_DOWN:
		eeprom_load(EEPROM_CURTAINS_TRAVEL2_DOWN, &value);
		break;
	case STATE_BUZZER:
		value = 0;
		break;
	case STATE_DIGITAL_MODE_D4:
	case STATE_DIGITAL_MODE_D7:
	case STATE_DIGITAL_MODE_D11:
	case STATE_DIGITAL_MODE_D12: {
		DigitalPin p(Pin::reg_to_pin(reg));
		value = p.get_mode().as_uint;
		break;
	}
	case STATE_ANALOG_MODE_A0:
	case STATE_ANALOG_MODE_A1:
	case STATE_ANALOG_MODE_A2:
	case STATE_ANALOG_MODE_A3:
	case STATE_ANALOG_MODE_A6:
	case STATE_ANALOG_MODE_A7: {
		AnalogPin p(Pin::reg_to_pin(reg));
		value = p.get_mode().as_uint;
		break;
	}
	case STATE_UPTIME_HIGH:
		value = values[REGVAL_UPTIME_HIGH];
		break;
	case STATE_UPTIME_LOW:
		value = values[REGVAL_UPTIME_LOW];
		break;
	case STATE_LATENCY_100NS:
		value = values[REGVAL_LATENCY_100NS];
		break;
	case STATE_LATENCY_MS_MAX:
		value = values[REGVAL_LATENCY_MS_MAX];
		break;
	case STATE_EEPROM_WRITES:
		eeprom_load(EEPROM_WRITES, &value);
		break;
	case STATE_EEPROM_ERRORS:
		value = values[REGVAL_EEPROM_ERRORS];
		break;
	case STATE_VCC:
		value = values[REGVAL_VCC];
		break;
	case STATE_TEMP:
		value = values[REGVAL_TEMP];
		break;
	case STATE_FREE_MEM:
		value = values[REGVAL_FREE_MEM];
		break;
	case STATE_WATCHDOG:
		value = values[REGVAL_WATCHDOG];
		break;
	case STATE_CURTAINS_POSITION1:
		value = values[REGVAL_CURTAINS_POSITION1];
		break;
	case STATE_CURTAINS_POSITION2:
		value = values[REGVAL_CURTAINS_POSITION2];
		break;
	case STATE_MODBUS_MSG_SEC:
		value = values[REGVAL_MODBUS_MSG_SEC];
		break;
	case STATE_MODBUS_ERRORS:
		value = values[REGVAL_MODBUS_ERRORS];
		break;
	case STATE_MODBUS_COMMANDS:
		value = values[REGVAL_MODBUS_COMMANDS];
		break;
	case STATE_INPUT_COMMANDS:
		value = values[REGVAL_INPUT_COMMANDS];
		break;
	case STATE_CURTAINS_DESIRED1:
		value = values[REGVAL_CURTAINS_DESIRED1];
		break;
	case STATE_CURTAINS_DESIRED2:
		value = values[REGVAL_CURTAINS_DESIRED2];
		break;
	case STATE_RELAY_CURTAINS1:
	case STATE_RELAY_CURTAINS2:
	case STATE_RELAY_AC1:
	case STATE_RELAY_AC2:
	case STATE_RELAY_AC3:
		value = values[REGVAL_RELAY_CURTAINS1 + reg -
			       STATE_RELAY_CURTAINS1];
		break;
	case STATE_DIGITAL_D4:
	case STATE_DIGITAL_D7:
	case STATE_DIGITAL_D11:
	case STATE_DIGITAL_D12: {
		DigitalPin p(Pin::reg_to_pin(reg));
		value = p.get_value();
		break;
	}
	case STATE_ANALOG_A0:
	case STATE_ANALOG_A1:
	case STATE_ANALOG_A2:
	case STATE_ANALOG_A3:
	case STATE_ANALOG_A6:
	case STATE_ANALOG_A7: {
		AnalogPin p(Pin::reg_to_pin(reg));
		value = p.get_value();
		break;
	}
	case STATE_BH1750_HIGH:
		value = values[REGVAL_BH1750_HIGH];
		break;
	case STATE_BH1750_LOW:
		value = values[REGVAL_BH1750_LOW];
		break;
	case STATE_BMP085_PRESSURE:
		value = values[REGVAL_BMP085_PRESSURE];
		break;
	case STATE_BMP085_TEMP:
		value = values[REGVAL_BMP085_TEMP];
		break;
	case STATE_SHT2X_HUMIDITY:
		value = values[REGVAL_SHT2X_HUMIDITY];
		break;
	case STATE_SHT2X_TEMP:
		value = values[REGVAL_SHT2X_TEMP];
		break;
	case STATE_DBG_1:
	case STATE_DBG_2:
	case STATE_DBG_3:
	case STATE_DBG_4:
		value = values[REGVAL_DBG_1 + reg - STATE_DBG_1];

		break;
	}

	return value;
}

void Register::name(char* buff)
{
	strcpy_P(buff, (char*) register_names[reg]);
}

unsigned int Register::name_len()
{
	return strlen_P((char*) register_names[reg]);
}

bool Register::update(unsigned int value)
{
	if (value > STATE_MAX) {
		value = STATE_MAX;
	}

	return __update(value);
}

bool Register::disable()
{
	return __update(STATE_UNK);
}

bool Register::__update(unsigned int value)
{
	bool b = true;

	switch(reg) {
	case STATE_REGISTER_COUNT:
		return false; // error

	case STATE_DEVICE_ID:
		b = eeprom_store(EEPROM_DEVICE_ID, value);
		break;
	case STATE_SERIAL_SPEED:
		b = eeprom_store(EEPROM_SERIAL_SPEED, value);
		break;
	case STATE_SW_VERSION:
		return false; // error
	case STATE_PWM_DUTY:
		b = eeprom_store(EEPROM_PWM_DUTY, value);
		break;
	case STATE_RELAY_OPEN_TIME:
		b = eeprom_store(EEPROM_RELAY_OPEN_TIME, value);
		break;
	case STATE_INPUT_POLL_TIME:
		b = eeprom_store(EEPROM_INPUT_POLL_TIME, value);
		break;
	case STATE_INPUT_FILTER_CNT:
		b = eeprom_store(EEPROM_INPUT_FILTER_CNT, value);
		break;
	case STATE_VCC_CALIB_0:
		b = eeprom_store(EEPROM_VCC_CALIB_0, value);
		break;
	case STATE_VCC_CALIB_1:
		b = eeprom_store(EEPROM_VCC_CALIB_1, value);
		break;
	case STATE_CURTAINS_TRAVEL1_UP:
		b = eeprom_store(EEPROM_CURTAINS_TRAVEL1_UP, value);
		break;
	case STATE_CURTAINS_TRAVEL1_DOWN:
		b = eeprom_store(EEPROM_CURTAINS_TRAVEL1_DOWN, value);
		break;
	case STATE_CURTAINS_TRAVEL2_UP:
		b = eeprom_store(EEPROM_CURTAINS_TRAVEL2_UP, value);
		break;
	case STATE_CURTAINS_TRAVEL2_DOWN:
		b = eeprom_store(EEPROM_CURTAINS_TRAVEL2_DOWN, value);
		break;
	case STATE_DIGITAL_MODE_D4:
	case STATE_DIGITAL_MODE_D7:
	case STATE_DIGITAL_MODE_D11:
	case STATE_DIGITAL_MODE_D12: {
		DigitalPin p(Pin::reg_to_pin(reg));
		union PinMode pm;
		pm.as_uint = value;
		b = p.set_mode(pm);
		break;
	}

	case STATE_ANALOG_MODE_A0:
	case STATE_ANALOG_MODE_A1:
	case STATE_ANALOG_MODE_A2:
	case STATE_ANALOG_MODE_A3:
	case STATE_ANALOG_MODE_A6:
	case STATE_ANALOG_MODE_A7: {
		AnalogPin p(Pin::reg_to_pin(reg));
		union PinMode pm;
		pm.as_uint = value;
		b = p.set_mode(pm);
		break;
	}
	case STATE_UPTIME_HIGH:
		values[REGVAL_UPTIME_HIGH] = value;
		break;
	case STATE_UPTIME_LOW:
		values[REGVAL_UPTIME_LOW] = value;
		break;
	case STATE_LATENCY_100NS:
		values[REGVAL_LATENCY_100NS] = value;
		break;
	case STATE_LATENCY_MS_MAX:
		values[REGVAL_LATENCY_MS_MAX] = value;
		break;
	case STATE_EEPROM_WRITES:
		b = eeprom_store(EEPROM_WRITES, value);
		break;
	case STATE_EEPROM_ERRORS:
		values[REGVAL_EEPROM_ERRORS] = value;
		break;
	case STATE_VCC:
		values[REGVAL_VCC] = value;
		break;
	case STATE_TEMP:
		values[REGVAL_TEMP] = value;
		break;
	case STATE_FREE_MEM:
		values[REGVAL_FREE_MEM] = value;
		break;
	case STATE_WATCHDOG:
		values[REGVAL_WATCHDOG] = value;
		break;
	case STATE_CURTAINS_POSITION1:
		values[REGVAL_CURTAINS_POSITION1] = value;
		break;
	case STATE_CURTAINS_POSITION2:
		values[REGVAL_CURTAINS_POSITION2] = value;
		break;
	case STATE_MODBUS_MSG_SEC:
		values[REGVAL_MODBUS_MSG_SEC] = value;
		break;
	case STATE_MODBUS_ERRORS:
		values[REGVAL_MODBUS_ERRORS] = value;
		break;
	case STATE_MODBUS_COMMANDS:
		values[REGVAL_MODBUS_COMMANDS] = value;
		break;
	case STATE_INPUT_COMMANDS:
		values[REGVAL_INPUT_COMMANDS] = value;
		break;
	case STATE_CURTAINS_DESIRED1:
		values[REGVAL_CURTAINS_DESIRED1] = value;
		break;
	case STATE_CURTAINS_DESIRED2:
		values[REGVAL_CURTAINS_DESIRED2] = value;
		break;
	case STATE_RELAY_CURTAINS1:
	case STATE_RELAY_CURTAINS2:
	case STATE_RELAY_AC1:
	case STATE_RELAY_AC2:
	case STATE_RELAY_AC3:
		values[REGVAL_RELAY_CURTAINS1 + reg -
		       STATE_RELAY_CURTAINS1] = value;
		break;
	case STATE_DIGITAL_D4:
	case STATE_DIGITAL_D7:
	case STATE_DIGITAL_D11:
	case STATE_DIGITAL_D12:{
		DigitalPin p(Pin::reg_to_pin(reg));
		p.set_value(value);
		break;
	}
	case STATE_ANALOG_A0:
	case STATE_ANALOG_A1:
	case STATE_ANALOG_A2:
	case STATE_ANALOG_A3:
	case STATE_ANALOG_A6:
	case STATE_ANALOG_A7:{
		AnalogPin p(Pin::reg_to_pin(reg));
		p.set_value(value);
		break;
	}
	case STATE_BH1750_HIGH:
		values[REGVAL_BH1750_HIGH] = value;
		break;
	case STATE_BH1750_LOW:
		values[REGVAL_BH1750_LOW] = value;
		break;
	case STATE_BMP085_PRESSURE:
		values[REGVAL_BMP085_PRESSURE] = value;
		break;
	case STATE_BMP085_TEMP:
		values[REGVAL_BMP085_TEMP] = value;
		break;
	case STATE_SHT2X_HUMIDITY:
		values[REGVAL_SHT2X_HUMIDITY] = value;
		break;
	case STATE_SHT2X_TEMP:
		values[REGVAL_SHT2X_TEMP] = value;
		break;
	case STATE_DBG_1:
	case STATE_DBG_2:
	case STATE_DBG_3:
	case STATE_DBG_4:
		values[REGVAL_DBG_1 + reg - STATE_DBG_1] = value;

		break;
	}

	return b;
}

bool Register::command(unsigned int value, enum command_source src)
{
	bool b = true;

	// call the register handler if any
	switch (reg) {
	case STATE_PWM_DUTY:
		b = pwm_duty_command(this, value, src);
		break;
	case STATE_RESET:
		b = reset_command(this, value, src);
		break;
	case STATE_BUZZER:
		b = buzzer_command(this, value, src);
		break;
	case STATE_DIGITAL_MODE_D4:
	case STATE_DIGITAL_MODE_D7:
	case STATE_DIGITAL_MODE_D11:
	case STATE_DIGITAL_MODE_D12: {
		DigitalPin p(Pin::reg_to_pin(reg));
		union PinMode pm;
		pm.as_uint = value;
		b = p.command_mode(pm, SRC_REMOTE);
		break;
	}
	case STATE_ANALOG_MODE_A0:
	case STATE_ANALOG_MODE_A1:
	case STATE_ANALOG_MODE_A2:
	case STATE_ANALOG_MODE_A3:
	case STATE_ANALOG_MODE_A6:
	case STATE_ANALOG_MODE_A7: {
		AnalogPin p(Pin::reg_to_pin(reg));
		union PinMode pm;
		pm.as_uint = value;
		b = p.command_mode(pm, SRC_REMOTE);
		break;
	}

	case STATE_CURTAINS_TRAVEL1_UP:
	case STATE_CURTAINS_TRAVEL1_DOWN:
	case STATE_CURTAINS_TRAVEL2_UP:
	case STATE_CURTAINS_TRAVEL2_DOWN:
		b = travel_command(this, value, src);
		break;

	case STATE_LATENCY_MS_MAX:
		b = latency_command(this, value, src);
		break;

	case STATE_DIGITAL_D4:
	case STATE_DIGITAL_D7:
	case STATE_DIGITAL_D11:
	case STATE_DIGITAL_D12: {
		DigitalPin p(Pin::reg_to_pin(reg));
		b = p.command(value, SRC_REMOTE);
		break;
	}

	case STATE_ANALOG_A0:
	case STATE_ANALOG_A1:
	case STATE_ANALOG_A2:
	case STATE_ANALOG_A3:
	case STATE_ANALOG_A6:
	case STATE_ANALOG_A7: {
		AnalogPin p(Pin::reg_to_pin(reg));
		b = p.command(value, SRC_REMOTE);
		break;
	}

	case STATE_SHT2X_TEMP:
	case STATE_SHT2X_HUMIDITY:
		b = sht2x_command(this, value, src);
		break;

	case STATE_DBG_1:
	case STATE_DBG_2:
	case STATE_DBG_3:
	case STATE_DBG_4:
		b = dbg_command(this, value, src);
		break;
	}

	if (true != b) {
		return false; // command returned error
	}

	this->update(value);

	if (SRC_LOCAL == src) {
		STATE_INC(STATE_INPUT_COMMANDS);
	}

	if (SRC_REMOTE == src) {
		STATE_INC(STATE_MODBUS_COMMANDS);
	}

	return true;
}

unsigned int Register::values[REGVAL_MAX];



