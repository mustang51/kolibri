// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>

#include "commands.h"
#include "state.h"
#include "tasks.h"
#include "task_queue.h"
#include "buzzer.h"
#include "sht2x.h"
#include "relay.h"

bool reset_command(class Register *r, unsigned int value,
		   enum command_source src)
{
	// arm a timer to trigger the reset, that way we will
	// answer the modbus server politely and die afterwards
	task_add(reset_task, STATE_LAST, 0);

	return true;
}

bool buzzer_command(class Register *r, unsigned int value,
		    enum command_source src)
{
	unsigned char ring_duration;
	unsigned char silent_duration;
	unsigned char count;
	// decode each part of the value

	count           = value & 0x000F;
	ring_duration   = (value & 0x00F0) >> 4;
	silent_duration = (value & 0x0F00) >> 8;

	if (0 == count) { // kill the buzzer
		buzzer_silence();
	} else {
		buzzer_ring(ring_duration, silent_duration, count);
	}

	return true;
}

bool latency_command(class Register *r, unsigned int value,
		     enum command_source src)
{
	return State::get(STATE_LATENCY_MS_MAX).update(value);
}

bool pwm_duty_command(class Register* r, unsigned int value,
		      enum command_source src)
{
	relay_refresh_duty_cycle();

	return true;
}


bool dbg_command(class Register* r, unsigned int value,
		 enum command_source src)
{
	switch (r->num()) {
	case STATE_DBG_1:
		break;
	case STATE_DBG_2:
		break;
	case STATE_DBG_3:
		break;
	case STATE_DBG_4:
		break;
	}

	return true;
}

bool sht2x_command(class Register* r, unsigned int value,
		   enum command_source src)
{
	switch (value) {
	case 1:
		sht2x_reset();
		break;
	case 2:
		sht2x_heater(true);
		break;
	case 3:
		sht2x_heater(false);
		break;
	}

	return true;
}

bool travel_command(class Register* r, unsigned int value,
		    enum command_source src)
{
	relay_refresh_travels();

	return true;
}
