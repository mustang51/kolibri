// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>

#include <avr/wdt.h>

#include "task_queue.h"
#include "heartbeat.h"
#include "state.h"
#include "misc.h"

#define BEAT_REFRESH_MILLIS   100

#define BEAT_WAITING_MILLIS  1000
#define BEAT_SISTOLIC_MILLIS  100
#define BEAT_PAUSE_MILLIS     100
#define BEAT_DIASTOLIC_MILLIS 100

enum {
	BEAT_WAITING = 0,
	BEAT_SISTOLIC,
	BEAT_PAUSE,
	BEAT_DIASTOLIC
};

#define LED_PIN 13

static unsigned long heartbeat_task(unsigned char c, unsigned long now)
{
	static unsigned char beating_stage = BEAT_WAITING;
	static unsigned long next_millis = 0;

	if (0 == State::get(STATE_WATCHDOG).value()) {
		wdt_reset();
	}

	if (! time_expired_from(now, next_millis)) {
		return BEAT_REFRESH_MILLIS;
	}

	/* cycle the state-machine */
	switch (beating_stage) {
	case BEAT_WAITING:
		digitalWrite(LED_PIN, HIGH);

		next_millis = now + BEAT_SISTOLIC_MILLIS;

		beating_stage = BEAT_SISTOLIC;
		break;
	case BEAT_SISTOLIC:
		digitalWrite(LED_PIN, LOW);

		if (State::get(STATE_MODBUS_MSG_SEC).value() > 0) {
			/* if we have I/O do the full beat, otherwise
			   just a blink */
			next_millis = now + BEAT_PAUSE_MILLIS;

			beating_stage = BEAT_PAUSE;
		} else {
			next_millis = now + BEAT_WAITING_MILLIS;

			beating_stage = BEAT_WAITING;
		}

		break;
	case BEAT_PAUSE:
		digitalWrite(LED_PIN, HIGH);

		next_millis = now + BEAT_DIASTOLIC_MILLIS;

		beating_stage = BEAT_DIASTOLIC;
		break;
	case BEAT_DIASTOLIC:
		digitalWrite(LED_PIN, LOW);

		next_millis = now + BEAT_WAITING_MILLIS;

		beating_stage = BEAT_WAITING;
		break;
	}

	return BEAT_REFRESH_MILLIS;
}

void reset_board(void)
{
	void (*rf)(void) = 0; //declare reset function @ address 0

	rf(); // execute position cero, effectively starting over
}

void heartbeat_setup(void)
{
	pinMode(LED_PIN, OUTPUT);

	wdt_enable(WDTO_2S); // have the wdt reset the chip
	wdt_reset();

	task_add(heartbeat_task, 0, BEAT_REFRESH_MILLIS);
}

