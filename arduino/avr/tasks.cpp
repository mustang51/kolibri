// -*- mode: c++; c-file-style: "linux" -*-

#include "tasks.h"
#include "state.h"

#include "pin.h"
#include "heartbeat.h"
#include "modbus.h"
#include "task_queue.h"
#include "bh1750.h"
#include "bmp085.h"
#include "sht2x.h"

#define SECOND_MS 1000
#define VCC_REFRESH_MS		2000
#define TEMP_REFRESH_MS		10000
#define FREE_MEM_REFRESH_MS	100
#define MODBUS_DELAY_MS         1
#define LOAD_TASK_MS            1

// wait for comparator voltages stabilization
#define ANALOG_COMPARATOR_MS    10

// #define BENCHMARK_TIMERS

unsigned long reset_task(unsigned char reg, unsigned long expiration)
{
	reset_board();
	return 0;
}

static unsigned long modbus_task(unsigned char reg, unsigned long expiration)
{
	if (true == modbus_poll()) {
		modbus_parse();
	}

	return MODBUS_DELAY_MS;
}

static unsigned long temp_task(unsigned char reg, unsigned long expiration)
{
	static bool stabilising = false;
	unsigned int temp;
	State& s = State::get();

	if (!stabilising) {
		// avoid race condition with vcc and temp tasks by
		// postponing this task some time
		if (true == s.analog_comparator_lock_) {
			return ANALOG_COMPARATOR_MS;
		}

		// start conversion
		// Read temperature sensor against 1.1V reference
		ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(MUX3);

		ADCSRA |= _BV(ADEN); // enable ADC
		ADCSRA &= ~_BV(ADATE); // disable ADATE

		stabilising		  = true;
		s.analog_comparator_lock_ = true;

		// wait for the voltages to stabilize
		return ANALOG_COMPARATOR_MS;
	}

	// voltages must be stable now, finish the conversion
	// Start AD conversion
	ADCSRA |= _BV(ADSC);
	// Detect end-of-conversion
	while (bit_is_set(ADCSRA, ADSC));
	// temp raw data
	temp = ADCL | (ADCH << 8);
	// convert it
	temp = ( temp - 324 ) / 1.22;

	STATE_SET(reg, temp);

	stabilising		  = false;
	s.analog_comparator_lock_ = false;

	// rearm task
	return TEMP_REFRESH_MS;
}

#define VCC_SAMPLES 64 // PWM messes with the value read, so ... average!

static unsigned long vcc_task(unsigned char reg, unsigned long expiration)
{
	long vcc = 0;
	unsigned char cnt;
	State& s = State::get();

	static bool stabilising = false;

	// no config? try again next time
	if (0 == State::get(STATE_VCC_CALIB_1).value()) {
		return VCC_REFRESH_MS;
	}

	if (!stabilising) {
		// avoid race condition with vcc and temp tasks by
		// postponing this task some time
		if (true == s.analog_comparator_lock_) {
			return ANALOG_COMPARATOR_MS;
		}

		// Read 1.1V reference against AVcc
		ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);

		ADCSRA |= _BV(ADEN); // enable ADC
		ADCSRA &= ~_BV(ADATE); // disable ADATE

		stabilising		  = true;
		s.analog_comparator_lock_ = true;

		// wait for the voltages to stabilize
		return ANALOG_COMPARATOR_MS;
	}

	// voltages should be stable now, finish the conversion
	for (cnt = 0; cnt < VCC_SAMPLES; cnt++) {
		// Start AD conversion
		ADCSRA |= _BV(ADSC);
		// Detect end-of-conversion
		while (bit_is_set(ADCSRA, ADSC));
		// vcc raw data
		vcc += ADCL | (ADCH << 8);
	}

	vcc /= VCC_SAMPLES;

	// convert it
	vcc = (((- ((long) STATE_GET(STATE_VCC_CALIB_1)) *
		 vcc) / 100) +
	       State::get(STATE_VCC_CALIB_0).value());

	STATE_SET(reg, vcc);

	stabilising		  = false;
	s.analog_comparator_lock_ = false;

	// rearm task
	return VCC_REFRESH_MS;
}

#define FREE_MEM_TRIGGER_RESET 90 // less tan those bytes, reset!

static unsigned long free_mem_task(unsigned char reg, unsigned long expiration)
{
	extern int __heap_start, *__brkval;
	unsigned int free;
	int v;

	free = (int) &v - (__brkval == 0 ?
			   (int) &__heap_start :
			   (int) __brkval);

	// watchdog
	if (free <= FREE_MEM_TRIGGER_RESET) {
		reset_board();
	}

	STATE_SET(reg, free);

	// rearm task
	return FREE_MEM_REFRESH_MS;
}

static unsigned long uptime_task(unsigned char reg, unsigned long expiration)
{
	if (STATE_MAX == STATE_GET(STATE_UPTIME_LOW)) { // next high value
		STATE_INC(STATE_UPTIME_HIGH);
		STATE_SET(STATE_UPTIME_LOW, 0);
	} else {
		STATE_INC(STATE_UPTIME_LOW);
	}

	// refresh modbus message_cnt
	modbus_update_msg_counter();

	// rearm task
	return SECOND_MS;
}

#ifdef BENCHMARK_TIMERS
unsigned long load_task(unsigned char reg, unsigned long expiration)
{
	return LOAD_TASK_MS;
}
#endif

void tasks_setup(void)
{
	task_add(uptime_task, STATE_UPTIME_LOW, SECOND_MS);

	task_add(vcc_task, STATE_VCC, VCC_REFRESH_MS);

	task_add(temp_task, STATE_TEMP, TEMP_REFRESH_MS + 500);

	task_add(free_mem_task, STATE_FREE_MEM, FREE_MEM_REFRESH_MS);

	task_add(modbus_task, STATE_MODBUS_COMMANDS, MODBUS_DELAY_MS);

	sht2x_setup();

	bh1750_setup();

	bmp085_setup();

	STATE_SET(STATE_FREE_MEM, 2048);

#ifdef BENCHMARK_TIMERS
	unsigned char i;

	// test timers performance
	for (i = 0; i < 20; i++) {
		task_add(load_task, 1, LOAD_TASK_MS);
	}
#endif
}
