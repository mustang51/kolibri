// -*- mode: c++; c-file-style: "linux" -*-

#ifndef __TASK_QUEUE_H__
#define __TASK_QUEUE_H__

/**
 * @file   task_queue.h
 * @author Yari Adán Petralanda <mustang@incantations>
 * @date   Sun Apr 14 22:42:33 2013
 *
 * @brief  Implements a queue of tasks.
 *
 *
 */

typedef unsigned long (*task_callback)(unsigned char, unsigned long);

/**
 * Adds a task to the queue. Callback and user will be used as keys
 * for deletion.
 *
 * @param callback This function will be called when the task expires.
 *                 If the callback returns zero, the timer will not be
 *                 rearmed. Otherwise, it will be rearmed for the returned
 *                 value in ms.
 * @param user Will be given as argument to the callback at task expiration.
 * @param when Remaining time in milliseconds when the task will expire.
 */

void task_add(task_callback callback, unsigned char user,
	      unsigned long when);

/**
 * Removes a task from the queue. Any task found with the same
 * callback and user parameters will be removed.
 *
 * @param callback Callback given at task creation.
 * @param user User given at task creation.
 */

void task_cancel(task_callback callback, unsigned char user);

/**
 * Executes any expired tasks.
 *
 */

void task_run(void);

#endif
