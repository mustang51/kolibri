#ifndef __TASKS_H__
#define __TASKS_H__

#include "state.h"

void tasks_setup(void);

unsigned long reset_task(unsigned char reg, unsigned long expiration);

#endif
