// -*- mode: c++; c-file-style: "linux" -*-

#ifndef __BUZZER_H__
#define __BUZZER_H__

#define BUZZER_PIN 8

void buzzer_setup(void);

void buzzer_ring(unsigned char ring, unsigned char silent,
		 unsigned char count);

void buzzer_silence(void);

#endif
