// -*- mode: c++; c-file-style: "linux" -*-

#ifndef __SHT2X_H__
#define __SHT2X_H__

void sht2x_setup();

bool sht2x_heater(bool state);

void sht2x_reset();

#endif
