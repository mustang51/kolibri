// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>
#include <EEPROM.h>

#include "eeprom.h"
#include "registers.h"
#include "state.h"

#define EEPROM_ACTUAL_SIZE 1024
#define EEPROM_WORD_SIZE 4
#define EEPROM_SIZE (EEPROM_ACTUAL_SIZE / EEPROM_WORD_SIZE)

#define READ_EEPROM_WORD(a) (				\
		EEPROM.read(a) |			\
		(EEPROM.read(a + 1) << 8))

#define WRITE_EEPROM_WORD(a,w) do {			\
	EEPROM.write(a, w & 0x00FF);			\
	EEPROM.write(a + 1, ((w & 0xFF00) >> 8));	\
	} while(0)


static bool __eeprom_load(unsigned int address, unsigned int *value)
{
	unsigned int crc;
	unsigned int curr_value;

	curr_value = READ_EEPROM_WORD(address * EEPROM_WORD_SIZE);
	crc        = READ_EEPROM_WORD((address * EEPROM_WORD_SIZE) + 2);

	if (curr_value != ~crc) {
		*value = 0;
		return false; // crc error
	}

	*value = curr_value;

	return true;
}

/**
 *
 *
 * @param address
 * @param value
 *
 * @return
 */


bool eeprom_load(unsigned int address, unsigned int *value)
{
	if (address >= EEPROM_SIZE) {
		return false;
	}

	if (true != __eeprom_load(address, value)) {
		STATE_INC(STATE_EEPROM_ERRORS);
		return false; // crc error
	}

	return true; // everything OK
}

/**
 * Stores the given value in EEPROM. EEPROM addresses store
 * two byte words. Only performs writing when the value
 * is different.
 *
 * @param address Two byte address
 * @param value
 */

bool eeprom_store(unsigned int address, unsigned int value)
{
	unsigned int current_value;
	unsigned int crc;

	if (address >= EEPROM_SIZE) {
		return false;
	}

	// check if value is already written and parity matches
	if ((true == eeprom_load(address, &current_value)) &&
	    (current_value == value)) {
		return true;
	}

	crc = ~value;

	// write the new value and parity
	WRITE_EEPROM_WORD(address * EEPROM_WORD_SIZE, value);
	WRITE_EEPROM_WORD((address * EEPROM_WORD_SIZE) + 2, crc);

	if (address != EEPROM_WRITES) {
		STATE_INC(STATE_EEPROM_WRITES);
	}

	// check the new value is correctly stored
	if (false == eeprom_load(address, &current_value)) {
		return false;
	}

	return (current_value == value);
}
