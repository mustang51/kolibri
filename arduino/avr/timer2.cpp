// -*- mode: c++; c-file-style: "linux" -*-

#include "MsTimer2.h"

static void timer2_interrupt(void)
{

}

#define TIMER2_SPEED_MS 5

void timer2_setup(void)
{
	MsTimer2::set(TIMER2_SPEED_MS, timer2_interrupt);
	MsTimer2::start();
}
