// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>

#include <math.h>

#include "state.h"
#include "sht2x.h"
#include "task_queue.h"
#include "i2c.h"

#define SHT2X_ADDRESS     0x40

#define SHT2X_TIMEOUT_DEFAULT_MS           250
#define SHT2X_TIMEOUT_POLL_MS             5000
#define SHT2X_TIMEOUT_NEXT_MEASURE_MS    10000

#define SHT2X_COMMAND_TEMP_HOLD       0xE3
#define SHT2X_COMMAND_RH_HOLD         0xE5
#define SHT2X_COMMAND_TEMP_NO_HOLD    0xF3
#define SHT2X_COMMAND_RH_NO_HOLD      0xF5
#define SHT2X_COMMAND_READ_USER_REG   0xE7
#define SHT2X_COMMAND_WRITE_USER_REG  0xE6
#define SHT2X_COMMAND_SOFT_RESET      0xFE

#define SHT2X_STATUS_REG_HEATER_MASK  0x04

#define SHT2X_SOFT_RESET_THRESHOLD    200

#define POLYNOMIAL 0x131  //P(x)=x^8+x^5+x^4+1 = 100110001

enum sht2x_state_e {
	SHT2X_STATE_DOWN = 0,
	SHT2X_STATE_IDLE,
	SHT2X_STATE_RAW_TEMP_MEASURE,
	SHT2X_STATE_RAW_HUMIDITY_MEASURE,
	SHT2X_STATE_CALCULATE_VALUES
};

static enum sht2x_state_e sht2x_state = SHT2X_STATE_DOWN; // initial state
static uint8_t sht2x_cnt;

static bool sht2x_check_crc(uint8_t bytes[], uint8_t byteCnt,
			    uint8_t checksum)
{
	uint8_t crc = 0;
	uint8_t byteCtr;

	//calculates 8-Bit checksum with given polynomial
	for (byteCtr = 0; byteCtr < byteCnt; ++byteCtr) {
		crc ^= (bytes[byteCtr]);
		for (uint8_t bit = 8; bit > 0; --bit) {
			if (crc & 0x80) {
				crc = ((uint16_t) crc << 1) ^
					(uint16_t) POLYNOMIAL;
			} else {
				crc = (crc << 1);
			}
		}
	}

	if (crc != checksum) {
		return false;
	}

	return true;
}

static bool sht2x_read_byte(uint8_t *value)
{
	uint8_t buff[2];

	if (0 != i2c_read(SHT2X_ADDRESS, buff, 2)) {
		return false;
	}

	if (true != sht2x_check_crc(buff, 1, buff[1])) {
		return false;
	}

	*value = buff[0];

	return true;
}

static bool sht2x_send_byte(uint8_t byte)
{
	if (0 != i2c_write(SHT2X_ADDRESS, byte)) {
		return false;
	}

	return true;
}

static bool sht2x_read_word(uint16_t *value)
{
	uint8_t buff[3];
	uint16_t result;

	if (0 != i2c_read(SHT2X_ADDRESS, buff, 3)) {
		return 1;
	}

	//Store the result
	result = (buff[0] << 8) | buff[1];

	// checksum
	if (true != sht2x_check_crc(buff, 2, buff[2])) {
		return false;
	}

	result &= ~0x0003;   // clear two low bits (status bits)

	*value = result;

	return true;
}

static bool sht2x_send_word(uint8_t buff[])
{
	if (0 != i2c_write(SHT2X_ADDRESS, buff, 2)) {
		return false;
	}

	return true;
}

static unsigned long sht2x_task(uint8_t reg, unsigned long expiration)
{
	static uint16_t rawHumid, rawTemp;
	unsigned long due = SHT2X_TIMEOUT_DEFAULT_MS;
	float humid, temp;
	Register rh = State::get(STATE_SHT2X_HUMIDITY);
	Register rt = State::get(STATE_SHT2X_TEMP);

	switch (sht2x_state) {
	case SHT2X_STATE_DOWN:
		rh.disable();
		rt.disable();

		sht2x_cnt = 0;

		// probe sensor by trying to disable heater
		if (true != sht2x_heater(false)) {
			goto error;
		}

		// go to the next step
		sht2x_state = SHT2X_STATE_IDLE;
		break;

	case SHT2X_STATE_IDLE:
		// start temperature measure
		if (true != sht2x_send_byte(SHT2X_COMMAND_TEMP_NO_HOLD)) {
			goto error;
		}

		// go to the next step
		sht2x_state = SHT2X_STATE_RAW_TEMP_MEASURE;

		break;

	case SHT2X_STATE_RAW_TEMP_MEASURE:
		// read the temperature measure result
		if (true != sht2x_read_word(&rawTemp)) {
			goto error;
		}

		// start humidity measure
		if (true != sht2x_send_byte(SHT2X_COMMAND_RH_NO_HOLD)) {
			goto error;
		}

		// go to the next step
		sht2x_state = SHT2X_STATE_RAW_HUMIDITY_MEASURE;

		break;

	case SHT2X_STATE_RAW_HUMIDITY_MEASURE:
		// read the temperature measure result
		if (true != sht2x_read_word(&rawHumid)) {
			goto error;
		}

		// go to the next step
		sht2x_state = SHT2X_STATE_CALCULATE_VALUES;

		break;

	case SHT2X_STATE_CALCULATE_VALUES:
		humid = (-6.0   + 125.0  / 65536.0 * (float)rawHumid);
		temp  = (-46.85 + 175.72 / 65536.0 * (float)rawTemp);

		rh.update((humid + 0.005) * 100);
		rt.update((temp  + 0.005) * 100);

		sht2x_cnt++;

		if (sht2x_cnt >= SHT2X_SOFT_RESET_THRESHOLD) {
			goto error;
		}

		// go to the next step
		sht2x_state = SHT2X_STATE_IDLE;

		due = SHT2X_TIMEOUT_NEXT_MEASURE_MS;

		break;
	}

	return due; // normal termination
error:
	sht2x_state = SHT2X_STATE_DOWN;

	// perform reset
	sht2x_send_byte(SHT2X_COMMAND_SOFT_RESET);

	return due; // error termination
}

void sht2x_setup()
{
	task_add(sht2x_task, STATE_LAST, SHT2X_TIMEOUT_DEFAULT_MS);
}

void sht2x_reset()
{
	task_cancel(sht2x_task, STATE_LAST);

	sht2x_send_byte(SHT2X_COMMAND_SOFT_RESET);

	sht2x_state = SHT2X_STATE_DOWN;

	task_add(sht2x_task, STATE_LAST, SHT2X_TIMEOUT_DEFAULT_MS);
}

bool sht2x_heater(bool state)
{
	uint8_t buff[2];

	// read current status register
	if (true != sht2x_send_byte(SHT2X_COMMAND_READ_USER_REG)) {
		return false;
	}

	if (true != sht2x_read_byte(&buff[1])) {
		return false;
	}

	// enable or disable
	if (true == state) {
		buff[1] |= SHT2X_STATUS_REG_HEATER_MASK;
	} else {
		buff[1] &= ~SHT2X_STATUS_REG_HEATER_MASK;
	}

	buff[0] = SHT2X_COMMAND_WRITE_USER_REG;

	// write the new user register
	if (true != sht2x_send_word(buff)) {
		return false;
	}

	return true;
}

