// -*- mode: c++; c-file-style: "linux" -*-

#if 0

#ifndef __PWM_H__
#define __PWM_H__


void pwm_enable(int pin);

void pwm_disable(int pin);

void pwm_setup(void);

#endif

#endif
