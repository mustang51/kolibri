// -*- mode: c++; c-file-style: "linux" -*-

#ifndef __STATE_H__
#define __STATE_H__

#include "registers.h"

class State
{
public:
	static State& get() {
		static State state_;
		return state_;
	};

	static Register get(unsigned char reg);

	void setup();
	void loop();

	bool analog_comparator_lock_;

private:
	State();
	State(State const&);
	void operator = (State const&);

	void show();
	void check();
	unsigned long serial_speed();

	void latency(void);

	unsigned long loop_end_;
	unsigned int loop_last_;
	unsigned long loop_cnt_;
	const unsigned int loop_ms_to_measure_ = 2000;

	const unsigned int default_serial_speed_ = 9600;
};

#define STATE_GET(r) State::get(r).value()
#define STATE_SET(r, v) State::get(r).update(v)
#define STATE_INC(r) State::get(r).update(State::get(r).value() + 1)
#define STATE_DEC(r) State::get(r).update(State::get(r).value() - 1)

#define STATE_MAX ((unsigned int) 0xFFFE)
#define STATE_UNK ((unsigned int) 0xFFFF)
#define ANALOG_MAX ((unsigned int) 1023)

enum {
	INPUT_LOW = 0,
	INPUT_HIGH,
	INPUT_TOGGLE
};

void input_event(unsigned char target, unsigned char value);

#endif
