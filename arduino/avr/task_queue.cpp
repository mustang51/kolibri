// -*- mode: c++; c-file-style: "linux" -*-

#ifdef ARDUINO
#include <Arduino.h>
#include "heartbeat.h"
#else
#include <stdio.h>
#include <stdlib.h>
#endif
#include "task_queue.h"
#include "misc.h"

// #define DBG_TASKS

#define NULLTASK 255

// the weird definition is to allow the body of the loop to delete the
// iterated object
#define for_each_task_in_queue(t, c, q) for (c = q;			\
					     t = &tasks[c], (c != NULLTASK) ? (c = t->nextp, true) : false; \
					     /*nothing*/)

#define for_each_task(t, c) for_each_task_in_queue(t, c, task_head)

#define task_index(t) (t - tasks)

// each task is represented by this structure
struct task_entry {
	unsigned long due; // in milliseconds
	task_callback callback;
	unsigned char user;
	unsigned char nextp; // next element in chain
}; // MUST BE 8 BYTES OR TASK_INDEX() WILL FAIL

static unsigned char task_queue_size = 0;

// pointer to the first task in the queue
static unsigned char task_head = NULLTASK;

// pointer to the first empty task
static unsigned char free_head = NULLTASK;

// pointer to the first task to be executed
static unsigned char exec_head = NULLTASK;

// an array of task structures
static struct task_entry *tasks = NULL;

#ifndef ARDUINO
static void check_tasks(void);
#endif

static void __task_add(task_callback callback, unsigned char user,
		       unsigned long due)
{
	struct task_entry* t;
	struct task_entry* p;
	unsigned char c;
	unsigned char found = 0;
	unsigned char* prev;

#ifndef ARDUINO
#ifdef DBG_TASKS
	printf("---->> task_add(), user: %u, when: %lu, millis: %lu\n",
	       user, due, millis());
#endif
	check_tasks();
#endif

	// allocate space for a new task if needed
	if (NULLTASK == free_head) {
		// increase it
		task_queue_size++;

		tasks = (struct task_entry *)
			realloc(tasks,
				(task_queue_size * sizeof(task_entry)));

		// the last task in the struct must be the new one
		t = &(tasks[task_queue_size - 1]);
	} else {
		// take the first empty task
		t = &(tasks[free_head]);

		// advance the head of free tasks to the next one
		free_head = t->nextp;
	}

	// t is the new task to insert, fill it up
	t->user	    = user;
	t->callback = callback;
	t->due      = due;

	prev = &task_head;

	// insert it in order, search for the position
	for_each_task(p, c) {
		if (0 != time_remaining_from(t->due, p->due)) {
			// the new task expires before the one in the queue
			// insert it here
			found = 1;
			t->nextp = *prev;
			*prev	 = task_index(t);
			break;
		}

		prev = &(p->nextp);
	}

	if (!found) {
		*prev = task_index(t); // put it after the last one
		t->nextp = NULLTASK;
	}

#ifndef ARDUINO
#ifdef DBG_TASKS
	printf("<<---- task_add()\n");
#endif
	check_tasks();
#endif
	return;
}

void task_add(task_callback callback, unsigned char user,
	      unsigned long when)
{
	return __task_add(callback, user, millis() + when);
}

void task_cancel(task_callback callback, unsigned char user)
{
	struct task_entry *t;
	unsigned char c;
	unsigned char* prev;

#ifndef ARDUINO
#ifdef DBG_TASKS
	printf("---->> task_cancel(): user: %d\n", user);
#endif
	check_tasks();
#endif

	// search for the given user AND callback

	prev = &task_head;

	for_each_task(t, c) {
		if ((t->user == user) &&
		    (t->callback == callback)) {
			// this task matches, remove it
			*prev = t->nextp; // bypass the pointers

			// add it to the free list
			t->nextp  = free_head;
			free_head = task_index(t);

			break;
		}

		prev = &(t->nextp);
	}

#ifndef ARDUINO
#ifdef DBG_TASKS
	printf("<<---- task_cancel()\n");
#endif
	check_tasks();
#endif
}

void task_run(void)
{
	struct task_entry *t;

	unsigned long now;
	unsigned long rearm;
	unsigned char exec_tail;
	unsigned char c;

	now = millis();

	if (NULLTASK == task_head) {
		// no tasks = reset_board, big bug happened!
#ifdef ARDUINO
		reset_board();
#endif
	}

	exec_head = task_head;
	exec_tail = NULLTASK;

	// find expired tasks
	for_each_task(t, c) {
		// tasks are time-ordered, so when we see the first
		// non-expired task, we can stop
		if (!time_expired_from(now, t->due)) {
			break;
		}

		exec_tail = task_index(t); // this task has to be executed
	}

	if (NULLTASK == exec_tail) {
		exec_head = NULLTASK;
		return; // no expired tasks found
	}

	// split the task list at the exec_tail task
	task_head	       = tasks[exec_tail].nextp;
	tasks[exec_tail].nextp = NULLTASK;

#ifndef ARDUINO
#ifdef DBG_TASKS
	printf("---->> run_tasks() millis: %lu\n", millis());
#endif
	check_tasks();
#endif

	// run all tasks
	for_each_task_in_queue(t, c, exec_head) {
		// execute it
		rearm = t->callback(t->user, t->due);

		// rearm it if needed
		if (rearm > 0) {
			__task_add(t->callback, t->user, t->due + rearm);
		}

		// return the task to the empty queue
		exec_head = t->nextp;
		t->nextp  = free_head;
		free_head = task_index(t);
	}

	exec_head = NULLTASK;

#ifndef ARDUINO
#ifdef DBG_TASKS
	printf("<<---- run_tasks()\n");
#endif
	check_tasks();
#endif
}


#ifndef ARDUINO
static void check_tasks(void)
{
	struct task_entry *t;
	unsigned char c;
	unsigned int j, k, x;
	unsigned long prev_time;

#ifdef DBG_TASKS
	printf("  task_head: %d, free_head: %d, exec_head: %d, task_queue_size: %d\n",
	       task_head, free_head, exec_head, task_queue_size);
#endif
	j = 0;

	if (task_head != NULLTASK) {
		prev_time = tasks[task_head].due;
	}

	x = 0;

	for_each_task_in_queue(t, c, exec_head) {
#ifdef DBG_TASKS
		printf("    EXEC: task: %d, due: %d, user: %d, next: %d\n",
		       task_index(t), t->due, t->user, t->nextp);
#endif
		x++;
	}

	for_each_task(t, c) {
#ifdef DBG_TASKS
		printf("    TASK: task: %d, due: %d, user: %d, next: %d\n",
		       task_index(t), t->due, t->user, t->nextp);
#endif

		if (time_remaining_from(t->due, prev_time) > 0) {
			printf("wrong time-order in tasks\n");
		}

		prev_time = t->due;

		j++;
	}

	k = 0;

	for_each_task_in_queue(t, c, free_head) {
#ifdef DBG_TASKS
		printf("    FREE: task: %d, due: %d, user: %d, next: %d\n",
		       task_index(t), t->due, t->user, t->nextp);
#endif
		k++;
	}

	if (j + k + x != task_queue_size) {
		printf("reachable tasks (pending %d, empty %d, exec %d) are "
		       "different than allocated tasks (%d)\n",
		       j, k, x, task_queue_size);
	}
}
#endif
