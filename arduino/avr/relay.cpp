// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>

#include "relay.h"
#include "misc.h"
#include "state.h"
#include "task_queue.h"
#include "buzzer.h"

#define RELAY_COOL 100    /* msecs */
#define RELAY_TIMER_CS 2UL  /* csecs */
#define RELAY_TIMER_MS (RELAY_TIMER_CS * 10UL) /* msecs */

// percentages
#define POSITION_FULL_CLOSED 1
#define POSITION_HALF_OPENED 50
#define POSITION_FULL_OPENED 100

#define PWM_FULL 255
#define PWM_ZERO 0

#define PIN_AC1 3
#define PIN_AC2 5
#define PIN_AC3 6
#define PIN_CURTAINS_DIRECTION 9
#define PIN_CURTAINS_CURRENT   10

enum {
	// base resting case
	RELAY_RESTING = 0,
	RELAY_COOLING,

	// all relay types, normal operation
	RELAY_OPENING,
	RELAY_OPENED,

	// relay_ac2 + relay_ac3 working together, reverse case
	RELAY_OPENING_DUAL,
	RELAY_OPENED_DUAL,

	// relay curtains, reverse case
	RELAY_SWITCHING_REVERSE,
	RELAY_OPENING_REVERSE,
	RELAY_OPENED_REVERSE,
	RELAY_COOLING_REVERSE
};


//                                timeout()
//                     +-------------------------------+
//                     |                               |
//                     V                               |
//                  +-------+  timeout()         +---------------+
//           +---+->|cooling|---------+          |cooling_reverse|<--+
//           |   |  +-------+         |          +---------------+   |
//           |   |                    |                              |
//           |   |                    V                              |
//           |   |                +-------+                          |
//           |   |                |resting|                          |
//           |   |                +-------+                          |
//           |   |rest()       open()|| |open_reverse()              |
//           |   |                   || |                            |rest()
//           |   |     +-------+     || |    +-------------------+   |
//           |   |     |opening|<----+| +--->| switching_reverse |   |
//           |   |     +-------+      |      +-------------------+   |
//           |   |         |          |open_dual()     |             |
//     rest()|   |         |timeout() |       timeout()|             |
//           |   |         V          V                V             |
//           |   |     +------+ +------------+ +---------------+     |
//           |   +-----|opened| |opening_dual| |opening_reverse|     |
//           |         +------+ +------------+ +---------------+     |
//           |           |  ^         |               |              |
//           |           |  |    timeout()   timeout()|              |
//           |           +--+         |               V              |
//           |        cycle_pwm()     V         +--------------+     |
//           |                 +-----------+    |opened_reverse|-----+
//           +-----------------|opened_dual|    +--------------+
//                             +-----------+         |   ^
//                                                   |   |
//                                                   +---+
//                                                 cycle_pwm()
//
//

struct relay_state {
	unsigned char pin;
	unsigned char state;
};

static struct relay_state relay_states[RELAY_MAX];

#define GUESS_START (0xFFFF >> 1)
#define EXTRA_GUESS 100

static unsigned int curtains_position_guess = GUESS_START;

static unsigned long curtains_position_long = 0;

#define LONG_SHIFT  22UL
#define PERCENTAGE_TO_LONG(x) (((unsigned long) x << LONG_SHIFT) / 100UL)
#define LONG_TO_PERCENTAGE(x) ((100UL * (unsigned long) x) >> LONG_SHIFT)

static unsigned long step_up;
static unsigned long step_down;

static unsigned long transition_timeout(unsigned char relay,
					unsigned long expiration);

static void known_position_update(unsigned char relay);

static void unknown_position_update(unsigned char relay);

static unsigned long relay_timeout(unsigned char ignored,
				   unsigned long expiration);

static void relay_transition(unsigned char relay, unsigned char next_state)
{
	struct relay_state *r = &relay_states[relay];

	// handle each new state looking at the old state
	switch(r->state) {
	case RELAY_RESTING:
		switch (next_state) {
		case RELAY_OPENING:
			analogWrite(r->pin, PWM_FULL);
			State::get(STATE_RELAY_CURTAINS1).update(1);
			task_add(transition_timeout, relay,
				 State::get(STATE_RELAY_OPEN_TIME).value());
			break;
		case RELAY_SWITCHING_REVERSE:
			analogWrite(PIN_CURTAINS_DIRECTION, PWM_FULL);
			State::get(STATE_RELAY_CURTAINS1).update(2);
			task_add(transition_timeout, relay, RELAY_COOL);
			break;
		default: // ignore invalid transition
			return;
		}
		break;

	case RELAY_OPENED:
	case RELAY_OPENING:
		switch (next_state) {
		case RELAY_COOLING:
			analogWrite(r->pin, PWM_ZERO);
			task_add(transition_timeout, relay, RELAY_COOL);
			break;
		case RELAY_OPENED: // recycle pwm duty cycle ?
			analogWrite(r->pin, State::get(STATE_PWM_DUTY).value());
			break;
		default: // ignore invalid transition
			return;
		}
		break;

	case RELAY_OPENED_REVERSE:
	case RELAY_OPENING_REVERSE:
		switch (next_state) {
		case RELAY_COOLING_REVERSE:
			analogWrite(PIN_CURTAINS_CURRENT, PWM_ZERO);
			task_add(transition_timeout, relay, RELAY_COOL);
			break;
		case RELAY_OPENED_REVERSE: // recycle pwm duty cycle ?
			analogWrite(PIN_CURTAINS_CURRENT,
				    State::get(STATE_PWM_DUTY).value());
			analogWrite(PIN_CURTAINS_DIRECTION,
				    State::get(STATE_PWM_DUTY).value());
			break;
		default: // ignore invalid transition
			return;
		}
		break;

	default: // current state does not allow any transition
		return; // do not update the state
	}

	// finally, update the state
	r->state = next_state;
}

static void known_position_update(unsigned char relay)
{
	struct relay_state *r    = &relay_states[relay];
	unsigned int desired     = State::get(STATE_CURTAINS_DESIRED1).value();
	unsigned int position    = State::get(STATE_CURTAINS_POSITION1).value();

	// update current position
	switch (r->state) {
	case RELAY_OPENED:  // going forward
	case RELAY_OPENING:  // going forward
		if (position < POSITION_FULL_OPENED) {
			curtains_position_long += step_up;

			State::get(STATE_CURTAINS_POSITION1).update(LONG_TO_PERCENTAGE(curtains_position_long));
		}
		break;
	case RELAY_OPENED_REVERSE: // going backwards
	case RELAY_OPENING_REVERSE: // going backwards
		if (position > POSITION_FULL_CLOSED) {
			curtains_position_long -= step_down;

			State::get(STATE_CURTAINS_POSITION1).update(LONG_TO_PERCENTAGE(curtains_position_long));
		}
		break;
	}

	// trigger any needed action
	switch (r->state) {
	case RELAY_RESTING:
		// need to start moving?
		if (desired == STATE_UNK) {
			break;
		} else if (desired > position) {
			relay_transition(RELAY_CURTAINS1,
					 RELAY_OPENING);
		} else if (desired < position) {
			relay_transition(RELAY_CURTAINS1,
					 RELAY_SWITCHING_REVERSE);
		}

		break;

	case RELAY_OPENED:
	case RELAY_OPENING:
		// change direction or stop?
		if ((desired == STATE_UNK) ||
		    (desired <= position)) {
			relay_transition(RELAY_CURTAINS1,
					 RELAY_COOLING);
		}

		break;

	case RELAY_OPENED_REVERSE:
	case RELAY_OPENING_REVERSE:
		// change direction or stop?
		if ((desired == STATE_UNK) ||
		    (desired >= position)) {
			relay_transition(RELAY_CURTAINS1,
					 RELAY_COOLING_REVERSE);
		}

		break;
	}
}

static void unknown_position_update(unsigned char relay)
{
	struct relay_state *r = &relay_states[relay];
	unsigned int travel_up   = State::get(STATE_CURTAINS_TRAVEL1_UP).value();
	unsigned int travel_down = State::get(STATE_CURTAINS_TRAVEL1_DOWN).value();
	unsigned int desired     = State::get(STATE_CURTAINS_DESIRED1).value();

	// update known position
	switch (r->state) {
	case RELAY_OPENED:  // going forward
	case RELAY_OPENING:  // going forward
		curtains_position_guess++;
		break;
	case RELAY_OPENED_REVERSE: // going backwards
	case RELAY_OPENING_REVERSE: // going backwards
		curtains_position_guess--;
		break;
	}

	// have we guessed enough to declare a known position?
	if (curtains_position_guess >=
	    GUESS_START + ((travel_up + EXTRA_GUESS) / RELAY_TIMER_CS)) {
		State::get(STATE_CURTAINS_POSITION1).update(POSITION_FULL_OPENED);
		curtains_position_long = PERCENTAGE_TO_LONG(100);
		return;
	} else if (curtains_position_guess <=
		   GUESS_START - ((travel_down + EXTRA_GUESS) / RELAY_TIMER_CS)) {
		State::get(STATE_CURTAINS_POSITION1).update(POSITION_FULL_CLOSED);
		curtains_position_long = PERCENTAGE_TO_LONG(1);
		return;
	}

	// trigger needed actions
	switch (r->state) {
	case RELAY_RESTING:
		if (desired == STATE_UNK) {
			break;
		} else if (desired >= POSITION_HALF_OPENED) { // guess normal
			relay_transition(RELAY_CURTAINS1,
					 RELAY_OPENING);
		} else { // guess backwards
			relay_transition(RELAY_CURTAINS1,
					 RELAY_SWITCHING_REVERSE);
		}

		break;
	case RELAY_OPENED:
		// stop guessing or guess change
		if ((desired == STATE_UNK) || (desired < POSITION_HALF_OPENED)) {
			relay_transition(RELAY_CURTAINS1,
					 RELAY_COOLING);
		}

		break;
	case RELAY_OPENED_REVERSE:
		// stop guessing or guess change
		if ((desired == STATE_UNK) ||
		    (desired >= POSITION_HALF_OPENED)) {
			relay_transition(RELAY_CURTAINS1,
					 RELAY_COOLING_REVERSE);
		}

		break;
	}
}

static unsigned long relay_timeout(unsigned char ignored,
				   unsigned long expiration)
{
	// relay AC1
	if ((relay_states[RELAY_AC1].state == RELAY_RESTING) &&
	    State::get(STATE_RELAY_AC1).value() != 0) {
		relay_transition(RELAY_AC1, RELAY_OPENING);
	}

	if ((relay_states[RELAY_AC1].state == RELAY_OPENED) &&
	    State::get(STATE_RELAY_AC1).value() == 0) {
		relay_transition(RELAY_AC1, RELAY_COOLING);
	}

	// relay AC2
	if ((relay_states[RELAY_AC2].state == RELAY_RESTING) &&
	    State::get(STATE_RELAY_AC2).value() != 0) {
		relay_transition(RELAY_AC2, RELAY_OPENING);
	}

	if ((relay_states[RELAY_AC2].state == RELAY_OPENED) &&
	    State::get(STATE_RELAY_AC2).value() == 0) {
		relay_transition(RELAY_AC2, RELAY_COOLING);
	}

	// relay AC3
	if ((relay_states[RELAY_AC3].state == RELAY_RESTING) &&
	    State::get(STATE_RELAY_AC3).value() != 0) {
		relay_transition(RELAY_AC3, RELAY_OPENING);
	}

	if ((relay_states[RELAY_AC3].state == RELAY_OPENED) &&
	    State::get(STATE_RELAY_AC3).value() == 0) {
		relay_transition(RELAY_AC3, RELAY_COOLING);
	}

	// CURTAINS 1
	if (State::get(STATE_CURTAINS_POSITION1).value() != STATE_UNK) {
		known_position_update(RELAY_CURTAINS1);
	} else {
		unknown_position_update(RELAY_CURTAINS1);
	}

	// CURTAINS 2
	// if (State::get(STATE_CURTAINS_POSITION2).value() != STATE_UNK) {
	// 	known_position_update(RELAY_CURTAINS2);
	// } else {
	// 	unknown_position_update(RELAY_CURTAINS2);
	// }

	return RELAY_TIMER_MS;
}

static unsigned long transition_timeout(unsigned char relay,
					unsigned long expiration)
{
	struct relay_state *r = &relay_states[relay];
	unsigned long due = 0;

	switch(r->state) {
	case RELAY_OPENING: // time to switch to PWM
		analogWrite(r->pin, State::get(STATE_PWM_DUTY).value());
		r->state = RELAY_OPENED;
		break;

	case RELAY_SWITCHING_REVERSE: // direction chosen, give current
		analogWrite(PIN_CURTAINS_CURRENT, PWM_FULL);
		r->state = RELAY_OPENING_REVERSE;
		due = State::get(STATE_RELAY_OPEN_TIME).value();
		break;

	case RELAY_OPENING_REVERSE: // time to switch to PWM both relays
		analogWrite(PIN_CURTAINS_DIRECTION,
			    State::get(STATE_PWM_DUTY).value());
		analogWrite(PIN_CURTAINS_CURRENT,
			    State::get(STATE_PWM_DUTY).value());
		r->state = RELAY_OPENED_REVERSE;
		break;

	case RELAY_COOLING: // now we are rested, announce it
		State::get(STATE_RELAY_CURTAINS1).update(0);
		r->state = RELAY_RESTING;
		break;

	case RELAY_COOLING_REVERSE: // rest in two steps
		analogWrite(PIN_CURTAINS_DIRECTION, PWM_ZERO);
		r->state = RELAY_COOLING;
		due = RELAY_COOL;
		break;
	}

	return due;
}

void relays_setup(void)
{
	unsigned char i;

	for (i = 0; i < RELAY_MAX; i++) {
		relay_states[i].state = RELAY_RESTING;
	}

	relay_states[RELAY_CURTAINS1].pin = PIN_CURTAINS_CURRENT;
	relay_states[RELAY_CURTAINS2].pin = PIN_AC2;
	relay_states[RELAY_AC1].pin = PIN_AC1;
	relay_states[RELAY_AC2].pin = PIN_AC2;
	relay_states[RELAY_AC3].pin = PIN_AC3;

	pinMode(PIN_CURTAINS_CURRENT, OUTPUT);
	pinMode(PIN_CURTAINS_DIRECTION, OUTPUT);
	pinMode(PIN_AC1, OUTPUT);
	pinMode(PIN_AC2, OUTPUT);
	pinMode(PIN_AC3, OUTPUT);

	analogWrite(PIN_CURTAINS_CURRENT, PWM_ZERO);
	analogWrite(PIN_CURTAINS_DIRECTION, PWM_ZERO);
	analogWrite(PIN_AC1, PWM_ZERO);
	analogWrite(PIN_AC2, PWM_ZERO);
	analogWrite(PIN_AC3, PWM_ZERO);

	State::get(STATE_CURTAINS_POSITION1).disable();
	State::get(STATE_CURTAINS_POSITION2).disable();

	State::get(STATE_CURTAINS_DESIRED1).disable();
	State::get(STATE_CURTAINS_DESIRED2).disable();

	relay_refresh_travels();

	// start relay check routine
	task_add(relay_timeout, 255, RELAY_TIMER_MS);
}

void relay_refresh_duty_cycle(void)
{
	unsigned char i;

	for (i = 0; i < RELAY_MAX; i++) {
		switch (relay_states[i].state) {
		case RELAY_OPENED:
		case RELAY_OPENED_REVERSE:
			// recycle the state, so new duty is applied
			relay_transition(i, relay_states[i].state);
		}
	}
}

void relay_refresh_travels(void)
{
	unsigned long travel_up   = State::get(STATE_CURTAINS_TRAVEL1_UP).value();
	unsigned long travel_down = State::get(STATE_CURTAINS_TRAVEL1_DOWN).value();

	step_up   = ((RELAY_TIMER_CS << LONG_SHIFT) / travel_up);
	step_down = ((RELAY_TIMER_CS << LONG_SHIFT) / travel_down);
}
