// -*- mode: c++; c-file-style: "linux" -*-

/** @file */

#include <Arduino.h>

#include "misc.h"
#include "relay.h"
#include "state.h"

void setup()
{
	State::get().setup();
}

void loop()
{
	State::get().loop();
}
