// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>

#include <math.h>

#include "state.h"
#include "task_queue.h"
#include "bh1750.h"
#include "i2c.h"

// Puts the sensor in continuous capture H-mode and poll constantly
// the last measurement. Dynamic ranging is achieved through
// different measurement windows.

#define BH1750_ADDRESS_DEFAULT   0x23 /* ADDR low  */
#define BH1750_ADDRESS_ALTERNATE 0x5C /* ADDR high */

#define BH1750_COMMAND_POWER_DOWN		0x00
#define BH1750_COMMAND_POWER_ON			0x01
#define BH1750_COMMAND_RESET			0x07
#define BH1750_COMMAND_CONT_H_RESOL_MODE	0x10
#define BH1750_COMMAND_CONT_H_RESOL_MODE2	0x11
#define BH1750_COMMAND_CONT_L_RESOL_MODE	0x13
#define BH1750_COMMAND_ONE_H_RESOL_MODE		0x20
#define BH1750_COMMAND_ONE_H_RESOL_MODE2	0x21
#define BH1750_COMMAND_ONE_L_RESOL_MODE		0x23
#define BH1750_COMMAND_CHANGE_MEAS_TIME_HIGH	0x40
#define BH1750_COMMAND_CHANGE_MEAS_TIME_LOW	0x60

#define BH1750_WINDOW_MAX_VALUE_HIGH            0x07
#define BH1750_WINDOW_MAX_VALUE_LOW             0x1E
#define BH1750_WINDOW_NORMAL_VALUE_HIGH         0x02
#define BH1750_WINDOW_NORMAL_VALUE_LOW          0x05
#define BH1750_WINDOW_MIN_VALUE_HIGH            0x00
#define BH1750_WINDOW_MIN_VALUE_LOW             0x1F
#define BH1750_WINDOW_DEFAULT_VALUE             0x45

#define BH1750_TIMEOUT_DEFAULT_MS                250
#define BH1750_TIMEOUT_POLL_MS                  5000
#define BH1750_TIMEOUT_NEXT_MEASURE_MS          500

enum bh1750_sense_e {
	BH1750_SENSE_NORMAL = 0,
	BH1750_SENSE_LOW,
	BH1750_SENSE_HIGH
};

enum bh1750_state_e {
	BH1750_STATE_DOWN = 0,
	BH1750_STATE_ON,
	BH1750_STATE_READY,
	BH1750_STATE_MEASURING_NORMAL_SENSE,
	BH1750_STATE_MEASURING_HIGH_SENSE,
	BH1750_STATE_MEASURING_LOW_SENSE
};

static unsigned char bh1750_address = BH1750_ADDRESS_DEFAULT;

static enum bh1750_state_e bh1750_state = BH1750_STATE_DOWN; // initial state


#define bh1750_send_byte(byte) i2c_write(bh1750_address, byte)

#define bh1750_receive_value(buff) i2c_read(bh1750_address, buff, 2)


static unsigned char bh1750_set_sense(enum bh1750_sense_e sense)
{
	unsigned char window_high = 0, window_low = 0;

	// first, decide the size of the measurement window
	switch (sense) {
	case BH1750_SENSE_NORMAL:
		window_high = (BH1750_COMMAND_CHANGE_MEAS_TIME_HIGH |
			       BH1750_WINDOW_NORMAL_VALUE_HIGH);

		window_low  = (BH1750_COMMAND_CHANGE_MEAS_TIME_LOW |
			       BH1750_WINDOW_NORMAL_VALUE_LOW);

		break;

	case BH1750_SENSE_LOW:
		window_high = (BH1750_COMMAND_CHANGE_MEAS_TIME_HIGH |
			       BH1750_WINDOW_MIN_VALUE_HIGH);

		window_low  = (BH1750_COMMAND_CHANGE_MEAS_TIME_LOW |
			       BH1750_WINDOW_MIN_VALUE_LOW);

		break;

	case BH1750_SENSE_HIGH:
		window_high = (BH1750_COMMAND_CHANGE_MEAS_TIME_HIGH |
			       BH1750_WINDOW_MAX_VALUE_HIGH);

		window_low  = (BH1750_COMMAND_CHANGE_MEAS_TIME_LOW |
			       BH1750_WINDOW_MAX_VALUE_LOW);

		break;
	}

	// then, program the window size on the system
	if (bh1750_send_byte(window_high)) return 1;

	if (bh1750_send_byte(window_low)) return 1;

	// and last, send the measurement command
	switch (sense) {
	case BH1750_SENSE_NORMAL:
		if (bh1750_send_byte(
			    BH1750_COMMAND_CONT_H_RESOL_MODE)) {
			return 1;
		}

		break;
	case BH1750_SENSE_LOW:
		if (bh1750_send_byte(
			    BH1750_COMMAND_CONT_L_RESOL_MODE)) {
			return 1;
		}

		break;
	case BH1750_SENSE_HIGH:
		if (bh1750_send_byte(
			    BH1750_COMMAND_CONT_H_RESOL_MODE2)) {
			return 1;
		}

		break;
	}

	return 0;
}

static unsigned long bh1750_task(unsigned char reg, unsigned long expiration)
{
	float value;
	unsigned long value_int;
	unsigned char buff[2];
	unsigned long due = BH1750_TIMEOUT_DEFAULT_MS;
	Register rh = State::get(STATE_BH1750_HIGH);
	Register rl = State::get(STATE_BH1750_LOW);

	switch (bh1750_state) {
	case BH1750_STATE_DOWN:
		rh.disable();
		rl.disable();

		// start the chip
		if (0 != bh1750_send_byte(BH1750_COMMAND_POWER_ON)) {
			// try another address
			if (bh1750_address == BH1750_ADDRESS_DEFAULT) {
				bh1750_address = BH1750_ADDRESS_ALTERNATE;
			} else {
				bh1750_address = BH1750_ADDRESS_DEFAULT;
			}

			due = BH1750_TIMEOUT_POLL_MS;

			break; // error
		}

		bh1750_state = BH1750_STATE_ON;
		break;

	case BH1750_STATE_ON:
		// reset the chip, to ensure stable value
		if (0 != bh1750_send_byte(BH1750_COMMAND_RESET)) {
			bh1750_state = BH1750_STATE_DOWN;
			break; // error
		}

		bh1750_state = BH1750_STATE_READY;
		break;

	case BH1750_STATE_READY:
		// start continuous measuring in default mode
		if (0 != bh1750_send_byte(BH1750_COMMAND_CONT_H_RESOL_MODE)) {
			bh1750_state = BH1750_STATE_DOWN;
			break; // error
		}

		bh1750_state = BH1750_STATE_MEASURING_NORMAL_SENSE;
		break;

	case BH1750_STATE_MEASURING_NORMAL_SENSE:
	case BH1750_STATE_MEASURING_LOW_SENSE:
	case BH1750_STATE_MEASURING_HIGH_SENSE:

		if (0 != bh1750_receive_value(buff)) {
			// reset the sensor
			bh1750_state = BH1750_STATE_DOWN;
			break; // error
		}

		// convert the read value
		value = (buff[0] << 8) | buff[1];

		value /= 1.2;

		switch (bh1750_state) {
		case BH1750_STATE_MEASURING_NORMAL_SENSE:

			if (value < 100) {
				if (0 != bh1750_set_sense(BH1750_SENSE_HIGH)) {
					bh1750_state = BH1750_STATE_DOWN;
					break; // error
				}

				bh1750_state =
					BH1750_STATE_MEASURING_HIGH_SENSE;

			} else if (value > 10000) {
				if (0 != bh1750_set_sense(BH1750_SENSE_LOW)) {
					bh1750_state = BH1750_STATE_DOWN;
					break; // error
				}

				bh1750_state =
					BH1750_STATE_MEASURING_LOW_SENSE;
			}

			break;

		case BH1750_STATE_MEASURING_LOW_SENSE:

			value *= (4.0F * 2.222F);

			if (value < 7500) {
				if (0 != bh1750_set_sense(BH1750_SENSE_NORMAL)) {
					bh1750_state = BH1750_STATE_DOWN;
					break; // error
				}

				bh1750_state =
					BH1750_STATE_MEASURING_NORMAL_SENSE;
			}

			break;

		case BH1750_STATE_MEASURING_HIGH_SENSE:

			value /= (2.0F * 3.68F);

			if (value > 150) {
				if (0 != bh1750_set_sense(BH1750_SENSE_NORMAL)) {
					bh1750_state = BH1750_STATE_DOWN;
					break; // error
				}

				bh1750_state =
					BH1750_STATE_MEASURING_NORMAL_SENSE;
			}

			break;
		default:
			break; // should be impossible
		}

		// return a value in scale of 0.1 lux
		// and round it
		value_int = (value * 10) + 0.5;

		rh.update((value_int & 0xFFFF0000) >> 16);
		rl.update((value_int & 0x0000FFFF));

		due = BH1750_TIMEOUT_NEXT_MEASURE_MS;
		break;
	}

	return due;
}

void bh1750_setup(void)
{
	task_add(bh1750_task, STATE_LAST, BH1750_TIMEOUT_DEFAULT_MS);
}
