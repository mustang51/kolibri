// -*- mode: c++; c-file-style: "linux" -*-

#ifndef __REGISTERS_H__
#define __REGISTERS_H__

enum command_source {
	SRC_LOCAL = 0,
	SRC_REMOTE
};

enum {
	REGVAL_UPTIME_HIGH = 0,
	REGVAL_UPTIME_LOW,
	REGVAL_LATENCY_100NS,
	REGVAL_LATENCY_MS_MAX,
	REGVAL_EEPROM_ERRORS,
	REGVAL_VCC,
	REGVAL_TEMP,
	REGVAL_FREE_MEM,
	REGVAL_WATCHDOG,
	REGVAL_CURTAINS_POSITION1,
	REGVAL_CURTAINS_POSITION2,
	REGVAL_MODBUS_MSG_SEC,
	REGVAL_MODBUS_ERRORS,
	REGVAL_MODBUS_COMMANDS,
	REGVAL_INPUT_COMMANDS,
	REGVAL_CURTAINS_DESIRED1,
	REGVAL_CURTAINS_DESIRED2,

	REGVAL_RELAY_CURTAINS1,
	REGVAL_RELAY_CURTAINS2,
	REGVAL_RELAY_AC1,
	REGVAL_RELAY_AC2,
	REGVAL_RELAY_AC3,

	REGVAL_BH1750_HIGH,
	REGVAL_BH1750_LOW,
	REGVAL_BMP085_PRESSURE,
	REGVAL_BMP085_TEMP,
	REGVAL_SHT2X_HUMIDITY,
	REGVAL_SHT2X_TEMP,

	REGVAL_DBG_1,
	REGVAL_DBG_2,
	REGVAL_DBG_3,
	REGVAL_DBG_4,

	REGVAL_MAX // last one
};

enum {
	EEPROM_WRITES = 0,
	EEPROM_DEVICE_ID,
	EEPROM_SERIAL_SPEED,
	EEPROM_PWM_DUTY,
	EEPROM_RELAY_OPEN_TIME,
	EEPROM_VCC_CALIB_0,
	EEPROM_VCC_CALIB_1,

	EEPROM_DIGITAL_MODE_D4,
	EEPROM_DIGITAL_MODE_D7,
	EEPROM_DIGITAL_MODE_D11,
	EEPROM_DIGITAL_MODE_D12,
	EEPROM_ANALOG_MODE_A0,
	EEPROM_ANALOG_MODE_A1,
	EEPROM_ANALOG_MODE_A2,
	EEPROM_ANALOG_MODE_A3,
	EEPROM_ANALOG_MODE_A6,
	EEPROM_ANALOG_MODE_A7,
	EEPROM_CURTAINS_TRAVEL1_UP,
	EEPROM_CURTAINS_TRAVEL1_DOWN,
	EEPROM_CURTAINS_TRAVEL2_UP,
	EEPROM_CURTAINS_TRAVEL2_DOWN,
	EEPROM_INPUT_POLL_TIME,
	EEPROM_INPUT_FILTER_CNT,

	EEPROM_MAX // last one
};

#define PLAIN(p) STATE_##p
#define STRINGY(p) #p

#define PARAM_LIST(P)				\
	P(REGISTER_COUNT),			\
		P(DEVICE_ID),			\
		P(SERIAL_SPEED),		\
		P(SW_VERSION),                  \
		P(RESET),			\
		P(PWM_DUTY),			\
		P(RELAY_OPEN_TIME),		\
		P(INPUT_POLL_TIME),		\
		P(INPUT_FILTER_CNT),		\
		P(VCC_CALIB_0),			\
		P(VCC_CALIB_1),			\
		P(CURTAINS_TRAVEL1_UP),		\
		P(CURTAINS_TRAVEL1_DOWN),	\
		P(CURTAINS_TRAVEL2_UP),		\
		P(CURTAINS_TRAVEL2_DOWN),	\
		P(BUZZER),			\
						\
		P(DIGITAL_MODE_D4),		\
		P(DIGITAL_MODE_D7),		\
		P(DIGITAL_MODE_D11),		\
		P(DIGITAL_MODE_D12),		\
						\
		P(ANALOG_MODE_A0),		\
		P(ANALOG_MODE_A1),		\
		P(ANALOG_MODE_A2),		\
		P(ANALOG_MODE_A3),		\
		P(ANALOG_MODE_A6),		\
		P(ANALOG_MODE_A7),		\
						\
		/* must be first */		\
		P(UPTIME_HIGH),			\
		P(UPTIME_LOW),			\
		P(LATENCY_100NS),		\
		P(LATENCY_MS_MAX),		\
		P(EEPROM_WRITES),		\
		P(EEPROM_ERRORS),		\
		P(VCC),				\
		P(TEMP),			\
		P(FREE_MEM),			\
		P(WATCHDOG),			\
		P(CURTAINS_POSITION1),		\
		P(CURTAINS_POSITION2),		\
		P(MODBUS_MSG_SEC),              \
		P(MODBUS_ERRORS),		\
		P(MODBUS_COMMANDS),		\
		P(INPUT_COMMANDS),		\
		/* must be last */		\
						\
		P(CURTAINS_DESIRED1),		\
		P(CURTAINS_DESIRED2),		\
						\
		P(RELAY_CURTAINS1),		\
		P(RELAY_CURTAINS2),		\
		P(RELAY_AC1),			\
		P(RELAY_AC2),			\
		P(RELAY_AC3),			\
						\
		P(DIGITAL_D4),			\
		P(DIGITAL_D7),			\
		P(DIGITAL_D11),			\
		P(DIGITAL_D12),			\
						\
		P(ANALOG_A0),			\
		P(ANALOG_A1),			\
		P(ANALOG_A2),			\
		P(ANALOG_A3),			\
		P(ANALOG_A6),			\
		P(ANALOG_A7),			\
						\
		P(BH1750_HIGH),			\
		P(BH1750_LOW),			\
		P(BMP085_PRESSURE),		\
		P(BMP085_TEMP),			\
		P(SHT2X_HUMIDITY),		\
		P(SHT2X_TEMP),			\
						\
		P(DBG_1),			\
		P(DBG_2),			\
		P(DBG_3),			\
		P(DBG_4),			\
		/* must be last */		\
		P(LAST)

enum state_params_e {
        PARAM_LIST(PLAIN)
};

enum {
	SERIAL_SPEED_9600 = 0,
	SERIAL_SPEED_19200,
	SERIAL_SPEED_38400,
	SERIAL_SPEED_57600,
	SERIAL_SPEED_115200,
	SERIAL_SPEED_230400,
	SERIAL_SPEED_460800,

	// keep this one
	SERIAL_SPEED_MAX_VAL
};

class Register {
public:
	Register(unsigned char reg) { this->reg = reg; };

	~Register() { };

	unsigned char num() { return reg; };

	// get current value of the register
	unsigned int value();

	void name(char* buff);

	unsigned int name_len();

	// updating a register just reflects in memory/eeprom the new value
	bool update(unsigned int value);

	bool disable();

	// commanding a register invokes the associated command() function
	bool command(unsigned int value, enum command_source src);

	unsigned int mode();

	unsigned char pin();

private:
	unsigned char reg;

	bool __update(unsigned int value);

	static unsigned int values[REGVAL_MAX];
};

#endif
