// -*- mode: c++; c-file-style: "linux" -*-

#include <Arduino.h>

#include <Wire.h> //IIC

#include "i2c.h"

unsigned char i2c_read(unsigned char address, unsigned char *byte)
{
	return i2c_read(address, byte, 1);
}

unsigned char i2c_read(unsigned char address, unsigned char *buff,
		       unsigned char nbytes)
{
	unsigned char i;

	Wire.requestFrom(address, (unsigned char) nbytes);

	for (i = 0; Wire.available(); i++) {
		if (i < nbytes) {
			buff[i] = Wire.read();
		} else {
			Wire.read(); // eat the remaining values
		}
	}

	if (i != nbytes) {
		return 1; // error
	}

	return 0; // OK
}

unsigned char i2c_write(unsigned char address, unsigned char *buff,
			unsigned char nbytes)
{
	Wire.beginTransmission(address);

	Wire.write(buff, nbytes);

	return Wire.endTransmission();
}

unsigned char i2c_write(unsigned char address, unsigned char byte)
{
	Wire.beginTransmission(address);

	Wire.write(byte);

	return Wire.endTransmission();
}


