// -*- mode: c++; c-file-style: "linux" -*-

#if 0

#include <Arduino.h>

#include "misc.h"
#include "pwm.h"

#define PWM_DUTY_CYCLE 90 // 90 over 255, seems like 1.5 volts

void pwm_enable(int pin)
{
	switch(pin) {
	case 6:
		TCCR0A |= _BV(COM0A1);
		break;
	case 5:
		TCCR0A |= _BV(COM0B1);
		break;
	case 9:
		TCCR1A |= _BV(COM1A1);
		break;
	case 10:
		TCCR1A |= _BV(COM1B1);
		break;
	case 11:
		TCCR2A |= _BV(COM2A1);
		break;
	case 3:
		TCCR2A |= _BV(COM2B1);
		break;
	}
}

void pwm_disable(int pin)
{
	switch(pin) {
	case 6:
		TCCR0A &= ~ _BV(COM0A1);
		break;
	case 5:
		TCCR0A &= ~ _BV(COM0B1);
		break;
	case 9:
		TCCR1A &= ~ _BV(COM1A1);
		break;
	case 10:
		TCCR1A &= ~ _BV(COM1B1);
		break;
	case 11:
		TCCR2A &= ~ _BV(COM2A1);
		break;
	case 3:
		TCCR2A &= ~ _BV(COM2B1);
		break;
	}
}

void pwm_setup(void)
{
	// setup timer0 registers
	// OUTPUTA = pin 6
	// OUTPUTB = pin 5
	// no prescaling

	OCR0A = PWM_DUTY_CYCLE;
	OCR0B = PWM_DUTY_CYCLE;

	TCCR0A = _BV(WGM01) | _BV(WGM00);
	TCCR0B = _BV(CS00);  
  
	pinMode(6, OUTPUT);
	pinMode(5, OUTPUT);

	set_timer_factor(64);

	// setup timer1 registers
	// OUTPUTA = pin 9
	// OUTPUTB = pin 10
	// no prescaling

	OCR1A = PWM_DUTY_CYCLE;
	OCR1B = PWM_DUTY_CYCLE; 

	TCCR1A = _BV(WGM12) | _BV(WGM10);
	TCCR1B = _BV(CS10); 

	pinMode(9, OUTPUT);     
	pinMode(10, OUTPUT);

	// setup timer2 registers
	// OUTPUTA = pin 11
	// OUTPUTB = pin 3
	// no prescaling

	OCR2A = PWM_DUTY_CYCLE;
	OCR2B = PWM_DUTY_CYCLE; 

	TCCR2A = _BV(WGM21) | _BV(WGM20);
	TCCR2B = _BV(CS20);  

	pinMode(11, OUTPUT);     
	pinMode(3, OUTPUT);
}
#endif
