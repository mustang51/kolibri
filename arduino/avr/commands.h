#ifndef __COMMANDS_H__
#define __COMMANDS_H__

#include "registers.h"

bool reset_command(class Register *r, unsigned int value,
		   enum command_source src);

bool buzzer_command(class Register *r, unsigned int value,
		    enum command_source src);

bool latency_command(class Register *r, unsigned int value,
		     enum command_source src);

bool pwm_duty_command(class Register* r, unsigned int value,
		      enum command_source src);

bool dbg_command(class Register* r, unsigned int value,
		 enum command_source src);

bool sht2x_command(class Register* r, unsigned int value,
		   enum command_source src);

bool travel_command(class Register* r, unsigned int value,
		    enum command_source src);
#endif
