// -*- mode: c++; c-file-style: "linux" -*-

#include <stdio.h>
#include <unistd.h>
#include <modbus.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <stdint.h>

#include <sys/time.h>

#include "../avr/state_params.h"

extern char *optarg;

modbus_t *ctx;

#define DEFAULT_SERIAL_DEV "/dev/ttyUSB0"
#define DEFAULT_CLIENT_SERIAL_SPEED 115200

#define DEFAULT_RETRY_COUNT 3

#define MAX_MODBUS_DEVICES 255

int modbus_slaves[MAX_MODBUS_DEVICES];

char serial_dev[128]	     = DEFAULT_SERIAL_DEV;
unsigned long	serial_speed = DEFAULT_CLIENT_SERIAL_SPEED;
int address		     = -1;	/* default, all addresess */
int modbus_debug             = 0;
int loop                     = -1;
int modbus_frame_delay       = 3500;
int retry_count              = DEFAULT_RETRY_COUNT;
unsigned int only_errors     = 0;

static unsigned int msgs_count     = 0;
static unsigned int msgs_ok        = 0;
static unsigned int msgs_crc_err   = 0;
static unsigned int msgs_timeout   = 0;
static unsigned int msgs_other_err = 0;

static struct timeval tv_start;

#define PRINT_PARAMS_INDENT 27

#define MAX_REGS 128
#define MAX_REGS_PER_QUERY 20

#define PANIC_IF(condition, ...) if (condition) {			\
		fprintf(stderr, "%s(%d): ", __FILE__, __LINE__);	\
		fprintf(stderr, __VA_ARGS__);				\
		exit(1);						\
	}

#define PANIC(...) do {							\
		fprintf(stderr, "%s(%d): ", __FILE__, __LINE__);	\
		fprintf(stderr, __VA_ARGS__);				\
		exit(1);						\
	} while(0)


char params_name[][PARAMS_STRING_MAX_LEN] = {
	PARAM_LIST(STRINGY)
};

void show_usage(void)
{
	printf("Usage: \n"
	       "  -s slave   : modbus slave to address (default: %d)\n"
	       "  -a address : modbus address to access (default: -1, all)\n"
	       "  -d device  : serial device to use (default: %s)\n"
	       "  -p speed   : serial speed to use  (default: %d)\n"
	       "  -w value   : write value to given address\n"
	       "  -l msec    : loop every msec\n"
	       "  -r cnt     : retry at max cnt times\n"
               "  -c         : show error summary\n"
	       "  -v         : enable libmodbus debugging\n"
	       , DEFAULT_DEVICE_ID, DEFAULT_SERIAL_DEV,
	       DEFAULT_CLIENT_SERIAL_SPEED);
}

int mb_read_registers(modbus_t *ctx, int addr, int nb, uint16_t *dest)
{
	int i, rc, retries;

	for (retries = 0, rc = -1;
	     (rc == -1) && (retries < retry_count);
	     retries++) {

		usleep(modbus_frame_delay);

		rc = modbus_read_registers(ctx, addr, nb, dest);

		msgs_count++;

		if(-1 == rc) {
			printf("ERROR: %s\n", modbus_strerror(errno));
			switch(errno) {
			case ETIMEDOUT:
				msgs_timeout++;
				break;
			case EMBBADCRC:
				msgs_crc_err++;
				break;
			default:
				msgs_other_err++;
				break;
			}
		} else {
			msgs_ok++;
		}
	}

	return rc;
}

int mb_write_registers(modbus_t *ctx, int addr, int nb, const uint16_t *data)
{
	int i, rc, retries;

	for (retries = 0, rc = -1;
	     (rc == -1) && (retries < retry_count);
	     retries++) {
		usleep(modbus_frame_delay);

		rc = modbus_write_registers(ctx, addr, nb, data);

		msgs_count++;

		if(-1 == rc) {
			printf("ERROR: %s\n", modbus_strerror(errno));
			switch(errno) {
			case ETIMEDOUT:
				msgs_timeout++;
				break;
			case EMBBADCRC:
				msgs_crc_err++;
				break;
			default:
				msgs_other_err++;
				break;
			}
		} else {
			msgs_ok++;
		}
	}

	return rc;
}

void register_print(unsigned int reg, uint16_t value)
{
	int i, len;
	char spaces[32];

	len = strlen(params_name[reg]);

	if (len < PRINT_PARAMS_INDENT) {
		for (i = 0; i < (PRINT_PARAMS_INDENT - len); i++) {
			spaces[i] = ' ';
		}
	}

	spaces[i] = '\0';

	printf("[%3d] %s%s=%6d (0x%04X)\n",
	       reg, params_name[reg], spaces, value, value);
}

void read_registers(int address, int count, uint16_t *regs)
{
	int i, first, last, begin, end, rc;

	begin = address;
	end   = begin + count;

	for (i = 0, first = begin;
	     first < end;
	     first += MAX_REGS_PER_QUERY, i += MAX_REGS_PER_QUERY) {

		last = first + MAX_REGS_PER_QUERY;

		if (last > end) last = end;

		rc = mb_read_registers(ctx, first, last - first, regs + i);

		PANIC_IF((-1 == rc), "%s\n", modbus_strerror(errno));
	}
}

void write_registers(int address, int count, uint16_t *regs)
{
	int i, first, last, begin, end, rc;

	begin = address;
	end   = begin + count;

	for (i = 0, first = begin;
	     first < end;
	     first += MAX_REGS_PER_QUERY, i += MAX_REGS_PER_QUERY) {

		last = first + MAX_REGS_PER_QUERY;

		if (last > end) last = end;

		rc = mb_write_registers(ctx, first, last - first, regs + i);

		PANIC_IF((-1 == rc), "%s\n", modbus_strerror(errno));
	}
}

void print_stats(void)
{
	struct timeval tv_now;

	gettimeofday(&tv_now, NULL);

	timersub(&tv_now, &tv_start, &tv_now);

	printf("Msgs total: %d  ok: %d  crc: %d  timeout: %d  other: %d,"
	       " error_rate: %.2f%%, msgs/sec: %.2f\n",
	       msgs_count, msgs_ok, msgs_crc_err, msgs_timeout,
	       msgs_other_err,
	       (float) ((msgs_count - msgs_ok) * 100) / (float) msgs_count,
	       (float) msgs_count /
	       ((float) tv_now.tv_sec + ((float) tv_now.tv_usec) / 1000000));

}

int main(int argc, char *argv[])
{
	int i, s, rc, opt;
	int address = -1, count;
	int write_value = -1;
	uint16_t read_regs[MAX_REGS];
	uint16_t write_regs[MAX_REGS];
	char * aux;

	for (i = 0; i < MAX_MODBUS_DEVICES; i++) {
		modbus_slaves[i] = -1;
	}

	while ((opt = getopt(argc, argv, "ha:d:p:s:w:l:r:cv")) != -1) {
		switch (opt) {
		case 'h':
			show_usage();
			return 0;
		case 'a':
			address = strtol(optarg, &aux, 0);
			break;
		case 'd':
			strncpy(serial_dev, optarg, sizeof(serial_dev));
			break;
		case 'p':
			serial_speed = strtol(optarg, &aux, 0);
			break;
		case 's':
			for (i = 0; i < MAX_MODBUS_DEVICES; i++) {
				if (modbus_slaves[i] >= 0) {
					continue;
				}

				modbus_slaves[i] = strtol(optarg, &aux, 0);
				break;
			}
			break;
		case 'w':
			write_value = strtol(optarg, &aux, 0);
			break;
		case 'l':
			loop = strtol(optarg, &aux, 0);
			break;
                case 'c':
			only_errors = 1;
			break;
		case 'r':
			retry_count = strtol(optarg, &aux, 0);
			break;
		case 'v':
			modbus_debug = 1;
			break;
		default: /* '?' */
			show_usage();
			return 1;
		}
	}

	gettimeofday(&tv_start, NULL);

	if (modbus_slaves[0] < 0) {
		modbus_slaves[0] = DEFAULT_DEVICE_ID;
	}

	// create modbus context
	ctx = modbus_new_rtu(serial_dev, serial_speed, 'E', 8, 1);

	PANIC_IF((NULL == ctx),
		 "Unable to create the libmodbus context\n");

  	if (0 != modbus_connect(ctx)) {
		modbus_free(ctx);
		PANIC("Connection failed: %s\n", modbus_strerror(errno));
	}

	// if (0 != modbus_rtu_set_serial_mode(ctx, MODBUS_RTU_RS485)) {
	// 	modbus_free(ctx);
	// 	PANIC("Connection failed: %s\n", modbus_strerror(errno));
	// }

	//if (0 != modbus_rtu_set_rts(ctx, MODBUS_RTU_RTS_UP)) {
	//	modbus_free(ctx);
	//	PANIC("Connection failed: %s\n", modbus_strerror(errno));
	//}

	if (0 != modbus_debug) {
		modbus_set_debug(ctx, 1);
	}

	struct timeval tv_timeout;

	tv_timeout.tv_sec = 0;
	tv_timeout.tv_usec = 1500;

	modbus_set_byte_timeout(ctx, &tv_timeout);

	tv_timeout.tv_sec = 0;
	tv_timeout.tv_usec = 50000;

	modbus_set_response_timeout(ctx, &tv_timeout);

	if ( 0 != modbus_set_error_recovery(ctx,
					    MODBUS_ERROR_RECOVERY_LINK |
					    MODBUS_ERROR_RECOVERY_PROTOCOL)) {
		modbus_free(ctx);
		PANIC("Unable to set error recovery mode: %s\n",
		      modbus_strerror(errno));
	}


	// address an slave
	rc = modbus_set_slave(ctx, modbus_slaves[0]);

	PANIC_IF((0 != rc), "%s\n", modbus_strerror(errno));

	if ((-1 == write_value) && (-1 == address)) {
		// to read all registers, get register count
		read_registers(STATE_REGISTER_COUNT, 1, read_regs);

		address = 0;
		count   = read_regs[0];

	} else {
		count = 1;
	}

	s = 0;

	while(modbus_slaves[s] > 0) { // loop all slaves

		// address an slave
		rc = modbus_set_slave(ctx, modbus_slaves[s]);

		PANIC_IF((0 != rc), "%s\n", modbus_strerror(errno));

		if (-1 == write_value) { // read registers
			read_registers(address, count, read_regs);

                        if (only_errors) {
				print_stats();
                        } else {
				// print them
				for (i = 0; i < count; i++) {
					register_print(address + i, read_regs[i]);
				}
			}

			s++;

			if (loop >= 0) {
				usleep(loop * 1000);

				if (modbus_slaves[s] < 0) {
					s = 0;
				}
			}
		} else { // write registers
			write_regs[0] = write_value;
			write_registers(address, count, write_regs);

			s++;
		}
	}

	modbus_close(ctx);

	modbus_free(ctx);

	return 0;
}
