EESchema Schematic File Version 2  date mar 02 jul 2013 10:15:48 CEST
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:kolibriconn-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "2 jul 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2900 5000 2900 4750
Wire Wire Line
	2250 4750 2250 5000
Connection ~ 3450 4950
Wire Wire Line
	3650 4850 3650 4950
Wire Wire Line
	3650 4950 3250 4950
Wire Wire Line
	3650 4200 3650 4350
Wire Wire Line
	3650 3650 3650 3800
Wire Wire Line
	2400 4150 2400 3850
Wire Wire Line
	2400 3850 2050 3850
Wire Wire Line
	2350 3350 2350 3650
Wire Wire Line
	2350 3650 2050 3650
Wire Wire Line
	4700 3700 4400 3700
Wire Wire Line
	4700 3500 4400 3500
Connection ~ 3550 3050
Wire Wire Line
	3550 3050 3550 3300
Connection ~ 4200 2450
Wire Wire Line
	4200 2850 4200 2000
Connection ~ 3950 2550
Wire Wire Line
	4500 2750 3950 2750
Wire Wire Line
	3950 2750 3950 2350
Connection ~ 3050 2550
Wire Wire Line
	2500 2750 3050 2750
Wire Wire Line
	4500 2350 2500 2350
Connection ~ 4200 2850
Wire Wire Line
	4500 2650 4200 2650
Wire Wire Line
	2500 3050 4500 3050
Wire Wire Line
	4500 2950 2500 2950
Wire Wire Line
	2500 2850 4500 2850
Wire Wire Line
	2900 2650 2500 2650
Connection ~ 2900 2850
Wire Wire Line
	2500 2450 2900 2450
Wire Wire Line
	2900 2450 2900 2850
Connection ~ 2900 2650
Wire Wire Line
	4200 2450 4500 2450
Connection ~ 4200 2650
Wire Wire Line
	3050 2550 2500 2550
Connection ~ 3050 2350
Wire Wire Line
	4500 2550 3950 2550
Connection ~ 3950 2350
Wire Wire Line
	3050 2750 3050 2000
Wire Wire Line
	3350 3300 3350 2950
Connection ~ 3350 2950
Wire Wire Line
	4700 3600 4400 3600
Wire Wire Line
	4700 3800 4400 3800
Wire Wire Line
	2050 3750 2700 3750
Wire Wire Line
	2700 3750 2700 3350
Wire Wire Line
	3250 3650 3250 3800
Wire Wire Line
	3250 4350 3250 4200
Wire Wire Line
	3250 4950 3250 4850
Wire Wire Line
	3450 5150 3450 4950
Wire Wire Line
	2600 5000 2600 4750
Text Label 2900 5000 0    60   ~ 0
GND
Text Label 2600 5000 0    60   ~ 0
+5V
Text Label 2250 5000 0    60   ~ 0
+9V
$Comp
L TST P3
U 1 1 51D1EC30
P 2900 4750
F 0 "P3" H 2900 5050 40  0000 C CNN
F 1 "GND" H 2900 5000 30  0000 C CNN
	1    2900 4750
	1    0    0    -1  
$EndComp
$Comp
L TST P2
U 1 1 51D1EC2E
P 2600 4750
F 0 "P2" H 2600 5050 40  0000 C CNN
F 1 "+5V" H 2600 5000 30  0000 C CNN
	1    2600 4750
	1    0    0    -1  
$EndComp
$Comp
L TST P1
U 1 1 51D1EC26
P 2250 4750
F 0 "P1" H 2250 5050 40  0000 C CNN
F 1 "+9V" H 2250 5000 30  0000 C CNN
	1    2250 4750
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 51D1E3DC
P 3650 4600
F 0 "R2" V 3730 4600 50  0000 C CNN
F 1 "470" V 3650 4600 50  0000 C CNN
	1    3650 4600
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 51D1E3D6
P 3250 4600
F 0 "R1" V 3330 4600 50  0000 C CNN
F 1 "220" V 3250 4600 50  0000 C CNN
	1    3250 4600
	1    0    0    -1  
$EndComp
Text Label 3650 3650 0    60   ~ 0
+5V
Text Label 3450 5150 0    60   ~ 0
GND
Text Label 3250 3650 0    60   ~ 0
+9V
$Comp
L LED D2
U 1 1 51D1E38F
P 3650 4000
F 0 "D2" H 3650 4100 50  0000 C CNN
F 1 "+5V" H 3650 3900 50  0000 C CNN
	1    3650 4000
	0    1    1    0   
$EndComp
$Comp
L +9V #PWR01
U 1 1 51D1E274
P 2350 3350
F 0 "#PWR01" H 2350 3320 20  0001 C CNN
F 1 "+9V" H 2350 3460 30  0000 C CNN
	1    2350 3350
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR02
U 1 1 51D1E270
P 2700 3350
F 0 "#PWR02" H 2700 3440 20  0001 C CNN
F 1 "+5V" H 2700 3440 30  0000 C CNN
	1    2700 3350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 51D1E25B
P 2400 4150
F 0 "#PWR03" H 2400 4150 30  0001 C CNN
F 1 "GND" H 2400 4080 30  0001 C CNN
	1    2400 4150
	1    0    0    -1  
$EndComp
$Comp
L LED D1
U 1 1 51D1E246
P 3250 4000
F 0 "D1" H 3250 4100 50  0000 C CNN
F 1 "+9V" H 3250 3900 50  0000 C CNN
	1    3250 4000
	0    1    1    0   
$EndComp
$Comp
L RJ45 J1
U 1 1 51D1C5F5
P 2050 2700
F 0 "J1" H 2250 3200 60  0000 C CNN
F 1 "RS485-IN" H 1900 3200 60  0000 C CNN
	1    2050 2700
	0    -1   -1   0   
$EndComp
Text Label 4400 3500 0    60   ~ 0
GND
Text Label 4400 3800 0    60   ~ 0
+5V
Text Label 4400 3600 0    60   ~ 0
B
Text Label 4400 3700 0    60   ~ 0
A
Text Label 3550 3300 0    60   ~ 0
A
Text Label 3350 3300 0    60   ~ 0
B
$Comp
L CONN_4 K2
U 1 1 51D1DC61
P 5050 3650
F 0 "K2" V 5000 3650 50  0000 C CNN
F 1 "RASPICOMM" V 5100 3650 50  0000 C CNN
	1    5050 3650
	1    0    0    -1  
$EndComp
Text Label 2350 3400 0    60   ~ 0
+9V
Text Label 2700 3400 0    60   ~ 0
+5V
Text Label 2400 4050 0    60   ~ 0
GND
$Comp
L CONN_3 K1
U 1 1 51D1DC0A
P 1700 3750
F 0 "K1" V 1650 3750 50  0000 C CNN
F 1 "PWR" V 1750 3750 40  0000 C CNN
	1    1700 3750
	-1   0    0    1   
$EndComp
Text Label 3050 2000 0    60   ~ 0
GND
Text Label 4200 2000 0    60   ~ 0
+9V
$Comp
L RJ45 J2
U 1 1 51D1C618
P 4950 2700
F 0 "J2" H 5150 3200 60  0000 C CNN
F 1 "RS485-OUT" H 4800 3200 60  0000 C CNN
	1    4950 2700
	0    1    -1   0   
$EndComp
$EndSCHEMATC
