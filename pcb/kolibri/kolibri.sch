EESchema Schematic File Version 2  date mié 24 jul 2013 13:09:35 CEST
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MAX485
LIBS:microchip-enc28j60
LIBS:relays
LIBS:DSP1A-L2-DC5V
LIBS:JS1-5V-F
LIBS:rj45
LIBS:ARDUINO_NANO
LIBS:PCH-105D2H,000
LIBS:kolibri-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "24 jul 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 10100 4300
Wire Wire Line
	10100 4300 10100 4200
Wire Wire Line
	10100 4200 10000 4200
Wire Wire Line
	3050 1600 3400 1600
Connection ~ 5350 1500
Connection ~ 4550 1500
Connection ~ 6400 1800
Wire Wire Line
	6400 2000 6400 1600
Wire Wire Line
	6400 1600 6850 1600
Wire Wire Line
	6850 2000 3050 2000
Wire Wire Line
	6850 2100 3050 2100
Connection ~ 3500 750 
Wire Wire Line
	3500 650  3500 800 
Connection ~ 3400 1800
Wire Wire Line
	3400 1600 3400 2000
Connection ~ 3400 2000
Connection ~ 6650 1500
Wire Wire Line
	6850 1700 6650 1700
Connection ~ 3250 1500
Wire Wire Line
	3050 1700 3250 1700
Wire Wire Line
	5350 1400 5350 1500
Wire Wire Line
	4550 1400 4550 1500
Wire Wire Line
	7500 6700 7500 6600
Wire Wire Line
	7500 6600 8000 6600
Wire Wire Line
	8000 6400 7500 6400
Wire Wire Line
	7500 6400 7500 6350
Wire Wire Line
	1950 4850 1700 4850
Wire Wire Line
	1700 4850 1700 3500
Connection ~ 4100 2450
Wire Wire Line
	4100 2200 4100 2600
Wire Wire Line
	4100 2600 3700 2600
Wire Wire Line
	10250 6750 10250 6850
Wire Wire Line
	10250 6350 10250 6450
Wire Wire Line
	7400 5300 6850 5300
Wire Wire Line
	3500 2600 3500 2450
Wire Wire Line
	9700 6050 9700 6250
Connection ~ 9400 6850
Wire Wire Line
	9400 6850 9750 6850
Wire Wire Line
	9750 6450 9400 6450
Wire Wire Line
	9400 6450 9400 7100
Wire Wire Line
	9700 6250 10650 6250
Wire Wire Line
	10450 6650 10250 6650
Wire Wire Line
	10450 6650 10450 6550
Wire Wire Line
	10450 6550 10650 6550
Connection ~ 3650 7500
Wire Wire Line
	3650 3400 3650 7600
Wire Wire Line
	3650 7600 3850 7600
Wire Wire Line
	3850 7200 3400 7200
Connection ~ 3400 6450
Wire Wire Line
	3400 6600 3400 6350
Connection ~ 3400 5650
Wire Wire Line
	3400 5800 3400 5550
Connection ~ 3650 6050
Wire Wire Line
	3650 6050 3850 6050
Wire Wire Line
	3800 4700 3450 4700
Connection ~ 3500 3700
Wire Wire Line
	3500 3850 3500 3600
Connection ~ 3650 4100
Wire Wire Line
	3650 4100 3800 4100
Wire Wire Line
	10350 2200 10350 2300
Wire Wire Line
	10350 2300 10500 2300
Connection ~ 1700 7300
Wire Wire Line
	1700 5700 1700 7500
Wire Wire Line
	1700 5700 1900 5700
Wire Wire Line
	1900 7300 1700 7300
Connection ~ 3650 3950
Wire Wire Line
	3650 3950 3500 3950
Connection ~ 3650 5900
Wire Wire Line
	3650 5900 3400 5900
Wire Wire Line
	3850 3500 3850 2900
Wire Wire Line
	5200 6100 5300 6100
Wire Wire Line
	6600 5700 6600 5450
Wire Wire Line
	6600 5450 6550 5450
Wire Wire Line
	6350 4050 6550 4050
Wire Wire Line
	7400 4800 6850 4800
Wire Wire Line
	10000 4100 10200 4100
Wire Wire Line
	4000 3650 7250 3650
Wire Wire Line
	7250 3650 7250 4300
Wire Wire Line
	7400 4000 7350 4000
Wire Wire Line
	7350 4000 7350 2900
Wire Wire Line
	7350 2900 5450 2900
Connection ~ 4400 5800
Wire Wire Line
	5850 5900 5850 5800
Wire Wire Line
	5850 5800 4000 5800
Wire Wire Line
	4000 4350 5650 4350
Wire Wire Line
	5650 4350 5650 4550
Connection ~ 4000 5100
Wire Wire Line
	1700 3500 1050 3500
Wire Wire Line
	1050 3500 1050 3950
Wire Wire Line
	5750 5650 5750 7450
Wire Wire Line
	5550 4250 5550 7150
Connection ~ 5650 7300
Connection ~ 5850 7600
Wire Wire Line
	5850 6300 5850 7600
Connection ~ 5550 3000
Wire Wire Line
	5950 7300 4250 7300
Wire Wire Line
	4250 7600 5950 7600
Wire Wire Line
	6450 6400 6450 7600
Connection ~ 6450 7150
Wire Wire Line
	6450 7150 6350 7150
Connection ~ 6450 7450
Wire Wire Line
	6350 7450 6450 7450
Connection ~ 5550 3100
Wire Wire Line
	5450 3100 5550 3100
Connection ~ 5650 850 
Wire Wire Line
	5850 700  5850 850 
Wire Wire Line
	5850 850  5350 850 
Wire Wire Line
	5350 850  5350 1000
Wire Wire Line
	4200 850  4550 850 
Wire Wire Line
	4550 850  4550 1000
Wire Wire Line
	5650 700  5650 850 
Wire Wire Line
	3050 2950 3050 3200
Wire Wire Line
	3050 3200 3950 3200
Wire Wire Line
	3950 3100 3400 3100
Wire Wire Line
	3400 3100 3400 2100
Connection ~ 3400 2100
Connection ~ 3500 3100
Wire Wire Line
	3700 2600 3700 3000
Wire Wire Line
	3700 3000 3950 3000
Wire Wire Line
	6450 7600 6350 7600
Wire Wire Line
	6450 7300 6350 7300
Connection ~ 6450 7300
Wire Wire Line
	6450 7000 6350 7000
Connection ~ 6450 7000
Wire Wire Line
	5950 7450 4250 7450
Wire Wire Line
	4250 7150 5950 7150
Wire Wire Line
	5550 3100 5550 3000
Wire Wire Line
	10200 4100 10200 4150
Connection ~ 5750 7450
Connection ~ 5550 7150
Wire Wire Line
	5650 4950 5650 7300
Wire Wire Line
	5950 7000 4250 7000
Wire Wire Line
	4400 6300 4400 7000
Connection ~ 4400 7000
Wire Wire Line
	4400 5900 4400 5800
Connection ~ 4000 4350
Wire Wire Line
	5550 3850 5550 3650
Connection ~ 5550 3650
Wire Wire Line
	4000 5100 5750 5100
Wire Wire Line
	5750 5100 5750 5250
Wire Wire Line
	4000 5800 4000 3350
Connection ~ 4000 3650
Wire Wire Line
	5450 3200 7300 3200
Wire Wire Line
	7300 3200 7300 4100
Wire Wire Line
	7300 4100 7400 4100
Wire Wire Line
	7250 4300 7400 4300
Wire Wire Line
	10000 4300 10600 4300
Wire Wire Line
	3850 2900 3950 2900
Connection ~ 3850 3350
Wire Wire Line
	7400 4400 6850 4400
Wire Wire Line
	6850 4500 7400 4500
Wire Wire Line
	7400 4700 6850 4700
Wire Wire Line
	7400 4900 6850 4900
Wire Wire Line
	7400 5100 6850 5100
Wire Wire Line
	7750 3000 5450 3000
Wire Wire Line
	6650 6100 6850 6100
Wire Wire Line
	6450 4750 6550 4750
Wire Wire Line
	6550 4750 6550 5000
Wire Wire Line
	800  4500 1050 4500
Wire Wire Line
	1050 4500 1050 4450
Wire Wire Line
	4000 3350 3850 3350
Wire Wire Line
	3400 7500 3650 7500
Wire Wire Line
	3650 6700 3400 6700
Connection ~ 3650 6700
Wire Wire Line
	3650 4950 3450 4950
Connection ~ 3650 4950
Wire Wire Line
	1700 6500 1900 6500
Connection ~ 1700 6500
Wire Wire Line
	1000 7100 1000 7500
Wire Wire Line
	1000 7500 1700 7500
Wire Wire Line
	900  6500 1000 6500
Wire Wire Line
	1000 6500 1000 6600
Wire Wire Line
	10500 2400 10200 2400
Wire Wire Line
	10200 2400 10200 2500
Wire Wire Line
	10650 7050 10250 7050
Wire Wire Line
	10650 7050 10650 6750
Wire Wire Line
	3800 3700 3500 3700
Wire Wire Line
	3650 5100 3800 5100
Connection ~ 3650 5100
Wire Wire Line
	3450 4850 3450 4600
Connection ~ 3450 4700
Wire Wire Line
	3850 5650 3400 5650
Wire Wire Line
	3850 6450 3400 6450
Wire Wire Line
	3650 6850 3850 6850
Connection ~ 3650 6850
Wire Wire Line
	3400 7400 3400 7100
Connection ~ 3400 7200
Wire Wire Line
	10650 6650 10550 6650
Wire Wire Line
	10550 6650 10550 6850
Wire Wire Line
	10550 6850 10250 6850
Wire Wire Line
	10250 6450 10650 6450
Wire Wire Line
	10650 6350 10450 6350
Wire Wire Line
	10450 6350 10450 6250
Connection ~ 10450 6250
Wire Wire Line
	9400 6650 9750 6650
Connection ~ 9400 6650
Wire Wire Line
	9750 7050 9400 7050
Connection ~ 9400 7050
Connection ~ 4100 2200
Wire Wire Line
	7400 5200 6850 5200
Wire Wire Line
	7400 4600 6850 4600
Wire Wire Line
	10250 6650 10250 6550
Wire Wire Line
	10250 7050 10250 6950
Wire Wire Line
	6850 5400 7400 5400
Wire Wire Line
	10600 4300 10600 3650
Wire Wire Line
	7400 5000 6850 5000
Wire Wire Line
	4950 1150 4950 1500
Wire Wire Line
	3050 1900 3250 1900
Wire Wire Line
	3250 1900 3250 1500
Connection ~ 3250 1700
Wire Wire Line
	6850 1900 6650 1900
Wire Wire Line
	6650 1900 6650 1500
Connection ~ 6650 1700
Wire Wire Line
	3500 750  3750 750 
Wire Wire Line
	3050 1800 3400 1800
Wire Wire Line
	4200 650  4200 2000
Connection ~ 4200 850 
Wire Wire Line
	6850 1500 3050 1500
Wire Wire Line
	6850 2200 3050 2200
Wire Wire Line
	6850 1800 6400 1800
Connection ~ 6400 2000
Wire Wire Line
	3750 750  3750 1500
Connection ~ 3750 1500
Connection ~ 4950 1500
Connection ~ 4200 2000
Wire Wire Line
	7400 4200 7050 4200
Wire Wire Line
	7050 4200 7050 4000
$Comp
L ARDUINO_NANO_RIGHT_RAIL U3
U 1 1 511982F7
P 9250 4700
F 0 "U3" H 9250 4600 50  0000 C CNN
F 1 "ARDUINO_NANO_RIGHT_RAIL" V 9100 4800 50  0000 C CNN
F 2 "MODULE" H 9250 4700 50  0001 C CNN
F 3 "DOCUMENTATION" H 9250 4700 50  0001 C CNN
	1    9250 4700
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 51EFB5C5
P 7050 4000
F 0 "#PWR?" H 7050 4100 30  0001 C CNN
F 1 "VCC" H 7050 4100 30  0000 C CNN
	1    7050 4000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 51C9FBBF
P 7500 6700
F 0 "#PWR?" H 7500 6700 30  0001 C CNN
F 1 "GND" H 7500 6630 30  0001 C CNN
	1    7500 6700
	1    0    0    -1  
$EndComp
Text Label 7500 6350 0    60   ~ 0
D8
$Comp
L SPEAKER SP1
U 1 1 51C9FB7B
P 8300 6500
F 0 "SP1" H 8200 6750 70  0000 C CNN
F 1 "SPEAKER" H 8200 6250 70  0000 C CNN
	1    8300 6500
	1    0    0    -1  
$EndComp
Text Label 6850 5000 0    60   ~ 0
D8
Text Label 10500 2900 2    60   ~ 0
A3
Text Label 10500 2500 2    60   ~ 0
A7
Text Label 10500 2700 2    60   ~ 0
A5
Text Label 10500 3200 2    60   ~ 0
D11
Text Label 10250 6950 0    60   ~ 0
D12
Text Label 10250 6750 0    60   ~ 0
A2
Text Label 10250 6350 0    60   ~ 0
A0
Text Label 10250 6550 0    60   ~ 0
A1
Text Label 6850 4600 0    60   ~ 0
D4
Text Label 10000 4800 0    60   ~ 0
A3
Text Label 6850 5300 0    60   ~ 0
D11
Text Label 6850 5200 0    60   ~ 0
D10
$Comp
L JUMPER JP1
U 1 1 516006FD
P 3800 2450
F 0 "JP1" H 3800 2600 60  0000 C CNN
F 1 "TERMINATOR" H 3800 2370 40  0000 C CNN
	1    3800 2450
	1    0    0    -1  
$EndComp
$Comp
L R R13
U 1 1 5160062F
P 10000 7050
F 0 "R13" V 10080 7050 50  0000 C CNN
F 1 "10k" V 10000 7050 50  0000 C CNN
	1    10000 7050
	0    -1   -1   0   
$EndComp
$Comp
L CAPAPOL C7
U 1 1 515FFF3B
P 3850 7400
F 0 "C7" H 3900 7500 50  0000 L CNN
F 1 "100uF" H 3900 7300 50  0000 L CNN
	1    3850 7400
	1    0    0    1   
$EndComp
$Comp
L CAPAPOL C6
U 1 1 515FFF39
P 3850 6650
F 0 "C6" H 3900 6750 50  0000 L CNN
F 1 "100uF" H 3900 6550 50  0000 L CNN
	1    3850 6650
	1    0    0    1   
$EndComp
$Comp
L CAPAPOL C5
U 1 1 515FFF33
P 3850 5850
F 0 "C5" H 3900 5950 50  0000 L CNN
F 1 "100uF" H 3900 5750 50  0000 L CNN
	1    3850 5850
	1    0    0    1   
$EndComp
$Comp
L CAPAPOL C4
U 1 1 515FFF2C
P 3800 3900
F 0 "C4" H 3850 4000 50  0000 L CNN
F 1 "100uF" H 3850 3800 50  0000 L CNN
	1    3800 3900
	1    0    0    1   
$EndComp
$Comp
L CAPAPOL C3
U 1 1 515FFE96
P 3800 4900
F 0 "C3" H 3850 5000 50  0000 L CNN
F 1 "100uF" H 3850 4800 50  0000 L CNN
	1    3800 4900
	1    0    0    1   
$EndComp
NoConn ~ 10000 5400
Text Label 1100 6250 0    60   ~ 0
ACIN_LIGHTS
$Comp
L CONN_2 O3
U 1 1 51395ECB
P 750 5250
F 0 "O3" V 700 5250 40  0000 C CNN
F 1 "CURT_2" V 800 5250 40  0000 C CNN
	1    750  5250
	-1   0    0    1   
$EndComp
Text Label 1100 4700 0    60   ~ 0
ACIN_CURT
$Comp
L CONN_2 O2
U 1 1 51395E14
P 750 5700
F 0 "O2" V 700 5700 40  0000 C CNN
F 1 "LIGHTS_2" V 800 5700 40  0000 C CNN
	1    750  5700
	-1   0    0    1   
$EndComp
$Comp
L CONN_2 O1
U 1 1 51395E12
P 750 6150
F 0 "O1" V 700 6150 40  0000 C CNN
F 1 "LIGHTS_1" V 800 6150 40  0000 C CNN
	1    750  6150
	-1   0    0    1   
$EndComp
NoConn ~ 1900 7500
NoConn ~ 1900 6700
NoConn ~ 1900 5900
$Comp
L PCH-105D2H,000 Y1
U 1 1 512725B0
P 2650 7400
F 0 "Y1" H 2650 7300 50  0000 C CNN
F 1 "PCH-105D2H,000" H 2650 7600 50  0000 C CNN
F 2 "MODULE" H 2650 7400 50  0001 C CNN
F 3 "DOCUMENTATION" H 2650 7400 50  0001 C CNN
	1    2650 7400
	-1   0    0    1   
$EndComp
$Comp
L PCH-105D2H,000 Y2
U 1 1 512725AD
P 2650 6600
F 0 "Y2" H 2650 6500 50  0000 C CNN
F 1 "PCH-105D2H,000" H 2650 6800 50  0000 C CNN
F 2 "MODULE" H 2650 6600 50  0001 C CNN
F 3 "DOCUMENTATION" H 2650 6600 50  0001 C CNN
	1    2650 6600
	-1   0    0    1   
$EndComp
$Comp
L PCH-105D2H,000 Y3
U 1 1 512725A9
P 2650 5800
F 0 "Y3" H 2650 5700 50  0000 C CNN
F 1 "PCH-105D2H,000" H 2650 6000 50  0000 C CNN
F 2 "MODULE" H 2650 5800 50  0001 C CNN
F 3 "DOCUMENTATION" H 2650 5800 50  0001 C CNN
	1    2650 5800
	-1   0    0    1   
$EndComp
$Comp
L CONN_6 I1
U 1 1 511C421D
P 11000 6500
F 0 "I1" V 10950 6500 60  0000 C CNN
F 1 "INPUT 1" V 11050 6500 60  0000 C CNN
	1    11000 6500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 511C3A71
P 10200 2500
F 0 "#PWR01" H 10200 2500 30  0001 C CNN
F 1 "GND" H 10200 2430 30  0001 C CNN
	1    10200 2500
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR02
U 1 1 511C3A65
P 10350 2200
F 0 "#PWR02" H 10350 2300 30  0001 C CNN
F 1 "VCC" H 10350 2300 30  0000 C CNN
	1    10350 2200
	1    0    0    -1  
$EndComp
Text Label 10500 2800 2    60   ~ 0
A4
Text Label 10500 2600 2    60   ~ 0
A6
Text Label 10500 3100 2    60   ~ 0
D7
Text Label 10500 3000 2    60   ~ 0
D4
$Comp
L CONN_10 I4
U 1 1 511C39CF
P 10850 2750
F 0 "I4" V 10800 2750 60  0000 C CNN
F 1 "INPUT 4" V 10900 2750 60  0000 C CNN
	1    10850 2750
	1    0    0    -1  
$EndComp
Text Label 10000 5100 0    60   ~ 0
A0
Text Label 10000 5000 0    60   ~ 0
A1
Text Label 10000 4900 0    60   ~ 0
A2
Text Label 10000 4700 0    60   ~ 0
A4
Text Label 10000 4600 0    60   ~ 0
A5
Text Label 10000 4500 0    60   ~ 0
A6
Text Label 10000 4400 0    60   ~ 0
A7
Text Label 3500 650  0    60   ~ 0
GND
Text Label 4200 650  0    60   ~ 0
+9V
Text Label 1000 7100 3    60   ~ 0
ACIN_LIGHTS_F
Text Label 800  4500 1    60   ~ 0
ACIN_CURT
$Comp
L FUSE F2
U 1 1 511AA58F
P 1000 6850
F 0 "F2" H 1100 6900 40  0000 C CNN
F 1 "FUSE" H 900 6800 40  0000 C CNN
	1    1000 6850
	0    1    1    0   
$EndComp
$Comp
L PCH-105D2H,000 Y4
U 1 1 511A3AD1
P 2750 3850
F 0 "Y4" H 2750 3750 50  0000 C CNN
F 1 "PCH-105D2H,000" H 2750 4050 50  0000 C CNN
F 2 "MODULE" H 2750 3850 50  0001 C CNN
F 3 "DOCUMENTATION" H 2750 3850 50  0001 C CNN
	1    2750 3850
	-1   0    0    1   
$EndComp
$Comp
L PCH-105D2H,000 Y5
U 1 1 511A39D1
P 2700 4850
F 0 "Y5" H 2700 4750 50  0000 C CNN
F 1 "PCH-105D2H,000" H 2700 5050 50  0000 C CNN
F 2 "MODULE" H 2700 4850 50  0001 C CNN
F 3 "DOCUMENTATION" H 2700 4850 50  0001 C CNN
	1    2700 4850
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR03
U 1 1 51198C1D
P 9400 7100
F 0 "#PWR03" H 9400 7100 30  0001 C CNN
F 1 "GND" H 9400 7030 30  0001 C CNN
	1    9400 7100
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 51198B1E
P 10000 6850
F 0 "R12" V 10080 6850 50  0000 C CNN
F 1 "10k" V 10000 6850 50  0000 C CNN
	1    10000 6850
	0    -1   -1   0   
$EndComp
$Comp
L R R11
U 1 1 51198B1B
P 10000 6650
F 0 "R11" V 10080 6650 50  0000 C CNN
F 1 "10k" V 10000 6650 50  0000 C CNN
	1    10000 6650
	0    -1   -1   0   
$EndComp
$Comp
L R R10
U 1 1 51198AFD
P 10000 6450
F 0 "R10" V 10080 6450 50  0000 C CNN
F 1 "10k" V 10000 6450 50  0000 C CNN
	1    10000 6450
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR04
U 1 1 51198AE1
P 9700 6050
F 0 "#PWR04" H 9700 6140 20  0001 C CNN
F 1 "+5V" H 9700 6140 30  0000 C CNN
	1    9700 6050
	1    0    0    -1  
$EndComp
$Comp
L ARDUINO_NANO_LEFT_RAIL U2
U 1 1 511982D0
P 8150 4700
F 0 "U2" H 8300 4600 50  0000 C CNN
F 1 "ARDUINO_NANO_LEFT_RAIL" V 8050 4800 50  0000 C CNN
F 2 "MODULE" H 8150 4700 50  0001 C CNN
F 3 "DOCUMENTATION" H 8150 4700 50  0001 C CNN
	1    8150 4700
	1    0    0    -1  
$EndComp
$Comp
L RJ45OUT J2
U 1 1 51197EDB
P 7300 1850
F 0 "J2" H 7500 2350 60  0000 C CNN
F 1 "RS485-OUT" H 7150 2350 60  0000 C CNN
	1    7300 1850
	0    1    -1   0   
$EndComp
$Comp
L RJ45IN J1
U 1 1 51197EBD
P 2600 1850
F 0 "J1" H 2800 2350 60  0000 C CNN
F 1 "RS485-IN" H 2450 2350 60  0000 C CNN
	1    2600 1850
	0    -1   -1   0   
$EndComp
$Comp
L CONN_2 O4
U 1 1 511841C4
P 750 4800
F 0 "O4" V 700 4800 40  0000 C CNN
F 1 "CURT_1" V 800 4800 40  0000 C CNN
	1    750  4800
	-1   0    0    1   
$EndComp
Text Label 3400 7100 0    60   ~ 0
C3
Text Label 4250 7000 0    60   ~ 0
C3
Text Label 3400 6350 0    60   ~ 0
C5
Text Label 4250 7150 0    60   ~ 0
C5
Text Label 3400 5550 0    60   ~ 0
C6
Text Label 4250 7300 0    60   ~ 0
C6
Text Label 3500 3600 0    60   ~ 0
C9
Text Label 4250 7450 0    60   ~ 0
C9
Text Label 3450 4600 0    60   ~ 0
C10
Text Label 4250 7600 0    60   ~ 0
C10
Text Label 1100 5150 0    60   ~ 0
AC5
Text Label 1100 5350 0    60   ~ 0
AC4
Text Label 2000 3950 2    60   ~ 0
AC5
Text Label 2000 3850 2    60   ~ 0
AC4
Text Label 2000 3750 2    60   ~ 0
AC6
Text Label 1950 4750 2    60   ~ 0
AC6
Text Label 900  6500 0    60   ~ 0
ACIN_LIGHTS
Text Label 1050 3950 1    60   ~ 0
ACIN_CURT_F
Text Label 1100 5600 0    60   ~ 0
AC3
Text Label 1100 5800 0    60   ~ 0
AC2
Text Label 1900 5800 2    60   ~ 0
AC3
Text Label 1900 6600 2    60   ~ 0
AC2
Text Label 1100 6050 0    60   ~ 0
AC1
Text Label 1900 7400 2    60   ~ 0
AC1
Text Label 6550 4050 0    60   ~ 0
D5
Text Label 5300 6100 0    60   ~ 0
D3
Text Label 6550 5000 0    60   ~ 0
D6
Text Label 6600 5700 0    60   ~ 0
D9
Text Label 6850 6100 0    60   ~ 0
D10
Text Label 7750 3000 0    60   ~ 0
D2
Text Label 6850 5100 0    60   ~ 0
D9
Text Label 6850 5400 0    60   ~ 0
D12
Text Label 6850 4900 0    60   ~ 0
D7
Text Label 6850 4800 0    60   ~ 0
D6
Text Label 6850 4700 0    60   ~ 0
D5
Text Label 6850 4500 0    60   ~ 0
D3
Text Label 6850 4400 0    60   ~ 0
D2
NoConn ~ 1950 4950
$Comp
L R R5
U 1 1 5117D766
P 6100 4050
F 0 "R5" V 6180 4050 50  0000 C CNN
F 1 "1k" V 6100 4050 50  0000 C CNN
	1    6100 4050
	0    -1   -1   0   
$EndComp
$Comp
L R R8
U 1 1 5117D75F
P 6400 6100
F 0 "R8" V 6480 6100 50  0000 C CNN
F 1 "1k" V 6400 6100 50  0000 C CNN
	1    6400 6100
	0    -1   -1   0   
$EndComp
$Comp
L R R7
U 1 1 5117D75C
P 6300 5450
F 0 "R7" V 6380 5450 50  0000 C CNN
F 1 "1k" V 6300 5450 50  0000 C CNN
	1    6300 5450
	0    -1   -1   0   
$EndComp
$Comp
L R R6
U 1 1 5117D75A
P 6200 4750
F 0 "R6" V 6280 4750 50  0000 C CNN
F 1 "1k" V 6200 4750 50  0000 C CNN
	1    6200 4750
	0    -1   -1   0   
$EndComp
$Comp
L BC548 Q5
U 1 1 5117D589
P 5650 4050
F 0 "Q5" H 5650 3901 40  0000 R CNN
F 1 "BC548" H 5650 4200 40  0000 R CNN
F 2 "TO92" H 5550 4152 29  0000 C CNN
	1    5650 4050
	-1   0    0    1   
$EndComp
$Comp
L BC548 Q6
U 1 1 5117D587
P 5750 4750
F 0 "Q6" H 5750 4601 40  0000 R CNN
F 1 "BC548" H 5750 4900 40  0000 R CNN
F 2 "TO92" H 5650 4852 29  0000 C CNN
	1    5750 4750
	-1   0    0    1   
$EndComp
$Comp
L BC548 Q7
U 1 1 5117D584
P 5850 5450
F 0 "Q7" H 5850 5301 40  0000 R CNN
F 1 "BC548" H 5850 5600 40  0000 R CNN
F 2 "TO92" H 5750 5552 29  0000 C CNN
	1    5850 5450
	-1   0    0    1   
$EndComp
$Comp
L BC548 Q8
U 1 1 5117D556
P 5950 6100
F 0 "Q8" H 5950 5951 40  0000 R CNN
F 1 "BC548" H 5950 6250 40  0000 R CNN
F 2 "TO92" H 5850 6202 29  0000 C CNN
	1    5950 6100
	-1   0    0    1   
$EndComp
$Comp
L BC548 Q4
U 1 1 5117D553
P 4500 6100
F 0 "Q4" H 4500 5951 40  0000 R CNN
F 1 "BC548" H 4500 6250 40  0000 R CNN
F 2 "TO92" H 4400 6202 29  0000 C CNN
	1    4500 6100
	-1   0    0    1   
$EndComp
$Comp
L +5V #PWR05
U 1 1 5117D29B
P 6450 6400
F 0 "#PWR05" H 6450 6490 20  0001 C CNN
F 1 "+5V" H 6450 6490 30  0000 C CNN
	1    6450 6400
	1    0    0    -1  
$EndComp
$Comp
L DIODE D4
U 1 1 5117D249
P 6150 7000
F 0 "D4" H 6300 7050 40  0000 C CNN
F 1 "N4004" H 6000 7050 40  0000 C CNN
	1    6150 7000
	1    0    0    -1  
$EndComp
$Comp
L DIODE D5
U 1 1 5117D245
P 6150 7150
F 0 "D5" H 6300 7200 40  0000 C CNN
F 1 "N4004" H 6000 7200 40  0000 C CNN
	1    6150 7150
	1    0    0    -1  
$EndComp
$Comp
L DIODE D6
U 1 1 5117D243
P 6150 7300
F 0 "D6" H 6300 7350 40  0000 C CNN
F 1 "N4004" H 6000 7350 40  0000 C CNN
	1    6150 7300
	1    0    0    -1  
$EndComp
$Comp
L DIODE D7
U 1 1 5117D241
P 6150 7450
F 0 "D7" H 6300 7500 40  0000 C CNN
F 1 "N4004" H 6000 7500 40  0000 C CNN
	1    6150 7450
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR06
U 1 1 5117CA1E
P 3650 3400
F 0 "#PWR06" H 3650 3490 20  0001 C CNN
F 1 "+5V" H 3650 3490 30  0000 C CNN
	1    3650 3400
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5117C9AC
P 4950 6100
F 0 "R4" V 5030 6100 50  0000 C CNN
F 1 "1k" V 4950 6100 50  0000 C CNN
	1    4950 6100
	0    -1   -1   0   
$EndComp
$Comp
L DIODE D8
U 1 1 5117C978
P 6150 7600
F 0 "D8" H 6300 7650 40  0000 C CNN
F 1 "N4004" H 6000 7650 40  0000 C CNN
	1    6150 7600
	1    0    0    -1  
$EndComp
$Comp
L FUSE F1
U 1 1 5117C62B
P 1050 4200
F 0 "F1" H 1150 4250 40  0000 C CNN
F 1 "FUSE" H 950 4150 40  0000 C CNN
	1    1050 4200
	0    -1   -1   0   
$EndComp
NoConn ~ 10000 4000
NoConn ~ 10000 5300
NoConn ~ 10000 5200
$Comp
L VCC #PWR07
U 1 1 51170698
P 10600 3650
F 0 "#PWR07" H 10600 3750 30  0001 C CNN
F 1 "VCC" H 10600 3750 30  0000 C CNN
	1    10600 3650
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 5116EB85
P 3500 2850
F 0 "R9" V 3580 2850 50  0000 C CNN
F 1 "120" V 3500 2850 50  0000 C CNN
	1    3500 2850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5116E92D
P 3850 3500
F 0 "#PWR08" H 3850 3500 30  0001 C CNN
F 1 "GND" H 3850 3430 30  0001 C CNN
	1    3850 3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5116E929
P 10200 4150
F 0 "#PWR09" H 10200 4150 30  0001 C CNN
F 1 "GND" H 10200 4080 30  0001 C CNN
	1    10200 4150
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR010
U 1 1 5116E8A4
P 3050 2950
F 0 "#PWR010" H 3050 3050 30  0001 C CNN
F 1 "VCC" H 3050 3050 30  0000 C CNN
	1    3050 2950
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR011
U 1 1 5116E7F5
P 5850 700
F 0 "#PWR011" H 5850 800 30  0001 C CNN
F 1 "VCC" H 5850 800 30  0000 C CNN
	1    5850 700 
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR012
U 1 1 5116E70C
P 5650 700
F 0 "#PWR012" H 5650 790 20  0001 C CNN
F 1 "+5V" H 5650 790 30  0000 C CNN
	1    5650 700 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 5116E6E4
P 3500 800
F 0 "#PWR013" H 3500 800 30  0001 C CNN
F 1 "GND" H 3500 730 30  0001 C CNN
	1    3500 800 
	1    0    0    -1  
$EndComp
$Comp
L CAPAPOL C2
U 1 1 5116E545
P 5350 1200
F 0 "C2" H 5400 1300 50  0000 L CNN
F 1 "0.1uF" H 5400 1100 50  0000 L CNN
	1    5350 1200
	1    0    0    -1  
$EndComp
$Comp
L CAPAPOL C1
U 1 1 5116E540
P 4550 1200
F 0 "C1" H 4600 1300 50  0000 L CNN
F 1 "0.33uF" H 4600 1100 50  0000 L CNN
	1    4550 1200
	1    0    0    -1  
$EndComp
$Comp
L 7805 L1
U 1 1 5116DE3F
P 4950 900
F 0 "L1" H 5100 1100 60  0000 C CNN
F 1 "7805" H 4850 1100 60  0000 C CNN
	1    4950 900 
	1    0    0    -1  
$EndComp
$Comp
L MAX485 U1
U 1 1 5116DBEA
P 4700 3050
F 0 "U1" H 4700 2950 50  0000 C CNN
F 1 "MAX485" H 4700 3150 50  0000 C CNN
F 2 "MODULE" H 4700 3050 50  0001 C CNN
F 3 "DOCUMENTATION" H 4700 3050 50  0001 C CNN
	1    4700 3050
	-1   0    0    1   
$EndComp
$EndSCHEMATC
